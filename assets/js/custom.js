/** For Benefits **/
$(document).ready(function() {
    var MaxInputs = 5;
    var InputsWrapper = $("#InputsWrapper");
    var AddButton = $("#AddMoreFileBox");
    var x = InputsWrapper.length;
    var FieldCount = 1;
	
    $(AddButton).click(function(e) {
		var benef=$("#benef").val();
		var benarr=benef.split(',');
        if(x <= MaxInputs) {
            FieldCount++;
			if(benarr[FieldCount-1]==undefined) var fldval="";
			else var fldval=benarr[FieldCount-1];
            $(InputsWrapper).append('<div class="row"><div class="col-sm-11"><input class="form-control" type="text" name="benefits[]" id="benefits_' + FieldCount + '" value="'+fldval+'"></div><a href="javascript:;" class="removeclass"><i class="fa fa-minus-circle danger"></i></a></div></div>');
            x++;
            $("#AddMoreFileId").show();
            $('AddMoreFileBox').html("Add More");
            if(x == MaxInputs) {
                $("#AddMoreFileId").hide();
            }
        }
        return false;
    });
	
    $("body").on("click", ".removeclass", function(e) {
        if(x > 1) {
            $(this).parent('div').remove();
            x--;
            $("#AddMoreFileId").show();
            $('AddMoreFileBox').html("Add More");
        }
        return false;
    })
});

/** For Specializations **/
$(document).ready(function() {
    var MaxInputs_1 = 5;
    var InputsWrapper_1 = $("#InputsWrapper_1");
    var AddButton_1 = $("#AddMoreFileBox_1");
    var x = InputsWrapper_1.length;
    var FieldCount_1 = 1;
	
    $(AddButton_1).click(function(e) {
		var spec=$("#spec").val();
		var specarr=spec.split(',');
        if(x <= MaxInputs_1) {
            FieldCount_1++;
			if(specarr[FieldCount_1-1]==undefined) var fldval_1="";
			else var fldval_1=specarr[FieldCount_1-1];
            $(InputsWrapper_1).append('<div class="row"><div class="col-sm-11"><input class="form-control" type="text" name="specializations[]" id="specializations_' + FieldCount_1 + '" value="'+fldval_1+'"></div><a href="javascript:;" class="removeclass"><i class="fa fa-minus-circle danger"></i></a></div></div>');
            x++;
            $("#AddMoreFileId_1").show();
            $('AddMoreFileBox_1').html("Add More");
            if(x == MaxInputs_1) {
                $("#AddMoreFileId_1").hide();
            }
        }
        return false;
    });
	
    $("body").on("click", ".removeclass_1", function(e) {
        if(x > 1) {
            $(this).parent('div').remove();
            x--;
            $("#AddMoreFileId_1").show();
            $('AddMoreFileBox_1').html("Add More");
        }
        return false;
    })
});
/** preview image before upload **/
function readURL(input) {
	if (input.files && input.files[0]) {
		var reader = new FileReader();
		reader.onload = function(e) {
			$('#imgRef').attr('src', e.target.result);
		}
		reader.readAsDataURL(input.files[0]);
	}
}
$("#image").change(function() {
	readURL(this);
});
/** permalink generation **/
function genpermalink(pname) {
	var permalink;
	permalink=$.trim(pname);
	permalink=permalink.replace(/\s+/g,' ');
	permalink=permalink.toLowerCase();
	permalink=permalink.replace(/\W/g, ' ');
	permalink=permalink.replace(/\s+/g, '-');
	return permalink;
}

function qtVal(step) {
	var err=0;
	if(step=="st1") {
		var rqplc=$("#rqplc").val();
		var plcFor=$("input[name='plcFor']:checked").val();
		alert(plcFor);
		return;
		if(rqplc=="") {
			err++;
			alert("Choose a policy");
			return false;
		}
		else if(plcFor=="") {
			err++;
			alert("Choose a person");
			return false;
		}
	}
	if(err==0) {
		$("#"+step).addClass("next-step");
	}
}