/* Script untuk halaman Asuransi Kendaraan */

	$(function() {
		$('#harga_kendaraan, #perlengkapan_nonstd, #limit_tjh, #limit_pad, #limit_pap').maskMoney({
			thousands: ".",
			decimal: ",",
			precision: "0"
		});
	});
	
	$(function () {
		$('#tjh').change(function () {
			if ($('#tjh').is(':checked')) {
				$('#grup_tjh').show();
				$('#limit_tjh').attr("required", "required");
			} else {
				$('#grup_tjh').hide();
				$('#limit_tjh').removeAttr("required");
			}
		}).change();
	});
	
	$(function () {
		$('#pad').change(function () {
			if ($('#pad').is(':checked')) {
				$('#grup_pad').show();
				$('#limit_pad').attr("required", "required");
			} else {
				$('#grup_pad').hide();
				$('#limit_pad').removeAttr("required");
			}
		}).change();
	});
	
	$(function () {
		$('#pap').change(function () {
			if ($('#pap').is(':checked')) {
				$('#grup_pap').show();
				$('#grup_seat').show();
				$('#limit_pap').attr("required", "required");
				$('#jml_seat').attr("required", "required");
			} else {
				$('#grup_pap').hide();
				$('#grup_seat').hide();
				$('#limit_pap').removeAttr("required");
				$('#jml_seat').removeAttr("required");
			}
		}).change();
	});
	
	$(document).ready(function () {
		$('#cariOlx').submit(function (event) {
			var keyword = $('#keyword').val();
			
			if (keyword == '')
			{
				event.preventDefault();
			}
			else
			{
				var arr = keyword.split(" "); //Memisahkan keyword pada variabel 'keyword' berdasarkan spasi dan menyimpan ke variabel 'arr' dalam bentuk array
				var keybaru = arr.join("-"); //Menggabungkan array dalam variabel 'arr' dengan pemisah tiap array berupa '-', kemudian menyimpannya ke variabel 'keybaru'
				var hasil = "http://olx.co.id/all-results/q-" + keybaru +"/";
	
				window.open(hasil, "olx", "height=500, width=750, toolbar=1, menubar=1, scrollbars=1, resizable=1");
				event.preventDefault();
			}
		});
	});
	
/* Akhir script halaman Asuransi Kendaraan */

/* Script untuk halaman Asuransi Properti */

	$(function() {
		$(".nilai-bangunan").popover({
			html: true
		});
	});

	$(function() {
		$('#nilai_bangunan, #nilai_perabotan, #nilai_mesin, #nilai_stok').maskMoney({
			thousands: ".",
			decimal: ",",
			precision: "0"
		});
	});
	
	$(document).ready(function () {
		var kelasKonstruksi = $('#kelas_konstruksi').val();
		var keterangan = ["Bangunan dikatakan berkonstruksi kelas 1 (satu) apabila dinding, lantai, dan semua komponen penunjang strukturalnya serta penutup atap terbuat seluruhnya dan sepenuhnya dari bahan yang tidak mudah terbakar. Jendela dan/atau pintu beserta kerangkanya, dinding partisi, dan penutup lantai boleh diabaikan.",
							"Bangunan dikatakan berkonstruksi kelas 2 (dua) adalah bangunan yang kriterianya sama seperti apa yang disebutkan dalam bangunan berkonstruksi kelas 1 (satu), dengan kelonggaran penutup atap boleh terbuat dari sirap kayu keras, dinding boleh mengandung bahan yang dapat terbakar sampai maksimun 20% (dua puluh persen) dari luas dinding, serta lantai dan struktur penunjangnya boleh terbuat dari kayu.",
							"Semua bangunan selain yang disebutkan pada kelas konstruksi 1 (satu) dan konstruksi 2 (dua)."];
		
		if (kelasKonstruksi == 1)
		{
			$('#keterangan').text(keterangan[0]);
		}
		else if (kelasKonstruksi == 2)
		{
			$('#keterangan').text(keterangan[1]);
		}
		else
		{
			$('#keterangan').text(keterangan[2]);
		}
		
		$('#kelas_konstruksi').on('change', function() {
			var kelas = $(this).val();
			
			if (kelas == 1)
			{
				$('#keterangan').text(keterangan[0]);
			}
			else if (kelas == 2)
			{
				$('#keterangan').text(keterangan[1]);
			}
			else
			{
				$('#keterangan').text(keterangan[2]);
			}
		});
	});
	
	$(function () {
		$('form').submit(function (event) {
		
			if ($("#nilai_bangunan").val() == '' && $("#nilai_perabotan").val() == '' && $("#nilai_mesin").val() == '' && $("#nilai_stok").val() == '')
			{
				alert("Anda harus mengisi salah satu dari: Nilai Bangunan, Nilai Perabotan, Nilai Mesin, atau Nilai Stok !");
				event.preventDefault();
			}
		});
	});
	
	$("#kab_kota").chained("#provinsi");

/* Akhir script halaman Asuransi Properti */