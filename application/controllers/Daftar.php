<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Daftar extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
		$this->load->helper('form');
		$this->load->library('form_validation');
	}
	
	public function index()
	{
		$data = array (
					'title'=> 'Daftar | solusipremi.com',
					'action' => 'daftar/proses-daftar');

		$this->load->view('templates/header', $data);
		$this->load->view('daftar');
		$this->load->view('partner_solusipremi');
		$this->load->view('templates/footer');
	}
	
	public function proses_daftar()
	{
		//Periksa form pendaftaran yang terkirim
		$this->form_validation->set_rules('firstname', 'Nama Depan', 'trim|required|alpha_numeric_spaces');
		$this->form_validation->set_rules('lastname', 'Nama Belakang', 'trim|required|alpha_numeric_spaces');
		$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|callback_cek_email');
		$this->form_validation->set_rules('handphone', 'Nomor Handphone', 'trim|required|numeric|min_length[10]|max_length[12]');
		$this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[6]');
		$this->form_validation->set_rules('confirmpassword', 'Konfirmasi Password', 'trim|required|matches[password]');
		
		//Jika ada kesalahan pengisian
		if($this->form_validation->run() === FALSE)
		{
			//Tampilkan pesan kesalahan lewat fungsi index()
			$this->index();
		}
		else
		{
			//Jika tidak, simpan form pendaftaran ke database
			//Panggil model 'Daftar_model.php' untuk melakukan proses daftar
			$this->load->model('daftar_model');
			$proses_simpan = $this->daftar_model->proses_daftar();
			
			//Jika proses pendaftaran gagal
			if(!$proses_simpan)
			{
				$data['title'] = 'Proses Pendaftaran Gagal | solusipremi.com';
				
				$this->load->view('templates/header', $data);
				$this->load->view('pendaftaran_gagal');
				$this->load->view('partner_solusipremi');
				$this->load->view('templates/footer');
			}
			else //Jika tidak, tampilkan pesan berhasil
			{
				$data['title'] = 'Proses Pendaftaran Berhasil | solusipremi.com';
				
				$this->load->view('templates/header', $data);
				$this->load->view('pendaftaran_berhasil');
				$this->load->view('partner_solusipremi');
				$this->load->view('templates/footer');
			}
		}
	}
	
	//Fungsi callback untuk memeriksa email pada tabel 'tabel_member' di database apakah ada atau tidak
	public function cek_email($str_email)
	{
		//Panggil model Cek_email_model.php
		$this->load->model('cek_email_model');
		
		//Ambil email dari tabel 'tabel_member' di database untuk mengecek
		$email = $this->cek_email_model->get_email('tabel_member', $str_email);
		
		if($email > 0)
		{
			//Jika email ada, tampilkan pesan kesalahan
			$this->form_validation->set_message('cek_email', 'Email sudah dipakai di database kami. Silakan gunakan alamat email Anda yang lain.');
			return FALSE;
		}
		else
		{
			//Jika tidak ada email nya, kembalikan nilai TRUE
			return TRUE;
		}
	}
}
/* End of file Daftar.php
 Location: ./application/controllers/Daftar.php */