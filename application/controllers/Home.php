<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
		$this->load->helper('form');
		$this->load->library('form_validation');
	}
	
	//Fungsi untuk menampilkan halaman utama
	public function index()
	{	
		$data['title']='Solusi Asuransi Anda | solusipremi.com';
		
		//Muat tampilan
		$this->load->view('templates/header', $data);
		$this->load->view('index');
		
	}
	
	//Fungsi untuk berlangganan newsletter
	public function subscribe()
	{
		//Validasi email yang masuk
		$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|callback_cek_email');
		
		if ($this->form_validation->run() == FALSE)
		{
			//Jika ada kesalahan pengisian, tampilkan pesan
			
			$data['title'] = 'Pendaftaran Newsletter Gagal | solusipremi.com';
			
			$this->load->view('templates/header', $data);
			$this->load->view('index');
			$this->load->view('partner_solusipremi');
			$this->load->view('newsletter');
			$this->load->view('templates/footer');
		}
		else
		{
			//Jika tidak ada kesalahan, panggil model Newsletter_model.php untuk simpan email ke tabel 'newsletter' di database
			$this->load->model('newsletter_model');
			$this->newsletter_model->simpan_email();
			
			//Tampilkan pesan pendaftaran newsletter berhasil
			$data['title'] = 'Pendaftaran Newsletter Berhasil | solusipremi.com';
			
			$this->load->view('templates/header', $data);
			$this->load->view('berlanggan_news_berhasil');
			$this->load->view('partner_solusipremi');
			$this->load->view('templates/footer');
		}
	}
	
	//Fungsi untuk memeriksa email pada tabel 'newsletter' di database apakah sudah pernah terdaftar atau belum
	public function cek_email($str_email)
	{
		//Panggil model Cek_email_model.php
		$this->load->model('cek_email_model');
		
		//Ambil email dari tabel 'newsletter' di database untuk mengecek
		$email = $this->cek_email_model->get_email('newsletter', $str_email);
		
		//Jika ada email yang sama
		if($email > 0)
		{
			//Tampilkan pesan error
			$this->form_validation->set_message('cek_email','Email sudah pernah terdaftar di database kami');
			return FALSE;
		}
		else
		{
			//Jika tidak ada email yang sama, kembalikan nilai TRUE
			return TRUE;
		}
	}
}
/* End of file Home.php
 Location: ./application/controllers/Home.php */