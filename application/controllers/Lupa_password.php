<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Lupa_password extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->load->library('fungsi'); //Memanggil file library Fungsi.php buatan sendiri
	}
	
	public function index()
	{
		$data = array (
						'title' => 'Lupa Password | solusipremi.com',
						'action' => 'lupa-password/ganti-password');

		$this->load->view('templates/header', $data);
		$this->load->view('lupa_password');
		$this->load->view('partner_solusipremi');
		$this->load->view('templates/footer');
	}
	
	//Fungsi untuk ganti password
	public function ganti_password()
	{
		//Validasi email
		$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|callback_cek_email');
		
		//Jika ada kesalahan pengisian
		if ($this->form_validation->run() === FALSE)
		{
			//Tampilkan kesalahan pada fungsi index()
			$this->index();
		}
		else
		{
			//Jika tidak ada kesalahan, lakukan proses penggantian password
			
			//Buat password baru
			$pass_baru = $this->fungsi->pass_acak(); //password baru (belum terenkrip)
			$enkrip_pass = $this->fungsi->balik_md5($pass_baru); //enkripsi password
			
			$this->load->model('lupa_password_model');
			$ganti_pass = $this->lupa_password_model->ganti_password($this->input->post('email'), $enkrip_pass);
			
			//Jika ganti password gagal
			if(!$ganti_pass)
			{
				//Tampilkan pesan Gagal Mengganti Password
				$data['title'] = 'Gagal Mengganti Password | solusipremi.com';
				
				$this->load->view('templates/header', $data);
				$this->load->view('ganti_pass_gagal');
				$this->load->view('partner_solusipremi');
				$this->load->view('templates/footer');
			}
			else
			{
				//Jika tidak, kirim password baru lewat email
				
				//Variabel untuk isi email pengiriman lupa password
				$mail_lupa_pass="Kepada Yth.
Saudara {NAMA},\n
\n
Pada hari {HARI}, {TANGGAL} jam {JAM}, Anda meminta kami untuk melakukan reset password Anda
di solusipremi.com. Berikut ini adalah password baru Anda untuk login kembali.\n
\n
Email	: {EMAIL}
Password: {PASSWORD}\n
\n
Gunakan password baru Anda tersebut untuk login dan update kembali password
Anda agar mudah diingat.\n
\n
Terima Kasih,\n
\n
\n
\n
Administrator
http://solusipremi.com\n";
				
				//Dapatkan data diri user
				$row = $this->lupa_password_model->get_userdata($this->input->post('email'));
				if (isset($row))
				{
					$nama_member = $row['nama_depan'];
					$email_member = $row['email'];
				}
				
				$kode_hari = date('w'); //Kode hari, 0=Minggu, 1=Senin, dan seterusnya
				$nama_hari = array("Minggu","Senin","Selasa","Rabu","Kamis","Jumat","Sabtu");
				$hari = $nama_hari[$kode_hari];
				$tgl = date('d-m-Y'); //Menghasilkan format tanggal seperti dd-mm-yyyy
				$jam = date('H:i:s A'); //Menghasilkan format jam seperti hh:mm:ss AM
				
				//Atur isi pesan email
				$isi = str_replace('{NAMA}', $nama_member, $mail_lupa_pass);
				$isi = str_replace('{HARI}', $hari, $isi);
				$isi = str_replace('{TANGGAL}', $tgl, $isi);
				$isi = str_replace('{JAM}', $jam, $isi);
				$isi = str_replace('{EMAIL}', $email_member, $isi);
				$isi = str_replace('{PASSWORD}', $pass_baru, $isi);
				
				//Proses menyusun dan mengirim email
				$this->load->library('email'); //Panggil library email

				$this->email->from('admin@solusipremi.com', 'Administrator');
				$this->email->to($email_member);
				$this->email->reply_to('no-reply@solusipremi.com', 'No Reply');

				$this->email->subject('Password baru Anda di solusipremi.com');
				$this->email->message($isi);

				$this->email->send(); //Kirim email
				
				//Kode untuk percobaan reset password
				$buka_file = fopen(FCPATH.'email.txt', 'a');
				fwrite($buka_file, $isi);
				fclose($buka_file);
				
				//Tampilkan pesan bahwa Ganti Password Berhasil
				$data['title'] = 'Ganti Password Berhasil | solusipremi.com';
			
				$this->load->view('templates/header', $data);
				$this->load->view('ganti_pass_berhasil');
				$this->load->view('partner_solusipremi');
				$this->load->view('templates/footer');
			}
		}
	}
	
	//Fungsi callback untuk memeriksa email pada tabel 'tabel_member' di database apakah ada atau tidak
	public function cek_email($str_email)
	{
		//Panggil model Cek_email_model.php
		$this->load->model('cek_email_model');
		
		//Ambil email dari tabel 'tabel_member' di database untuk mengecek
		$email = $this->cek_email_model->get_email('tabel_member', $str_email);
		
		if($email > 0)
		{
			//Jika ada email nya, kembalikan nilai TRUE
			return TRUE;
		}
		else
		{
			//Jika email tidak ada, tampilkan pesan kesalahan
			$this->form_validation->set_message('cek_email', 'Email tidak terdaftar di database kami');
			return FALSE;
		}
	}
}
/* End of file Lupa_password.php
 Location: ./application/controllers/Lupa_password.php */