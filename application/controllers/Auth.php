<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
		$this->load->helper('form');
	}
	
	public function index($error = NULL)
	{
		/*$data = array (
					'title'=>'Login | solusipremi.com',
					'action' => 'auth/login',
					'error' => $error);*/
		
		$data['title'] = 'Login | solusipremi.com';
		$data['action'] = 'auth/login';
		$data['error'] = $error;
		/*
		$this->load->helper('captcha');
		
		$vals = array(
						'word' => 'Random word',
						'img_path' => './captcha/',
						'img_url' => base_url().'captcha/',
						'font_path' => './path/to/fonts/texb.ttf',
						'img_width' => '150',
						'img_height' => 30,
						'expiration' => 7200,
						'word_length' => 8,
						'font_size' => 16,
						'img_id' => 'Imageid',
						'pool' => '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ',

						// White background and border, black text and red grid
						'colors' => array(
											'background' => array(255, 255, 255),
											'border' => array(255, 255, 255),
											'text' => array(0, 0, 0),
											'grid' => array(255, 40, 40)
										)
				);

		$cap = create_captcha($vals);
		$data['captcha'] = $cap['image'];
		*/
		$this->load->view('templates/header', $data);
		$this->load->view('login');
		$this->load->view('partner_solusipremi');
		$this->load->view('templates/footer');
	}
	
	public function login()
	{
		$email = $this->input->post('email');
		$password = strrev(md5($this->input->post('password'))); //Enkripsi password
		
		$this->load->model('login_model');
		
		$login = $this->login_model->login($email, $password);
		
		if($login == 1)
		{
			//ambil detail data
			$row = $this->login_model->data_login($email, $password);
			
			//daftarkan session
			$data = array(
						'logged_in' => TRUE,
						'username' => $row->nama_depan,
						'lastname' => $row->nama_belakang,
						'email' => $row->email,
						'id_member' => $row->id
						);
			$this->session->set_userdata($data);
			
			//redirect ke halaman utama
			redirect(site_url('home'));
		}
		else
		{
			//tampilkan pesan error
			$error = 'Email atau Password salah';
			$this->index($error);
		}
	}

	public function logout()
	{
		//destroy session
		$this->session->sess_destroy();
		
		//redirect ke halaman login
		redirect(site_url('auth'));
	}
}
/* End of file Auth.php
 Location: ./application/controllers/Auth.php */