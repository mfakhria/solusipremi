<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Asuransi_properti extends CI_Controller {
	
	//Variabel berisi array
	protected $nilai_bangunan;
	protected $nilai_perabotan;
	protected $nilai_mesin;
	protected $nilai_stok;
	protected $total_pertanggungan;
	protected $rate_par;
	protected $premi_par;
	protected $biaya_polis_materai_par;
	protected $total_premi_par;
	protected $premi_gempa;
	protected $biaya_polis_materai_gempa;
	protected $total_premi_gempa;
	protected $permil_flexas;
	protected $permil_banjir;
	protected $permil_gempa;
	protected $permil_rsmd_cc;
	protected $persen_banjir;
	
	public function __construct()
	{
		parent::__construct();
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->load->library('fungsi');
		$this->load->model('asuransi_properti_model');
	}
	
	public function index()
	{
		$data['title'] = 'Asuransi Properti | solusipremi.com';
		
		$data['provinsi'] = $this->asuransi_properti_model->get_provinsi();
		$data['kota'] = $this->asuransi_properti_model->get_kota();
		$data['provinsi_selected'] = '';
		$data['kota_selected'] = '';
		
		/*
		atau
		
		$data = array(
            'title' => 'Asuransi Properti | solusipremi.com',
			'provinsi' => $this->asuransi_properti_model->get_provinsi(),
            'kota' => $this->asuransi_properti_model->get_kota(),
            'provinsi_selected' => '',
            'kota_selected' => ''
        );
		*/
		
		$this->load->view('templates/header', $data);
		$this->load->view('asuransi_properti', $data);
		$this->load->view('partner_solusipremi');
		$this->load->view('templates/footer');
	}

	public function cari_asuransi_properti()
	{
		$this->form_validation->set_rules('nilai_bangunan', 'Nilai Bangunan', 'trim');
		$this->form_validation->set_rules('nilai_perabotan', 'Nilai Perabotan', 'trim');
		$this->form_validation->set_rules('nilai_mesin', 'Nilai Mesin', 'trim');
		$this->form_validation->set_rules('nilai_stok', 'Nilai Stok', 'trim');
		$this->form_validation->set_rules('penggunaan_bangunan', 'Penggunaan Bangunan', 'trim|required');
		$this->form_validation->set_rules('jumlah_lantai', 'Jumlah Lantai', 'trim|required|numeric');
		$this->form_validation->set_rules('provinsi', 'Provinsi', 'required');
		$this->form_validation->set_rules('kab_kota', 'Kota atau Kabupaten', 'required');
		
		if ($this->form_validation->run() === FALSE)
		{
			$this->index();
		}
		else
		{
			$data['title'] = 'Cari Asuransi Properti | solusipremi.com';
			
			//Definisi variabel untuk perluasan jaminan
			$ket_zona_gempa = '-';
			$this->permil_banjir['desimal'] = 0;
			$this->permil_gempa['desimal'] = 0;
			$this->permil_rsmd_cc['desimal'] = 0;
			
			//Tampung input nilai berupa string angka yang di-post dari form
			//Konversikan ke angka untuk perhitungan
			$this->nilai_bangunan['angka'] = $this->fungsi->convert_to_number($this->input->post('nilai_bangunan'));
			$this->nilai_perabotan['angka'] = $this->fungsi->convert_to_number($this->input->post('nilai_perabotan'));
			$this->nilai_mesin['angka'] = $this->fungsi->convert_to_number($this->input->post('nilai_mesin'));
			$this->nilai_stok['angka'] = $this->fungsi->convert_to_number($this->input->post('nilai_stok'));
			
			//Tampung 'Penggunaan Bangunan' ke variabel $penggunaan_bangunan
			$penggunaan_bangunan = $this->input->post('penggunaan_bangunan');
			
			//Kelas Konstruksi (Jika tidak dipilih, defaultnya '1' 'Kelas Konstruksi 1')
			//value="1" selected="selected" Kelas Konstruksi 1
			//value="2" Kelas Konstruksi 2
			//value="3" Kelas Konstruksi 3
			//Variabel penampungnya adalah $kode_konstruksi
			$kode_konstruksi = $this->input->post('kelas_konstruksi');
			
			//Baca value dari $kode_konstruksi untuk memberi nama jenis Kelas Konstruksi
			if($kode_konstruksi === '1')
			{
				$jenis_konstruksi = 'Kelas Konstruksi 1';
			}
			else if($kode_konstruksi === '2')
			{
				$jenis_konstruksi = 'Kelas Konstruksi 2';
			}
			else
			{
				$jenis_konstruksi = 'Kelas Konstruksi 3';
			}
			
			//Tampung 'Jumlah Lantai' ke variabel $jumlah_lantai
			$jumlah_lantai = $this->input->post('jumlah_lantai');
			
			//Tampung 'Provinsi' ke variabel $id_provinsi. Isi variabel $id_provinsi berupa id provinsi 
			$id_provinsi = $this->input->post('provinsi');
			
			//Tampung 'Kota atau Kabupaten' ke variabel $id_kab_kota. Isi variabel $id_kab_kota berupa id kota
			$id_kab_kota = $this->input->post('kab_kota');
			
			//$ket_jml_lantai untuk menyimpan keterangan mengenai jumlah lantai, kemudian ditampilkan untuk sekedar keterangan saja pada RATE EARTHQUAKE
			//Isi dari $ket_jml_lantai didapatkan dengan memeriksa isi dari variabel $jumlah_lantai
			if ($jumlah_lantai <= 9) //Jika 'Jumlah Lantai' kurang dari sama dengan 9
			{	
				$ket_jml_lantai = '<=9';
			}
			else
			{
				$ket_jml_lantai = '>9';
			}
			
			//Dapatkan okupasi berdasarkan isi variabel $penggunaan_bangunan
			$okupasi = $this->asuransi_properti_model->get_okupasi($penggunaan_bangunan);
			
			//Setelah mendapatkan okupasi, ambil kode okupasi dan simpan ke variabel $kode_okupasi
			$kode_okupasi = $okupasi['kode_okupasi'];
			
			//Kemudian, tentukan tarif premi permil FLEXAS (asuransi kebakaran) berdasarkan kelas konstruksi
			if ($kode_konstruksi === '1')
			{
				$this->permil_flexas['desimal'] = $okupasi['tarif_bawah_kelas_konstruksi1']; //Permil FLEXAS batas bawah pada kelas konstruksi 1
			}
			else if ($kode_konstruksi === '2')
			{
				$this->permil_flexas['desimal'] = $okupasi['tarif_bawah_kelas_konstruksi2']; //Permil FLEXAS batas bawah pada kelas konstruksi 2
			}
			else
			{
				$this->permil_flexas['desimal'] = $okupasi['tarif_bawah_kelas_konstruksi3']; //Permil FLEXAS batas bawah pada kelas konstruksi 3
			}
			
			//Dapatkan Zona berdasarkan id Kota atau Kabupaten.
			//Zona diperlukan untuk penentuan tarif permil Asuransi Gempa Bumi nantinya
			$zona = $this->asuransi_properti_model->get_zona($id_kab_kota);
			
			//$nama_provinsi menyimpan nama provinsi dari variabel $zona['provinsi']
			$nama_provinsi = $zona['provinsi'];
			
			//$nama_kab_kota menyimpan nama kabupaten atau kota dari variabel $zona['daerah_tingkat_ii'];
			$nama_kab_kota = $zona['daerah_tingkat_ii'];
			
			//$zona_gempa menyimpan Zona dari variabel $zona['zona']
			$zona_gempa = $zona['zona'];
			
			//Tampung value dari perluasan jaminan ke variabel $kode_perluasan
			$kode_perluasan = $this->input->post('perluasan[]');
			
			//Periksa apakah ada perluasan yang dipilih
			if(isset($kode_perluasan))
			{
				//Jika ada, periksa setiap indeks array pada $kode_perluasan dengan looping foreach
				foreach($kode_perluasan as $kode_jaminan):
				
					if($kode_jaminan === '1') //Jika jaminan 'Banjir' dipilih
					{
						$perluasan_dipilih[] = 'Banjir';
						
						//Dapatkan jaminan 'Banjir' berdasarkan id provinsi
						$jaminan_banjir = $this->asuransi_properti_model->get_jaminan_banjir($id_provinsi);
						
						//Ambil persen tarif premi banjir
						$this->persen_banjir = $jaminan_banjir['tarif_premi'];
						
						//Karena tarif premi banjir dalam persen, ubah jadi permil
						//Tarif premi banjir di tabel dalam database sudah dibagi 100, jadi yang di variabel $persen_banjir merupakan angka sebenarnya
						//Untuk mengubahnya menjadi permil, kalikan dengan 1000
						$this->permil_banjir['desimal'] = $this->persen_banjir * 1000;
					}
					
					else if($kode_jaminan === '2') //Jika jaminan 'Gempa Bumi' dipilih
					{
						$perluasan_dipilih[] = 'Gempa Bumi';
						
						//Dapatkan jaminan 'Gempa Bumi' berdasarkan wilayah
						$jaminan_gempa = $this->asuransi_properti_model->get_jaminan_gempa($kode_okupasi, $jumlah_lantai);
						
						//Ambil tarif permil gempa berdasarkan zona gempa
						if ($zona_gempa === 1)
						{
							$this->permil_gempa['desimal'] = $jaminan_gempa['zona_1'];
							$ket_zona_gempa = 'I';
						}
						else if ($zona_gempa === 2)
						{
							$this->permil_gempa['desimal'] = $jaminan_gempa['zona_2'];
							$ket_zona_gempa = 'II';
						}
						else if ($zona_gempa === 3)
						{
							$this->permil_gempa['desimal'] = $jaminan_gempa['zona_3'];
							$ket_zona_gempa = 'III';
						}
						else if ($zona_gempa === 4)
						{
							$this->permil_gempa['desimal'] = $jaminan_gempa['zona_4'];
							$ket_zona_gempa = 'IV';
						}
						else
						{
							$this->permil_gempa['desimal'] = $jaminan_gempa['zona_5'];
							$ket_zona_gempa = 'V';
						}
					}
					
					else if($kode_jaminan === '3') //Jika jaminan 'RSMD CC + Others' dipilih
					{
						$perluasan_dipilih[] = 'RSMD CC + Others';
						
						//$permil_rsmd_cc berisi tarif permil (belum dibagi 1000) untuk perluasan jaminan berupa RSMD CC + Others
						$this->permil_rsmd_cc['desimal'] = 0.2;
					}
					
				endforeach; //Akhir looping foreach()
		
			}
			else
			{
				//Jika Perluasan tidak dipilih
				$this->permil_banjir['desimal'] = 0;
				$this->permil_gempa['desimal'] = 0;
				$this->permil_rsmd_cc['desimal'] = 0;
				$perluasan_dipilih[] = 'Tidak Ada';
			}
			
			//Variabel $total_pertanggungan berisi total nilai dari nilai bangunan, nilai perabotan, nilai mesin, nilai stok
			$this->total_pertanggungan['angka'] = $this->nilai_bangunan['angka'] + $this->nilai_perabotan['angka'] + $this->nilai_mesin['angka'] + $this->nilai_stok['angka'];
			
			//$rate_par merupakan total permil dari FLEXAS (Kebakaran), TSFWD (Banjir), RSMD CC + Others
			$this->rate_par['desimal'] = $this->permil_flexas['desimal'] + $this->permil_banjir['desimal'] + $this->permil_rsmd_cc['desimal'];
			
			//Hitung premi untuk PAR. Rumusnya: $total_pertanggungan * $rate_par / 1000
			//$rate_par dibagi 1000 karena belum angka sebenarnya
			$this->premi_par['angka'] = $this->total_pertanggungan['angka'] * $this->rate_par['desimal'] / 1000;
			
			//Hitung Biaya Polis dan Materai PAR berdasarkan Total Premi PAR
			$this->biaya_polis_materai_par['angka'] = $this->fungsi->hitung_biaya_polis($this->premi_par['angka']);
			
			//$total_premi_par merupakan total dari: $premi_par + $biaya_polis_materai_par
			$this->total_premi_par['angka'] = $this->premi_par['angka'] + $this->biaya_polis_materai_par['angka'];
			
			//Hitung premi EQ (Gempa). Rumusnya: $total_pertanggungan * $permil_gempa / 1000
			//$permil_gempa dibagi 1000 karena belum angka sebenarnya
			$this->premi_gempa['angka'] = $this->total_pertanggungan['angka'] * $this->permil_gempa['desimal'] / 1000;
			
			//Hitung Biaya Polis dan Materai EQ (Gempa) berdasarkan Total Premi PAR
			$this->biaya_polis_materai_gempa['angka'] = $this->fungsi->hitung_biaya_polis($this->premi_gempa['angka']);
			
			//$total_premi_gempa merupakan total dari: $premi_gempa + $biaya_polis_materai_gempa
			$this->total_premi_gempa['angka'] = $this->premi_gempa['angka'] + $this->biaya_polis_materai_gempa['angka'];
			
			//Konversi angka ke string untuk tampilan
			$this->nilai_bangunan['string'] = $this->fungsi->format_angka($this->nilai_bangunan['angka']);
			$this->nilai_perabotan['string'] = $this->fungsi->format_angka($this->nilai_perabotan['angka']);
			$this->nilai_mesin['string'] = $this->fungsi->format_angka($this->nilai_mesin['angka']);
			$this->nilai_stok['string'] = $this->fungsi->format_angka($this->nilai_stok['angka']);
			$this->total_pertanggungan['string'] = $this->fungsi->format_angka($this->total_pertanggungan['angka']);
			$this->premi_par['string'] = $this->fungsi->format_angka($this->premi_par['angka']);
			$this->biaya_polis_materai_par['string'] = $this->fungsi->format_angka($this->biaya_polis_materai_par['angka']);
			$this->total_premi_par['string'] = $this->fungsi->format_angka($this->total_premi_par['angka']);
			$this->premi_gempa['string'] = $this->fungsi->format_angka($this->premi_gempa['angka']);
			$this->biaya_polis_materai_gempa['string'] = $this->fungsi->format_angka($this->biaya_polis_materai_gempa['angka']);
			$this->total_premi_gempa['string'] = $this->fungsi->format_angka($this->total_premi_gempa['angka']);
			
			$this->rate_par['string'] = $this->fungsi->konversi_str_permil($this->rate_par['desimal']);
			$this->permil_flexas['string'] = $this->fungsi->konversi_str_permil($this->permil_flexas['desimal']);
			$this->permil_banjir['string'] = $this->fungsi->konversi_str_permil($this->permil_banjir['desimal']);
			$this->permil_gempa['string'] = $this->fungsi->konversi_str_permil($this->permil_gempa['desimal']);
			$this->permil_rsmd_cc['string'] = $this->fungsi->konversi_str_permil($this->permil_rsmd_cc['desimal']);
			
			//Tampilkan ke view
			$data['v_penggunaan_bangunan'] = $penggunaan_bangunan;
			$data['v_kode_okupasi'] = $kode_okupasi;
			$data['v_jenis_konstruksi'] = $jenis_konstruksi;
			$data['v_jumlah_lantai'] = $jumlah_lantai;
			$data['v_provinsi'] = $nama_provinsi;
			$data['v_kab_kota'] = $nama_kab_kota;
			$data['v_nilai_bangunan'] = $this->nilai_bangunan['string'];
			$data['v_nilai_perabotan'] = $this->nilai_perabotan['string'];
			$data['v_nilai_mesin'] = $this->nilai_mesin['string'];
			$data['v_nilai_stok'] = $this->nilai_stok['string'];
			$data['v_total_pertanggungan'] = $this->total_pertanggungan['string'];
			
			$data['v_rate_par'] = $this->rate_par['string'];
			$data['v_permil_flexas'] = $this->permil_flexas['string'];
			$data['v_permil_banjir'] = $this->permil_banjir['string'];
			$data['v_permil_gempa'] = $this->permil_gempa['string'];
			$data['v_permil_rsmd_cc'] = $this->permil_rsmd_cc['string'];
			
			$data['v_ket_zona_gempa'] = $ket_zona_gempa;
			$data['v_ket_jml_lantai'] = $ket_jml_lantai;
			
			$data['v_premi_par'] = $this->premi_par['string'];
			$data['v_biaya_polis_materai_par'] = $this->biaya_polis_materai_par['string'];
			$data['v_total_premi_par'] = $this->total_premi_par['string'];
			
			$data['v_premi_gempa'] = $this->premi_gempa['string'];
			$data['v_biaya_polis_materai_gempa'] = $this->biaya_polis_materai_gempa['string'];
			$data['v_total_premi_gempa'] = $this->total_premi_gempa['string'];
			
			//Buatkan Session untuk menyimpan hasil pencarian asuransi properti secara global
			$_SESSION['asuransi_properti'] = array(
													'penggunaan_bangunan' => $penggunaan_bangunan,
													'kode_okupasi' => $kode_okupasi,
													'jenis_konstruksi' => $jenis_konstruksi,
													'jumlah_lantai' => $jumlah_lantai,
													'provinsi' => $nama_provinsi,
													'kab_kota' => $nama_kab_kota,
													'nilai_bangunan_str' => $this->nilai_bangunan['string'],
													'nilai_perabotan_str' => $this->nilai_perabotan['string'],
													'nilai_mesin_str' => $this->nilai_mesin['string'],
													'nilai_stok_str' => $this->nilai_stok['string'],
													'total_pertanggungan_str' => $this->total_pertanggungan['string'],
													'rate_par_str' => $this->rate_par['string'],
													'permil_flexas_str' => $this->permil_flexas['string'],
													'permil_banjir_str' => $this->permil_banjir['string'],
													'permil_gempa_str' => $this->permil_gempa['string'],
													'permil_rsmd_cc_str' => $this->permil_rsmd_cc['string'],
													'ket_zona_gempa' => $ket_zona_gempa,
													'jml_lantai' => $jumlah_lantai,
													'ket_jml_lantai' => $ket_jml_lantai,
													'premi_par_str' => $this->premi_par['string'],
													'biaya_polis_materai_par_str' => $this->biaya_polis_materai_par['string'],
													'total_premi_par_str' => $this->total_premi_par['string'],
													'premi_gempa_str' => $this->premi_gempa['string'],
													'biaya_polis_materai_gempa_str' => $this->biaya_polis_materai_gempa['string'],
													'total_premi_gempa_str' => $this->total_premi_gempa['string'],
													'nilai_bangunan' => $this->nilai_bangunan['angka'],
													'nilai_perabotan' => $this->nilai_perabotan['angka'],
													'nilai_mesin' => $this->nilai_mesin['angka'],
													'nilai_stok' => $this->nilai_stok['angka'],
													'total_pertanggungan' => $this->total_pertanggungan['angka'],
													'rate_par' => $this->rate_par['desimal'],
													'permil_flexas' => $this->permil_flexas['desimal'],
													'permil_banjir' => $this->permil_banjir['desimal'],
													'permil_gempa' => $this->permil_gempa['desimal'],
													'permil_rsmd_cc' => $this->permil_rsmd_cc['desimal'],
													'premi_par' => $this->premi_par['angka'],
													'biaya_polis_materai_par' => $this->biaya_polis_materai_par['angka'],
													'total_premi_par' => $this->total_premi_par['angka'],
													'premi_gempa' => $this->premi_gempa['angka'],
													'biaya_polis_materai_gempa' => $this->biaya_polis_materai_gempa['angka'],
													'total_premi_gempa' => $this->total_premi_gempa['angka'],
													'perluasan_dipilih' => $perluasan_dipilih
													);
			
			//Ambil data partner untuk pilihan perusahaan asuransi bagi konsumen
			$data['list_partner'] = $this->asuransi_properti_model->get_partner();
			if(!isset($_SESSION['username'])) //atau || $_SESSION['logged_in'] != TRUE)
		{
			//Jika belum login, arahkan ke halaman login
			redirect(site_url('auth'));
		}
			//Tampilkan hasil
			$this->load->view('templates/header', $data);
			$this->load->view('cari_asuransi_properti', $data);
			$this->load->view('partner_solusipremi');
			$this->load->view('templates/footer');
		}
	}
	
	//Fungsi untuk menangani pengisian data
	public function isi_data($id_part)
	{
		//Cek apakah user sudah login dengan memeriksa session
		if(!isset($_SESSION['username'])) //atau || $_SESSION['logged_in'] != TRUE)
		{
			//Jika belum login, arahkan ke halaman login
			redirect(site_url('auth'));
		}
		
		//Jika session pencarian asuransi properti tidak ada
		if(!isset($_SESSION['asuransi_properti']))
		{
			redirect(site_url('asuransi-properti'));
		}
		
		$data['title'] = 'Isi Data | solusipremi.com';
		
		$id_partner = $id_part;
		
		//Ambil data partner asuransi yang dipilih
		$partner = $this->asuransi_properti_model->get_partner($id_partner);
		
		if(empty($partner))
		{
			show_404();
		}
		
		$data['v_id_partner'] = $id_partner;
		$data['v_nama_partner'] = $partner['nama_partner'];
		
		$data['v_nama_tertanggung'] = $_SESSION['username'].' '.$_SESSION['lastname'];
		$data['v_email'] = $_SESSION['email'];
		
		$this->form_validation->set_rules('nomor_ktp', 'Nomor KTP', 'trim|required|numeric');
		$this->form_validation->set_rules('nama_pemohon', 'Nama Pemohon', 'trim|required|alpha_numeric_spaces');
		$this->form_validation->set_rules('alamat', 'Alamat', 'trim|required');
		$this->form_validation->set_rules('rt', 'RT', 'trim|required|numeric');
		$this->form_validation->set_rules('rw', 'RW', 'trim|required|numeric');
		$this->form_validation->set_rules('desa_kelurahan', 'Desa atau Kelurahan', 'trim|required');
		$this->form_validation->set_rules('kecamatan', 'Kecamatan', 'trim|required');
		$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
		$this->form_validation->set_rules('nomor_telepon', 'Nomor Telepon', 'trim|required|numeric');
		
		$this->form_validation->set_rules('penggunaan_bangunan', 'Penggunaan Bangunan', 'trim|required');
		
		
		if ($this->form_validation->run() == FALSE)
		{
			$this->load->view('templates/header', $data);
			$this->load->view('isi_spak', $data);
			$this->load->view('partner_solusipremi');
			$this->load->view('templates/footer');
		}
		else
		{
			
		}
		/*else
		{
			redirect(site_url('asuransi-properti/verifikasi-data'));
		}*/
	}
	
	//Fungsi untuk cetak pdf Ilustrasi Perhitungan Premi Asuransi Properti
	public function cetak_ilustrasi()
	{
		$this->load->library('fpdf');
		
		//Lebar A4= 210mm
		//Margin default= 10mm tiap sisi
		//Bidang Horizontal yang dapat ditulisi= 210 - (10*2) = 190mm
		
		$this->fpdf->AddPage();
		
		//SetFont ke Arial, B, 10pt
		$this->fpdf->SetFont('Arial','B', 10);
		
		//Set Image(string file, x, y , lebar, tinggi, string type, mixed link)
		$this->fpdf->Image(base_url().'assets/images/interface/logo-solusipremi-black.png', 10, 10, 40, 0, 'PNG');
		
		$this->fpdf->SetY(20);
		
		//Cell(lebar, tinggi, teks, border, end line, [align])
		$this->fpdf->Cell(0, 5, 'Ilustrasi Perhitungan Premi Asuransi Properti', 'TB', 1, 'C');
		
		//Hilangkan Bold
		$this->fpdf->SetFont('');
		//SetFont sekarang adalah Arial 10pt
		
		//define standard font size
		$fontSize = 10;

		//define a temporary font size
		$tempFontSize = $fontSize;
		
		/*
		//multicell method
		$cellWidth = 97; //wrapped cell width
		$cellHeight = 5; //normal one-line cell height
		
		$dataPenggunaan = array(
								array(
									$_SESSION['asuransi_properti']['penggunaan_bangunan'].' (kode: '.$_SESSION['asuransi_properti']['kode_okupasi'].')'
									)
								);
		
		foreach ($dataPenggunaan as $itemPenggunaan)
		{
			//check whether the text is overflowing
			if($this->fpdf->GetStringWidth($itemPenggunaan[0]) < $cellWidth)
			{
				//if not, then do nothing
				$line = 1;
			}
			else
			{
				//if it is, then calculate the height needed for wrapped cell
				//by splitting the text to fit the cell width
				//then count how many lines are needed for the text to fit the cell
				
				$textLength = strlen($itemPenggunaan[0]); //total text length
				//$errMargin = 10; //cell width error margin, just in case
				$errMargin = 0;
				$startChar = 0; //character start position for each line
				$maxChar = 0; //maximum character in a line, to be incremented later
				$textArray = array(); //to hold the strings for each line
				$tmpString = ""; //to hold the string for a line (temporary)
				
				while ($startChar < $textLength)
				{
					//loop until end of text
					//loop until maximum character reached
					while($this->fpdf->GetStringWidth($tmpString) < ($cellWidth - $errMargin) && ($startChar + $maxChar) < $textLength)
					{
						$maxChar++;
						$tmpString = substr($itemPenggunaan[0], $startChar, $maxChar);
					}
					//move startChar to next line
					$startChar = $startChar + $maxChar;
					//then add it into the array so we know how many line are needed
					array_push($textArray, $tmpString);
					//reset maxChar and tmpString
					$maxChar = 0;
					$tmpString = '';
				}
				//get number of line
				$line = count($textArray);
			}
		
			//write the cells
			
			$this->fpdf->Cell(90, ($line * $cellHeight), 'Penggunaan Bangunan', 1, 0, 'L');
			$this->fpdf->Cell(3, ($line * $cellHeight), ':', 1, 0, 'C');
			$this->fpdf->MultiCell(97, 5, $itemPenggunaan[0], 1, 'L');
			
			//use MultiCell instead of Cell
			//but first, because MultiCell is always treated as line ending, we need to
			//manually set the xy position for the next cell to be next to it
			//remember the x and y position before writing the multicell
			$xPos = $this->fpdf->GetX();
			$yPos = $this->fpdf->GetY();
			$this->fpdf->MultiCell($cellWidth, $cellHeight, $itemPenggunaan[0], 0, 'L');
			
			//return the position for next cell next to the multicell
			//and offset the x with multicell width
			$this->fpdf->SetXY($xPos + $cellWidth, $yPos);
		}
		*/
		
		$this->fpdf->Cell(90, 5, 'Penggunaan Bangunan', 0, 0, 'L');
		$this->fpdf->Cell(3, 5, ':', 0, 0, 'C');
		$this->fpdf->MultiCell(97, 5, $_SESSION['asuransi_properti']['penggunaan_bangunan'].' (kode: '.$_SESSION['asuransi_properti']['kode_okupasi'].')', 0, 'L');
		
		$this->fpdf->Cell(90, 5, 'Kelas Konstruksi', 0, 0, 'L');
		$this->fpdf->Cell(3, 5, ':', 0, 0, 'C');
		$this->fpdf->Cell(97, 5, $_SESSION['asuransi_properti']['jenis_konstruksi'], 0, 1, 'L');

		$this->fpdf->Cell(90, 5, 'Jumlah Lantai', 0, 0, 'L');
		$this->fpdf->Cell(3, 5, ':', 0, 0, 'C');
		$this->fpdf->Cell(97, 5, $_SESSION['asuransi_properti']['jumlah_lantai'], 0, 1, 'L');

		$this->fpdf->Cell(90, 5, 'Provinsi', 0, 0, 'L');
		$this->fpdf->Cell(3, 5, ':', 0, 0, 'C');
		$this->fpdf->Cell(97, 5, $_SESSION['asuransi_properti']['provinsi'], 0, 1, 'L');

		$this->fpdf->Cell(90, 5, 'Kota atau Kabupaten', 0, 0, 'L');
		$this->fpdf->Cell(3, 5, ':', 0, 0, 'C');
		$this->fpdf->Cell(97, 5, $_SESSION['asuransi_properti']['kab_kota'], 0, 1, 'L');

		$this->fpdf->Cell(90, 5, 'Nilai Bangunan', 0, 0, 'L');
		$this->fpdf->Cell(3, 5, ':', 0, 0, 'C');
		$this->fpdf->Cell(7, 5, 'Rp', 0, 0, 'C');
		$this->fpdf->Cell(90, 5, $_SESSION['asuransi_properti']['nilai_bangunan_str'], 0, 1, 'R');
		
		$this->fpdf->Cell(90, 5, 'Nilai Perabotan', 0, 0, 'L');
		$this->fpdf->Cell(3, 5, ':', 0, 0, 'C');
		$this->fpdf->Cell(7, 5, 'Rp', 0, 0, 'C');
		$this->fpdf->Cell(90, 5, $_SESSION['asuransi_properti']['nilai_perabotan_str'], 0, 1, 'R');
		
		$this->fpdf->Cell(90, 5, 'Nilai Mesin', 0, 0, 'L');
		$this->fpdf->Cell(3, 5, ':', 0, 0, 'C');
		$this->fpdf->Cell(7, 5, 'Rp', 0, 0, 'C');
		$this->fpdf->Cell(90, 5, $_SESSION['asuransi_properti']['nilai_mesin_str'], 0, 1, 'R');
		
		$this->fpdf->Cell(90, 5, 'Nilai Stok', 0, 0, 'L');
		$this->fpdf->Cell(3, 5, ':', 0, 0, 'C');
		$this->fpdf->Cell(7, 5, 'Rp', 0, 0, 'C');
		$this->fpdf->Cell(90, 5, $_SESSION['asuransi_properti']['nilai_stok_str'], 0, 1, 'R');
		
		$this->fpdf->SetFont('Arial', 'B', 10);
		
		$this->fpdf->Cell(90, 5, 'Total Nilai Pertanggungan', 'T', 0, 'L');
		$this->fpdf->Cell(3, 5, ':', 'T', 0, 'C');
		$this->fpdf->Cell(7, 5, 'Rp', 'T', 0, 'C');
		$this->fpdf->Cell(90, 5, $_SESSION['asuransi_properti']['total_pertanggungan_str'], 'T', 1, 'R');
		
		$this->fpdf->Cell(90, 5, 'Rate PAR', 0, 0, 'L');
		$this->fpdf->Cell(3, 5, ':', 0, 0, 'C');
		$this->fpdf->Cell(97, 5, $_SESSION['asuransi_properti']['rate_par_str'], 0, 1, 'R');
		
		$this->fpdf->Cell(10, 5, '', 0, 0, '');
		$this->fpdf->Cell(80, 5, 'FLEXAS', 0, 0, 'L');
		$this->fpdf->Cell(3, 5, ':', 0, 0, 'C');
		$this->fpdf->Cell(97, 5, $_SESSION['asuransi_properti']['permil_flexas_str'], 0, 1, 'R');
		
		$this->fpdf->Cell(10, 5, '', 0, 0, '');
		$this->fpdf->Cell(80, 5, 'TSFWD', 0, 0, 'L');
		$this->fpdf->Cell(3, 5, ':', 0, 0, 'C');
		$this->fpdf->Cell(97, 5, $_SESSION['asuransi_properti']['permil_banjir_str'], 0, 1, 'R');
		
		$this->fpdf->Cell(10, 5, '', 0, 0, '');
		$this->fpdf->Cell(80, 5, 'RSMD CC + OTHERS', 0, 0, 'L');
		$this->fpdf->Cell(3, 5, ':', 0, 0, 'C');
		$this->fpdf->Cell(97, 5, $_SESSION['asuransi_properti']['permil_rsmd_cc_str'], 0, 1, 'R');
		
		$this->fpdf->Cell(90, 5, 'Rate EARTHQUAKE (ZONA '.$_SESSION['asuransi_properti']['ket_zona_gempa'].')', 'B', 0, 'L');
		$this->fpdf->Cell(3, 5, ':', 'B', 0, 'C');
		$this->fpdf->Cell(97, 5, $_SESSION['asuransi_properti']['permil_gempa_str'].' (jumlah lantai '.$_SESSION['asuransi_properti']['ket_jml_lantai'].')', 'B', 1, 'R');
		
		$this->fpdf->Ln();
		
		$this->fpdf->Cell(0, 5, 'PREMIUM CALCULATION FOR PAR', 'TB', 1, 'C');
		
		//Hilangkan Bold
		$this->fpdf->SetFont('');
		//SetFont sekarang adalah Arial 10pt
		
		$this->fpdf->Cell(90, 5, 'Rp'.$_SESSION['asuransi_properti']['total_pertanggungan_str'].' x '.$_SESSION['asuransi_properti']['rate_par_str'], 0, 0, 'L');
		$this->fpdf->Cell(3, 5, ':', 0, 0, 'C');
		$this->fpdf->Cell(7, 5, 'Rp', 0, 0, 'C');
		$this->fpdf->Cell(90, 5, $_SESSION['asuransi_properti']['premi_par_str'], 0, 1, 'R');
		
		$this->fpdf->Cell(90, 5, 'Administration & Policy Cost', 0, 0, 'L');
		$this->fpdf->Cell(3, 5, ':', 0, 0, 'C');
		$this->fpdf->Cell(7, 5, 'Rp', 0, 0, 'C');
		$this->fpdf->Cell(90, 5, $_SESSION['asuransi_properti']['biaya_polis_materai_par_str'], 0, 1, 'R');
		
		$this->fpdf->Cell(90, 5, 'TOTAL', 'TB', 0, 'L');
		$this->fpdf->Cell(3, 5, ':', 'TB', 0, 'C');
		$this->fpdf->Cell(7, 5, 'Rp', 'TB', 0, 'C');
		$this->fpdf->Cell(90, 5, $_SESSION['asuransi_properti']['total_premi_par_str'], 'TB', 1, 'R');
		
		$this->fpdf->Ln();
		
		$this->fpdf->SetFont('Arial', 'B', 10);
		
		$this->fpdf->Cell(0, 5, 'PREMIUM CALCULATION FOR EQ', 'TB', 1, 'C');
		
		//Hilangkan Bold
		$this->fpdf->SetFont('');
		//SetFont sekarang adalah Arial 10pt
		
		$this->fpdf->Cell(90, 5, 'Rp'.$_SESSION['asuransi_properti']['total_pertanggungan_str'].' x '.$_SESSION['asuransi_properti']['permil_gempa_str'], 0, 0, 'L');
		$this->fpdf->Cell(3, 5, ':', 0, 0, 'C');
		$this->fpdf->Cell(7, 5, 'Rp', 0, 0, 'C');
		$this->fpdf->Cell(90, 5, $_SESSION['asuransi_properti']['premi_gempa_str'], 0, 1, 'R');
		
		$this->fpdf->Cell(90, 5, 'Administration & Policy Cost', 0, 0, 'L');
		$this->fpdf->Cell(3, 5, ':', 0, 0, 'C');
		$this->fpdf->Cell(7, 5, 'Rp', 0, 0, 'C');
		$this->fpdf->Cell(90, 5, $_SESSION['asuransi_properti']['biaya_polis_materai_gempa_str'], 0, 1, 'R');
		
		$this->fpdf->Cell(90, 5, 'TOTAL', 'TB', 0, 'L');
		$this->fpdf->Cell(3, 5, ':', 'TB', 0, 'C');
		$this->fpdf->Cell(7, 5, 'Rp', 'TB', 0, 'C');
		$this->fpdf->Cell(90, 5, $_SESSION['asuransi_properti']['total_premi_gempa_str'], 'TB', 1, 'R');
		
		$this->fpdf->Output('Ilustrasi Perhitungan Premi Asuransi Properti.pdf', 'I');
	}
	
	//Fungsi untuk menampilkan isi modal berupa detail produk pada pencarian asuransi properti
	public function view_detail()
	{
		if($_POST['id'])
		{
			$id = $_POST['id'];
			
			//Panggil Model untuk koneksi ke database
			//$this->load->model('asuransi_properti_model');
			
			$view = $this->asuransi_properti_model->get_detail($id);
			
			if(isset($view))
			{
				echo '<h3>Basic Coverage</h3>
				<p>'.$view['basic_coverage'].'</p>
				<h3>Fitur</h3>
				<p>'.$view['fitur'].'</p>
				<h3>Deductible</h3>
				<p>'.$view['deductible'].'</p>';
			}
		}
	}
	
	//Fungsi untuk autocomplete pada Penggunaan Bangunan
	public function get_nama_okupasi()
	{
		if (isset($_GET['term'])) {
            $result = $this->asuransi_properti_model->get_okupasi($_GET['term']);
            if (count($result) > 0) {
            foreach ($result as $row)
                $arr_result[] = $row->keterangan;
                echo json_encode($arr_result);
            }
        }
	}
}
/* End of file Asuransi_properti.php
 Location: ./application/controllers/Asuransi_properti.php */