<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tentang_solusipremi extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
	}
	
	//Fungsi untuk menampilkan halaman Partner Kami
	public function partner_kami($partner = '')
	{
		//Jika slug nama partner tidak ada pada URL
		if ($partner == '')
		{
			$data['title'] = 'Partner Kami | solusipremi.com';
			
			$this->load->view('templates/header', $data);
			$this->load->view('partner_kami');
			$this->load->view('templates/footer');
		}
		else
		{
			//Jika slug nama partner ada pada URL
			//Panggil model 'Partner_model.php' untuk mendapatkan data partner
			$this->load->model('partner_model');
			$partner_item = $this->partner_model->get_partner($partner);
			
			//Jika data partner pada $partner_item tidak ada
			if (empty($partner_item))
			{
				show_404();
			}
			else
			{
				//Jika data partner pada $partner_item ada, maka tampilkan
				$data['title'] = $partner_item['nama_partner'].' | solusipremi.com';
				
				$data['nama_partner'] = $partner_item['nama_partner'];
				$data['nama_gambar'] = $partner_item['nama_gambar'];
				$data['tentang_partner'] = $partner_item['tentang_partner'];
			
				$this->load->view('templates/header', $data);
				$this->load->view('templates/partner_view', $data);
				$this->load->view('partner_solusipremi');
				$this->load->view('templates/footer');
			}
		}
	}
	
	//Fungsi untuk menampilkan halaman Produk Kami
	public function produk_kami()
	{
		$data['title'] = 'Produk Kami | solusipremi.com';
		
		$this->load->view('templates/header', $data);
		$this->load->view('produk_kami');
		$this->load->view('partner_solusipremi');
		$this->load->view('templates/footer');
	}
	
	//Fungsi untuk menampilkan halaman FAQ
	public function faq()
	{
		$data['title'] = 'FAQ | solusipremi.com';
		
		$this->load->view('templates/header', $data);
		$this->load->view('faq');
		$this->load->view('partner_solusipremi');
		$this->load->view('templates/footer');
	}
	
	//Fungsi untuk menampilkan halaman Tentang Kami
	public function tentang_kami()
	{
		$data['title'] = 'Tentang Kami | solusipremi.com';
		
		$this->load->view('templates/header', $data);
		$this->load->view('tentang_kami');
		$this->load->view('partner_solusipremi');
		$this->load->view('templates/footer');
	}
	
	//Fungsi untuk menampilkan halaman Kontak Kami
	public function kontak_kami()
	{
		$this->load->helper('form');
		$this->load->library('form_validation');
		
		$data['title'] = 'Kontak Kami | solusipremi.com';
		
		$this->form_validation->set_rules('nama', 'Nama', 'trim|required|alpha_numeric_spaces|min_length[3]|max_length[35]');
		$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
		$this->form_validation->set_rules('telepon', 'Nomor Telepon', 'trim|required|numeric|min_length[10]|max_length[12]');
		$this->form_validation->set_rules('minat[]', 'Minat', 'required');
		$this->form_validation->set_rules('pesan', 'Pesan', 'trim|required');

		if ($this->form_validation->run() === FALSE)
		{
			//Jika ada kesalahan pengisian, tampilkan form isian kontak
			
			$this->load->view('templates/header', $data);
			$this->load->view('kontak_kami');
			$this->load->view('partner_solusipremi');
			$this->load->view('templates/footer');
		}
		else
		{
			//Jika tidak ada kesalahan, simpan data ke database lewat file model Kontak_model.php
			//Panggil model Kontak_model.php untuk koneksi ke tabel kontak_kami di database
			$this->load->model('kontak_model');
			$this->kontak_model->simpan_kontak();
			
			//Tampilkan pesan berhasil dikirim
			$data['title'] = 'Pesan Berhasil Terkirim | solusipremi.com';
			
			$this->load->view('templates/header', $data);
			$this->load->view('pesan_terkirim');
			$this->load->view('partner_solusipremi');
			$this->load->view('templates/footer');
			
		}
	}
	
	//Fungsi untuk menampilkan halaman Syarat dan Ketentuan
	public function syarat_ketentuan()
	{
		$data['title'] = 'Syarat dan Ketentuan | solusipremi.com';
		
		$this->load->view('templates/header', $data);
		$this->load->view('syarat_ketentuan');
		$this->load->view('partner_solusipremi');
		$this->load->view('templates/footer');
	}
	
	//Fungsi untuk menampilkan halaman Kebijakan Privasi
	public function kebijakan_privasi()
	{
		$data['title'] = 'Kebijakan Privasi | solusipremi.com';
		
		$this->load->view('templates/header', $data);
		$this->load->view('kebijakan_privasi');
		$this->load->view('partner_solusipremi');
		$this->load->view('templates/footer');
	}
}
/* End of file Tentang_solusipremi.php
 Location: ./application/controllers/Tentang_solusipremi.php */