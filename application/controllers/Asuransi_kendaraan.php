<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Asuransi_kendaraan extends CI_Controller
{
    //Variabel berisi array
    protected $harga_kendaraan;
    protected $perlengkapan_nonstd;
    protected $total_harga_pertanggungan;
    protected $premi_dasar;
    protected $loading_premi;
    protected $premi_banjir;
    protected $premi_gempa;
    protected $premi_huruhara;
    protected $premi_terorisme;
    protected $premi_tjh;
    protected $premi_pad;
    protected $premi_pap;
    protected $limit_tjh;
    protected $limit_pad;
    protected $limit_pap;
    protected $total_premi;
    protected $biaya_polis_materai;
    protected $total_bayar_setahun;
    protected $persen_usia;
    protected $persen_premi;
    protected $persen_banjir;
    protected $persen_gempa;
    protected $persen_huruhara;
    protected $persen_terorisme;
    protected $persen_pad;
    protected $persen_pap;

    public function __construct()
    {
        parent::__construct();
        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->load->library('fungsi');
    }

    public function index()
    {
        $data['title'] = 'Asuransi Kendaraan | solusipremi.com';

        $this->load->view('templates/header', $data);
        $this->load->view('asuransi_kendaraan');
        $this->load->view('partner_solusipremi');
        $this->load->view('templates/footer');
    }

    public function cari_asuransi_kendaraan()
    {
        $this->form_validation->set_rules('harga_kendaraan', 'Harga Kendaraan', 'trim|required');
        $this->form_validation->set_rules('perlengkapan_nonstd', 'Perlengkapan Tambahan Nonstandar', 'trim|callback_cek_nilai_perlengkapan');
        $this->form_validation->set_rules('limit_tjh', 'Limit TJH', 'trim|callback_cek_input_tjh');
        $this->form_validation->set_rules('limit_pad', 'Limit PAD', 'trim|callback_cek_input_pad');
        $this->form_validation->set_rules('limit_pap', 'Limit PAP', 'trim|callback_cek_input_pap');
        $this->form_validation->set_rules('jml_seat', 'Jumlah Seat', 'trim|callback_cek_jml_seat');

        if ($this->form_validation->run() === false) {
            $this->index();
        } else {
            $data['title'] = 'Cari Asuransi Kendaraan | solusipremi.com';

            //Definisi variabel untuk perluasan jaminan
            $this->persen_banjir['desimal'] = 0;
            $this->persen_gempa['desimal'] = 0;
            $this->persen_huruhara['desimal'] = 0;
            $this->persen_terorisme['desimal'] = 0;
            $persen_tjh1 = 0;
            $persen_tjh2 = 0;
            $persen_tjh3 = 0;
            $this->persen_pad['desimal'] = 0;
            $this->persen_pap['desimal'] = 0;
            $this->limit_tjh['angka'] = 0;
            $this->limit_pad['angka'] = 0;
            $this->limit_pap['angka'] = 0;
            $jml_seat = 0;

            //Tampung input nilai berupa string angka yang di-post dari form
            //Konversikan ke angka untuk perhitungan
            $this->harga_kendaraan['angka'] = $this->fungsi->convert_to_number($this->input->post('harga_kendaraan'));
            $this->perlengkapan_nonstd['angka'] = $this->fungsi->convert_to_number($this->input->post('perlengkapan_nonstd'));

            //'Total Harga Pertanggungan' merupakan total dari:
            //Harga Pertanggungan (Uang Pertanggungan) Kendaraan Bermotor + Perlengkapan Tambahan Nonstandar
            $this->total_harga_pertanggungan['angka'] = $this->harga_kendaraan['angka'] + $this->perlengkapan_nonstd['angka'];

            //Jenis Pertanggungan (Jika tidak dipilih, defaultnya '1' 'Comprehensive')
            //value="1" selected="selected" Comprehensive
            //value="2" Total Loss Only (TLO)
            //Variabel penampungnya adalah $kode_pertanggungan
            $kode_pertanggungan = $this->input->post('jenis_pertanggungan');

            //Baca value dari $kode_pertanggungan untuk memberi nama jenis pertanggungan
            if ($kode_pertanggungan === '1') {
                $jenis_pertanggungan = 'Comprehensive';
            } else {
                $jenis_pertanggungan = 'Total Loss Only (TLO)';
            }

            //Jenis Kendaraan (Jika tidak dipilih, defaultnya '1' yaitu 'Penumpang (non Truk/Pick-Up/Bus)')
            //value="1" selected="selected" Penumpang (non Truk/Pick-Up/Bus)
            //value="2" Truk/Pick-Up
            //value="3" Bus
            //value="4" Roda Dua (Sepeda Motor)
            //Variabel penampungnya adalah $kode_kendaraan
            $kode_kendaraan = $this->input->post('jenis_kendaraan');

            //Baca value dari $kode_kendaraan untuk memberi nama jenis kendaraan
            if ($kode_kendaraan === '1') {
                $jenis_kendaraan = 'Penumpang (non Truk/Pick-Up/Bus)';
            } elseif ($kode_kendaraan === '2') {
                $jenis_kendaraan = 'Truk/Pick-Up';
            } elseif ($kode_kendaraan === '3') {
                $jenis_kendaraan = 'Bus';
            } else {
                $jenis_kendaraan = 'Roda Dua (Sepeda Motor)';
            }

            //Tahun Kendaraan
            $tahun_kendaraan = $this->input->post('tahun');

            //Perhitungan untuk persen usia kendaraan
            //Jika jenis pertanggungan adalah '1' (Comprehensive)
            if ($kode_pertanggungan === '1') {
                //Ambil tahun sekarang
                $tahun_sekarang = date('Y');
                //Hitung usia kendaraan atau selisih tahunnya
                $selisih_tahun = $tahun_sekarang - $tahun_kendaraan;

                //Jika usia kendaraan atau selisih tahun lebih dari 5 tahun, maka dikenakan 5% setiap kelebihan tahunnya
                if ($selisih_tahun > 5) {
                    $kelebihan_tahun = $selisih_tahun - 5;
                    $this->persen_usia['desimal'] = $kelebihan_tahun * 5 / 100;
                } else {
                    $this->persen_usia['desimal'] = 0;
                }
            } else { //Jika jenis pertanggungan bukan Comprehensive
                $this->persen_usia['desimal'] = 0;
            }

            //Pelat Nomor. Value-nya untuk menentukan wilayah nantinya (Jika tidak dipilih, defaultnya '12' atau pelat 'A')
            $id_pelat = $this->input->post('pelat_nomor');

            //Jumlah seat yang dicover untuk Premi PAP
            $jml_seat = $this->input->post('jml_seat');

            //Panggil model 'Asuransi_kendaraan_model.php' untuk koneksi ke database
            $this->load->model('asuransi_kendaraan_model');

            //Dapatkan kategori kendaraan dan persen premi pada wilayahnya
            //berdasarkan jenis pertanggungan, jenis kendaraan, dan total harga pertanggungan
            $kategori = $this->asuransi_kendaraan_model->get_pertanggungan($kode_pertanggungan, $kode_kendaraan, $this->total_harga_pertanggungan['angka']);

            //Dapatkan wilayah kendaraan berdasarkan value pelat nomor
            $wilayah = $this->asuransi_kendaraan_model->get_wilayah($id_pelat);

            //Ambil kode wilayah dan simpan ke variabel $kode_wilayah
            $kode_wilayah = $wilayah['wilayah'];

            //Ambil nama kota dan simpan ke variabel $nama_kota
            $nama_kota = $wilayah['kota'];

            if ($kode_wilayah === '1') {
                $this->persen_premi['desimal'] = $kategori['wil1_bb']; //Persen premi batas bawah pada wilayah 1
            } elseif ($kode_wilayah === '2') {
                $this->persen_premi['desimal'] = $kategori['wil2_bb']; //Persen premi batas bawah pada wilayah 2
            } else {
                $this->persen_premi['desimal'] = $kategori['wil3_bb']; //Persen premi batas bawah pada wilayah 3
            }

            //Hitung premi_dasar
            $this->premi_dasar['angka'] = $this->total_harga_pertanggungan['angka'] * $this->persen_premi['desimal'];

            //Hitung loading_premi
            $this->loading_premi['angka'] = $this->premi_dasar['angka'] * $this->persen_usia['desimal'];

            //Tampung value dari perluasan jaminan ke variabel $kode_perluasan
            $kode_perluasan = $this->input->post('perluasan[]');

            //Periksa apakah ada perluasan yang dipilih
            if (isset($kode_perluasan)) {
                //Jika ada, periksa setiap indeks array pada $kode_perluasan dengan looping foreach
                foreach ($kode_perluasan as $kode_jaminan):

                    if ($kode_jaminan === '1') { //Jika jaminan 'Banjir termasuk Angin Topan' dipilih
                        $perluasan_dipilih[] = 'Banjir termasuk Angin Topan';

                        //Dapatkan jaminan 'Banjir termasuk Angin Topan' berdasarkan wilayah
                        $jaminan_banjir = $this->asuransi_kendaraan_model->get_jaminan_banjir($kode_wilayah);

                        if ($kode_pertanggungan === '1') {
                            $this->persen_banjir['desimal'] = $jaminan_banjir['tarif_premi_comprehensive'];
                        } else {
                            $this->persen_banjir['desimal'] = $jaminan_banjir['tarif_premi_tlo'];
                        }
                    } elseif ($kode_jaminan === '2') { //Jika jaminan 'Gempa Bumi, Tsunami' dipilih
                        $perluasan_dipilih[] = 'Gempa Bumi, Tsunami';

                        //Dapatkan jaminan 'Gempa Bumi, Tsunami' berdasarkan wilayah
                        $jaminan_gempa = $this->asuransi_kendaraan_model->get_jaminan_gempa($kode_wilayah);

                        if ($kode_pertanggungan === '1') {
                            $this->persen_gempa['desimal'] = $jaminan_gempa['tarif_premi_comprehensive'];
                        } else {
                            $this->persen_gempa['desimal'] = $jaminan_gempa['tarif_premi_tlo'];
                        }
                    } elseif ($kode_jaminan === '3') { //Jika jaminan 'Huru-hara dan Kerusuhan (SRCC)' dipilih
                        $perluasan_dipilih[] = 'Huru-hara dan Kerusuhan (SRCC)';

                        //Dapatkan jaminan 'Huru-hara dan Kerusuhan (SRCC)'
                        $jaminan_huruhara = $this->asuransi_kendaraan_model->get_jaminan_huruhara();

                        if ($kode_pertanggungan === '1') {
                            $this->persen_huruhara['desimal'] = $jaminan_huruhara['tarif_premi_comprehensive'];
                        } else {
                            $this->persen_huruhara['desimal'] = $jaminan_huruhara['tarif_premi_tlo'];
                        }
                    } elseif ($kode_jaminan === '4') { //Jika jaminan 'Terorisme dan Sabotase (TS)' dipilih
                        $perluasan_dipilih[] = 'Terorisme dan Sabotase (TS)';

                        //Dapatkan jaminan 'Terorisme dan Sabotase (TS)'
                        $jaminan_terorisme = $this->asuransi_kendaraan_model->get_jaminan_terorisme();

                        if ($kode_pertanggungan === '1') {
                            $this->persen_terorisme['desimal'] = $jaminan_terorisme['tarif_premi_comprehensive'];
                        } else {
                            $this->persen_terorisme['desimal'] = $jaminan_terorisme['tarif_premi_tlo'];
                        }
                    } elseif ($kode_jaminan === '5') { //Jika jaminan 'Tanggung Jawab Hukum Terhadap Pihak Ketiga' dipilih
                        $perluasan_dipilih[] = 'Tanggung Jawab Hukum Terhadap Pihak Ketiga (TJH)';

                        //Ambil Limit TJH yang di-post dan konversikan ke angka
                        $this->limit_tjh['angka'] = $this->fungsi->convert_to_number($this->input->post('limit_tjh'));

                        //Tentukan jaminan tjh berdasarkan jenis kendaraan
                        if ($kode_kendaraan === '1' || $kode_kendaraan === '4') { //Jika jenis kendaraan adalah 'Penumpang' atau 'Sepeda Motor'
                            //Dapatkan jaminan tjh
                            $jaminan_tjh = $this->asuransi_kendaraan_model->get_jaminan_tjh($id_tjh = 1); //Kendaraan Penumpang dan Sepeda Motor
                        } else { //Jika bukan, maka jenis kendaraan adalah 'Truk/Pick-Up' atau 'Bus'
                            $jaminan_tjh = $this->asuransi_kendaraan_model->get_jaminan_tjh($id_tjh = 2); //Kendaraan Niaga, Truk, dan Bus
                        }

                        //Ambil persen tjh berdasarkan Limit TJH untuk perhitungan progresif
                        if ($this->limit_tjh['angka'] >= 0 && $this->limit_tjh['angka'] <= 25000000) { //Limit tjh hingga Rp25 juta
                            $persen_tjh1 = $jaminan_tjh['up_a'];
                        } elseif ($this->limit_tjh['angka'] > 25000000 && $this->limit_tjh['angka'] <= 50000000) { //Limit tjh > Rp25 juta s.d. Rp50 juta
                            $persen_tjh1 = $jaminan_tjh['up_a'];
                            $persen_tjh2 = $jaminan_tjh['up_b'];
                        } elseif ($this->limit_tjh['angka'] > 50000000 && $this->limit_tjh['angka'] <= 100000000) { //Limit tjh > Rp50 juta s.d. Rp100 juta
                            $persen_tjh1 = $jaminan_tjh['up_a'];
                            $persen_tjh2 = $jaminan_tjh['up_b'];
                            $persen_tjh3 = $jaminan_tjh['up_c'];
                        }
                    } elseif ($kode_jaminan === '6') { //Jika jaminan 'Kecelakaan Diri untuk Pengemudi (PAD)' dipilih
                        $perluasan_dipilih[] = 'Kecelakaan Diri untuk Pengemudi (PAD)';

                        //Ambil Limit PAD yang di-post dan konversikan ke angka
                        $this->limit_pad['angka'] = $this->fungsi->convert_to_number($this->input->post('limit_pad'));

                        //Dapatkan jaminan 'Kecelakaan Diri untuk Pengemudi (PAD)'
                        $jaminan_pad = $this->asuransi_kendaraan_model->get_jaminan_kecelakaan_diri();
                        $this->persen_pad['desimal'] = $jaminan_pad['pengemudi'];
                    } elseif ($kode_jaminan === '7') { //Jika jaminan 'Kecelakaan Diri untuk Penumpang (PAP)' dipilih
                        $perluasan_dipilih[] = 'Kecelakaan Diri untuk Penumpang (PAP)';

                        //Ambil Limit PAP yang di-post dan konversikan ke angka
                        $this->limit_pap['angka'] = $this->fungsi->convert_to_number($this->input->post('limit_pap'));

                        //Dapatkan jaminan 'Kecelakaan Diri untuk Penumpang (PAP)'
                        $jaminan_pap = $this->asuransi_kendaraan_model->get_jaminan_kecelakaan_diri();
                        $this->persen_pap['desimal'] = $jaminan_pap['penumpang'];
                    }

                endforeach; //Akhir looping foreach()
            } else { //Jika Perluasan jaminan sama sekali tidak dipilih
                $this->persen_banjir['desimal'] = 0;
                $this->persen_gempa['desimal'] = 0;
                $this->persen_huruhara['desimal'] = 0;
                $this->persen_terorisme['desimal'] = 0;
                $persen_tjh1 = 0;
                $persen_tjh2 = 0;
                $persen_tjh3 = 0;
                $this->persen_pad['desimal'] = 0;
                $this->persen_pap['desimal'] = 0;
                $this->limit_tjh['angka'] = 0;
                $this->limit_pad['angka'] = 0;
                $this->limit_pap['angka'] = 0;
                $jml_seat = 0;
                $perluasan_dipilih[] = 'Tidak Ada';
            }
            //end if(isset($kode_perluasan))

            //Lakukan perhitungan untuk premi perluasan.
            //Hitung Premi 'Banjir termasuk Angin Topan'
            $this->premi_banjir['angka'] = $this->total_harga_pertanggungan['angka'] * $this->persen_banjir['desimal'];

            //Hitung Premi 'Gempa Bumi, Tsunami'
            $this->premi_gempa['angka'] = $this->total_harga_pertanggungan['angka'] * $this->persen_gempa['desimal'];

            //Hitung Premi 'Huru-hara dan Kerusuhan (SRCC)'
            $this->premi_huruhara['angka'] = $this->total_harga_pertanggungan['angka'] * $this->persen_huruhara['desimal'];

            //Hitung Premi 'Terorisme dan Sabotase (TS)'
            $this->premi_terorisme['angka'] = $this->total_harga_pertanggungan['angka'] * $this->persen_terorisme['desimal'];

            //Penetapan Tarif Premi atau Kontribusi untuk Asuransi Kendaraan Bermotor dengan
            //penambahan manfaat berupa perluasan jaminan risiko dihitung secara progresif.
            //Hitung Premi 'TJH' secara progresif
            if ($this->limit_tjh['angka'] >= 0 && $this->limit_tjh['angka'] <= 25000000) { //Limit TJH hingga Rp25 juta
                //Hitung Premi TJH untuk Limit TJH hingga Rp25 juta
                $this->premi_tjh['angka'] = $persen_tjh1 * $this->limit_tjh['angka'];
            } elseif ($this->limit_tjh['angka'] > 25000000 && $this->limit_tjh['angka'] <= 50000000) { //Limit TJH > Rp25 juta s.d. Rp50 juta
                //Hitung Premi TJH untuk Limit TJH > Rp25juta s.d. Rp50juta
                $this->premi_tjh['angka'] = ($persen_tjh1 * 25000000) + ($persen_tjh2 * ($this->limit_tjh['angka'] - 25000000));
            } elseif ($this->limit_tjh['angka'] > 50000000 && $this->limit_tjh['angka'] <= 100000000) { //Limit TJH > Rp50 juta s.d. Rp100 juta
                //Hitung Premi TJH untuk Limit TJH > Rp50 juta s.d. Rp100 juta
                $this->premi_tjh['angka'] = ($persen_tjh1 * 25000000) + ($persen_tjh2 * 25000000) + ($persen_tjh3 * ($this->limit_tjh['angka'] - 50000000));
            } else { //Jika Limit TJH lebih dari Rp100 juta atau angka lainnya
                $this->premi_tjh['angka'] = 0;
            }

            //Hitung Premi 'Kecelakaan Diri untuk Pengemudi (PAD)'
            $this->premi_pad['angka'] = $this->limit_pad['angka'] * $this->persen_pad['desimal'];

            //Hitung Premi 'Kecelakaan Diri untuk Penumpang (PAP)'
            $this->premi_pap['angka'] = $this->limit_pap['angka'] * $this->persen_pap['desimal'] * $jml_seat;

            //Totalkan Premi Setahun (belum termasuk biaya polis)
            $this->total_premi['angka'] = $this->premi_dasar['angka'] + $this->loading_premi['angka'] + $this->premi_banjir['angka'] + $this->premi_gempa['angka'] + $this->premi_huruhara['angka'] + $this->premi_terorisme['angka'] + $this->premi_tjh['angka'] + $this->premi_pad['angka'] + $this->premi_pap['angka'];

            //Hitung Biaya Polis dan Materai berdasarkan Total Premi Setahun
            $this->biaya_polis_materai['angka'] = $this->fungsi->hitung_biaya_polis($this->total_premi['angka']);

            //Total bayar setahun adalah $total_premi + $biaya_polis_materai
            $this->total_bayar_setahun['angka'] = $this->total_premi['angka'] + $this->biaya_polis_materai['angka'];

            //Konversikan angka dan persen untuk tampilan
            $this->harga_kendaraan['string'] = $this->fungsi->format_angka($this->harga_kendaraan['angka']);
            $this->perlengkapan_nonstd['string'] = $this->fungsi->format_angka($this->perlengkapan_nonstd['angka']);
            $this->total_harga_pertanggungan['string'] = $this->fungsi->format_angka($this->total_harga_pertanggungan['angka']);
            $this->premi_dasar['string'] = $this->fungsi->format_angka($this->premi_dasar['angka']);
            $this->loading_premi['string'] = $this->fungsi->format_angka($this->loading_premi['angka']);
            $this->premi_banjir['string'] = $this->fungsi->format_angka($this->premi_banjir['angka']);
            $this->premi_gempa['string'] = $this->fungsi->format_angka($this->premi_gempa['angka']);
            $this->premi_huruhara['string'] = $this->fungsi->format_angka($this->premi_huruhara['angka']);
            $this->premi_terorisme['string'] = $this->fungsi->format_angka($this->premi_terorisme['angka']);
            $this->premi_tjh['string'] = $this->fungsi->format_angka($this->premi_tjh['angka']);
            $this->premi_pad['string'] = $this->fungsi->format_angka($this->premi_pad['angka']);
            $this->premi_pap['string'] = $this->fungsi->format_angka($this->premi_pap['angka']);
            $this->total_premi['string'] = $this->fungsi->format_angka($this->total_premi['angka']);
            $this->biaya_polis_materai['string'] = $this->fungsi->format_angka($this->biaya_polis_materai['angka']);
            $this->total_bayar_setahun['string'] = $this->fungsi->format_angka($this->total_bayar_setahun['angka']);
            $this->limit_tjh['string'] = $this->fungsi->format_angka($this->limit_tjh['angka']);
            $this->limit_pad['string'] = $this->fungsi->format_angka($this->limit_pad['angka']);
            $this->limit_pap['string'] = $this->fungsi->format_angka($this->limit_pap['angka']);

            $this->persen_premi['string'] = $this->fungsi->konversi_str_persen($this->persen_premi['desimal']);
            $this->persen_usia['string'] = $this->fungsi->konversi_str_persen($this->persen_usia['desimal']);
            $this->persen_banjir['string'] = $this->fungsi->konversi_str_persen($this->persen_banjir['desimal']);
            $this->persen_gempa['string'] = $this->fungsi->konversi_str_persen($this->persen_gempa['desimal']);
            $this->persen_huruhara['string'] = $this->fungsi->konversi_str_persen($this->persen_huruhara['desimal']);
            $this->persen_terorisme['string'] = $this->fungsi->konversi_str_persen($this->persen_terorisme['desimal']);
            $this->persen_pad['string'] = $this->fungsi->konversi_str_persen($this->persen_pad['desimal']);
            $this->persen_pap['string'] = $this->fungsi->konversi_str_persen($this->persen_pap['desimal']);

            //Tampilkan ke view
            $data['v_harga_kendaraan'] = $this->harga_kendaraan['string'];
            $data['v_perlengkapan_nonstd'] = $this->perlengkapan_nonstd['string'];
            $data['v_total_harga_pertanggungan'] = $this->total_harga_pertanggungan['string'];

            $data['v_tahun_kendaraan'] = $tahun_kendaraan;
            $data['v_jenis_kendaraan'] = $jenis_kendaraan;
            $data['v_kode_wilayah'] = $kode_wilayah;
            $data['v_nama_kota'] = $nama_kota;
            $data['v_jenis_pertanggungan'] = $jenis_pertanggungan;

            $data['v_premi_dasar'] = $this->premi_dasar['string'];
            $data['v_persen_premi'] = $this->persen_premi['string'];

            $data['v_loading_premi'] = $this->loading_premi['string'];
            $data['v_persen_usia'] = $this->persen_usia['string'];

            $data['v_premi_banjir'] = $this->premi_banjir['string'];
            $data['v_persen_banjir'] = $this->persen_banjir['string'];

            $data['v_premi_gempa'] = $this->premi_gempa['string'];
            $data['v_persen_gempa'] = $this->persen_gempa['string'];

            $data['v_premi_huruhara'] = $this->premi_huruhara['string'];
            $data['v_persen_huruhara'] = $this->persen_huruhara['string'];

            $data['v_premi_terorisme'] = $this->premi_terorisme['string'];
            $data['v_persen_terorisme'] = $this->persen_terorisme['string'];

            $data['v_limit_tjh'] = $this->limit_tjh['string'];
            $data['v_premi_tjh'] = $this->premi_tjh['string'];

            $data['v_limit_pad'] = $this->limit_pad['string'];
            $data['v_premi_pad'] = $this->premi_pad['string'];
            $data['v_persen_pad'] = $this->persen_pad['string'];

            $data['v_limit_pap'] = $this->limit_pap['string'];
            $data['v_premi_pap'] = $this->premi_pap['string'];

            if ($this->premi_pap['angka'] != 0) { //Jika Premi PAP tidak sama dengan kosong (ada nilai preminya)
                $data['align'] = 'left';
                $keterangan_pap = $this->persen_pap['string'].'; berlaku untuk maksimum '.$jml_seat.' orang';
            } else {
                $data['align'] = 'right';
                $keterangan_pap = $this->persen_pap['string'].'';
            }

            $data['v_persen_pap_ket'] = $keterangan_pap;
            $data['v_total_premi'] = $this->total_premi['string'];
            $data['v_biaya_polis_materai'] = $this->biaya_polis_materai['string'];
            $data['v_total_bayar_setahun'] = $this->total_bayar_setahun['string'];

            //Buatkan Session untuk penyimpanan hasil pencarian asuransi kendaraan secara global
            $_SESSION['asuransi_kendaraan'] = array(
                                                    'tahun_kendaraan' => $tahun_kendaraan,
                                                    'jenis_kendaraan' => $jenis_kendaraan,
                                                    'kode_wilayah' => $kode_wilayah,
                                                    'nama_kota' => $nama_kota,
                                                    'jenis_pertanggungan' => $jenis_pertanggungan,
                                                    'jml_seat' => $jml_seat,
                                                    'harga_kendaraan_str' => $this->harga_kendaraan['string'],
                                                    'perlengkapan_nonstd_str' => $this->perlengkapan_nonstd['string'],
                                                    'total_harga_pertanggungan_str' => $this->total_harga_pertanggungan['string'],
                                                    'premi_dasar_str' => $this->premi_dasar['string'],
                                                    'persen_premi_str' => $this->persen_premi['string'],
                                                    'loading_premi_str' => $this->loading_premi['string'],
                                                    'persen_usia_str' => $this->persen_usia['string'],
                                                    'premi_banjir_str' => $this->premi_banjir['string'],
                                                    'persen_banjir_str' => $this->persen_banjir['string'],
                                                    'premi_gempa_str' => $this->premi_gempa['string'],
                                                    'persen_gempa_str' => $this->persen_gempa['string'],
                                                    'premi_huruhara_str' => $this->premi_huruhara['string'],
                                                    'persen_huruhara_str' => $this->persen_huruhara['string'],
                                                    'premi_terorisme_str' => $this->premi_terorisme['string'],
                                                    'persen_terorisme_str' => $this->persen_terorisme['string'],
                                                    'limit_tjh_str' => $this->limit_tjh['string'],
                                                    'premi_tjh_str' => $this->premi_tjh['string'],
                                                    'limit_pad_str' => $this->limit_pad['string'],
                                                    'premi_pad_str' => $this->premi_pad['string'],
                                                    'persen_pad_str' => $this->persen_pad['string'],
                                                    'limit_pap_str' => $this->limit_pap['string'],
                                                    'premi_pap_str' => $this->premi_pap['string'],
                                                    'persen_pap_str' => $this->persen_pap['string'],
                                                    'keterangan_pap' => $keterangan_pap,
                                                    'total_premi_str' => $this->total_premi['string'],
                                                    'biaya_polis_materai_str' => $this->biaya_polis_materai['string'],
                                                    'total_bayar_setahun_str' => $this->total_bayar_setahun['string'],
                                                    'harga_kendaraan' => $this->harga_kendaraan['angka'],
                                                    'perlengkapan_nonstd' => $this->perlengkapan_nonstd['angka'],
                                                    'total_harga_pertanggungan' => $this->total_harga_pertanggungan['angka'],
                                                    'premi_dasar' => $this->premi_dasar['angka'],
                                                    'persen_premi' => $this->persen_premi['desimal'],
                                                    'loading_premi' => $this->loading_premi['angka'],
                                                    'persen_usia' => $this->persen_usia['desimal'],
                                                    'premi_banjir' => $this->premi_banjir['angka'],
                                                    'persen_banjir' => $this->persen_banjir['desimal'],
                                                    'premi_gempa' => $this->premi_gempa['angka'],
                                                    'persen_gempa' => $this->persen_gempa['desimal'],
                                                    'premi_huruhara' => $this->premi_huruhara['angka'],
                                                    'persen_huruhara' => $this->persen_huruhara['desimal'],
                                                    'premi_terorisme' => $this->premi_terorisme['angka'],
                                                    'persen_terorisme' => $this->persen_terorisme['desimal'],
                                                    'limit_tjh' => $this->limit_tjh['angka'],
                                                    'premi_tjh' => $this->premi_tjh['angka'],
                                                    'limit_pad' => $this->limit_pad['angka'],
                                                    'premi_pad' => $this->premi_pad['angka'],
                                                    'persen_pad' => $this->persen_pad['desimal'],
                                                    'limit_pap' => $this->limit_pap['angka'],
                                                    'premi_pap' => $this->premi_pap['angka'],
                                                    'persen_pap' => $this->persen_pap['desimal'],
                                                    'total_premi' => $this->total_premi['angka'],
                                                    'biaya_polis_materai' => $this->biaya_polis_materai['angka'],
                                                    'total_bayar_setahun' => $this->total_bayar_setahun['angka'],
                                                    'perluasan_dipilih' => $perluasan_dipilih,
                                                    );

            //Ambil data partner untuk pilihan perusahaan asuransi bagi konsumen
            $data['list_partner'] = $this->asuransi_kendaraan_model->get_partner();
        if (!isset($_SESSION['username'])) { //atau || $_SESSION['logged_in'] != TRUE)
            //Jika belum login, arahkan ke halaman login
            redirect(site_url('auth'));

        }
            //Tampilkan hasil
            $this->load->view('templates/header', $data);
            $this->load->view('cari_asuransi_kendaraan', $data);
            $this->load->view('partner_solusipremi');
            $this->load->view('templates/footer');
        }
    }

    //Fungsi untuk menangani pengisian data
    public function isi_data($id_part)
    {
        //Cek apakah user sudah login dengan memeriksa session
        if (!isset($_SESSION['username'])) { //atau || $_SESSION['logged_in'] != TRUE)
            //Jika belum login, arahkan ke halaman login
            redirect(site_url('auth'));
        }

        //Jika session pencarian asuransi kendaraan tidak ada
        if (!isset($_SESSION['asuransi_kendaraan'])) {
            redirect(site_url('asuransi-kendaraan'));
        }

        if ($id_part == '') {
            show_404();
        }

        $data['title'] = 'Isi Data | solusipremi.com';

        $id_partner = $id_part;

        //Panggil model 'Asuransi_kendaraan_model.php' untuk koneksi ke database
        $this->load->model('asuransi_kendaraan_model');

        //Ambil data partner asuransi yang dipilih
        $partner = $this->asuransi_kendaraan_model->get_partner($id_partner);

        if (empty($partner)) {
            show_404();
        }

        $data['v_id_partner'] = $id_partner;
        $data['v_nama_partner'] = $partner['nama_partner'];

        $data['v_nama_tertanggung'] = $_SESSION['username'].' '.$_SESSION['lastname'];
        $data['v_email'] = $_SESSION['email'];
        $data['v_tahun_pembuatan'] = $_SESSION['asuransi_kendaraan']['tahun_kendaraan'];
        $data['v_jenis_kendaraan'] = $_SESSION['asuransi_kendaraan']['jenis_kendaraan'];

        $pertanggungan_dasar = $_SESSION['asuransi_kendaraan']['jenis_pertanggungan'];
        if ($pertanggungan_dasar == 'Comprehensive') {
            $kondisi_pertanggungan = 'Kerugian Sebagian dan Kerugian Total ['.$pertanggungan_dasar.']';
        } else {
            $kondisi_pertanggungan = 'Kerugian Total ['.$pertanggungan_dasar.']';
        }
        $data['v_jenis_pertanggungan'] = $kondisi_pertanggungan;

        //$perluasan_jaminan menyimpan array yang dikirim dari $_SESSION['asuransi_kendaraan']['perluasan_dipilih']
        $perluasan_jaminan = $_SESSION['asuransi_kendaraan']['perluasan_dipilih'];
        if (isset($perluasan_jaminan)) {
            $str_perluasan = implode('; ', $perluasan_jaminan);
        }
        $data['v_perluasan_jaminan'] = $str_perluasan;

        $data['v_harga_kendaraan'] = $_SESSION['asuransi_kendaraan']['harga_kendaraan_str'];
        $data['v_perlengkapan_nonstd'] = $_SESSION['asuransi_kendaraan']['perlengkapan_nonstd_str'];

        $this->form_validation->set_rules('nomor_ktp', 'Nomor KTP', 'trim|required|numeric');
        $this->form_validation->set_rules('nama_tertanggung', 'Nama Tertanggung', 'trim|required');
        $this->form_validation->set_rules('alamat_tertanggung', 'Alamat Tertanggung', 'trim|required');
        $this->form_validation->set_rules('rt', 'RT', 'trim|required|numeric');
        $this->form_validation->set_rules('rw', 'RW', 'trim|required|numeric');
        $this->form_validation->set_rules('desa_kelurahan', 'Desa atau Kelurahan', 'trim|required');
        $this->form_validation->set_rules('kecamatan', 'Kecamatan', 'trim|required');
        $this->form_validation->set_rules('kab_kota', 'Kabupaten atau Kota', 'trim|required');
        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
        $this->form_validation->set_rules('nomor_telepon', 'Nomor Telepon', 'trim|required|numeric');
        $this->form_validation->set_rules('periode_mulai', 'Periode Mulai', 'trim|required');
        $this->form_validation->set_rules('periode_sampai', 'Periode Sampai', 'trim|required');
        $this->form_validation->set_rules('nama_stnk', 'Nama Pemilik (Sesuai STNK)', 'trim|required');
        $this->form_validation->set_rules('alamat_stnk', 'Alamat Pemilik (Sesuai STNK)', 'trim|required');
        $this->form_validation->set_rules('merek_kendaraan', 'Merek Kendaraan', 'trim|required');
        $this->form_validation->set_rules('tipe_kendaraan', 'Tipe Kendaraan', 'trim|required');
        $this->form_validation->set_rules('tahun_pembuatan', 'Tahun Pembuatan', 'trim|required|integer');
        $this->form_validation->set_rules('warna', 'Warna', 'trim|required');
        $this->form_validation->set_rules('nomor_polisi', 'Nomor Polisi', 'trim|required|alpha_numeric_spaces');
        $this->form_validation->set_rules('nomor_rangka', 'Nomor Rangka', 'trim|required|alpha_numeric');
        $this->form_validation->set_rules('nomor_mesin', 'Nomor Mesin', 'trim|required|alpha_numeric');

        if ($this->form_validation->run() === false) {
            $this->load->view('templates/header', $data);
            $this->load->view('isi_sppkb', $data);
            $this->load->view('partner_solusipremi');
            $this->load->view('templates/footer');
        }

        /*else
        {
            //Panggil model 'Asuransi_kendaraan_model.php' untuk koneksi ke database
            $this->load->model('asuransi_kendaraan_model');

            $simpan = $this->asuransi_kendaraan_model->simpan_pembelian();

            if($simpan)
            {
                $data['title'] = 'Pembelian Asuransi Kendaraan Anda Berhasil | solusipremi.com';

                $this->load->view('templates/header', $data);
                $this->load->view('pembelian_asuransi_kendaraan_berhasil');
                $this->load->view('partner_solusipremi');
                $this->load->view('templates/footer');
            }
        }*/
    }

    //Fungsi untuk menampilkan isi modal berupa detail produk pada pencarian asuransi kendaraan
    public function fitur()
    {
       
            //Panggil Model untuk koneksi ke database
            $this->load->model('asuransi_kendaraan_model');

            $view = $this->asuransi_kendaraan_model->get_detail($id);

           
    }

    //Fungsi untuk cetak pdf Ilustrasi Perhitungan Premi Asuransi Kendaraan
    public function cetak_ilustrasi()
    {
        $this->load->library('fpdf');

        //Lebar A4= 210mm
        //Margin default= 10mm tiap sisi
        //Bidang Horizontal yang dapat ditulisi= 210 - (10*2) = 190mm

        $this->fpdf->AddPage();

        //SetFont ke Arial, B, 10pt
        $this->fpdf->SetFont('Arial', 'B', 10);

        //Set Image(string file, x, y , lebar, tinggi, string type, mixed link)
        $this->fpdf->Image(base_url().'assets/images/interface/logo-solusipremi-black.png', 10, 10, 40, 0, 'PNG');

        $this->fpdf->SetY(20);

        //Cell(lebar, tinggi, teks, border, end line, [align])
        $this->fpdf->Cell(0, 5, 'Ilustrasi Perhitungan Premi Asuransi Kendaraan Bermotor', 'TB', 1, 'C');

        //Hilangkan Bold
        $this->fpdf->SetFont('');
        //SetFont sekarang adalah Arial 10pt

        //define standard font size
        $fontSize = 10;

        //define a temporary font size
        $tempFontSize = $fontSize;

        $this->fpdf->Cell(95, 5, 'Harga Kendaraan', 0, 0, 'L');
        $this->fpdf->Cell(3, 5, ':', 0, 0, 'C');
        $this->fpdf->Cell(7, 5, 'Rp', 0, 0, 'C');
        $this->fpdf->Cell(50, 5, $_SESSION['asuransi_kendaraan']['harga_kendaraan_str'], 0, 0, 'R');
        $this->fpdf->Cell(35, 5, '', 0, 1, '');

        $this->fpdf->Cell(95, 5, 'Perlengkapan Tambahan Nonstandar', 0, 0, 'L');
        $this->fpdf->Cell(3, 5, ':', 0, 0, 'C');
        $this->fpdf->Cell(7, 5, 'Rp', 0, 0, 'C');
        $this->fpdf->Cell(50, 5, $_SESSION['asuransi_kendaraan']['perlengkapan_nonstd_str'], 0, 0, 'R');
        $this->fpdf->Cell(35, 5, '', 0, 1, '');

        $this->fpdf->Cell(95, 5, 'Total Harga Pertanggungan', 0, 0, 'L');
        $this->fpdf->Cell(3, 5, ':', 0, 0, 'C');
        $this->fpdf->Cell(7, 5, 'Rp', 0, 0, 'C');
        $this->fpdf->Cell(50, 5, $_SESSION['asuransi_kendaraan']['total_harga_pertanggungan_str'], 0, 0, 'R');
        $this->fpdf->Cell(35, 5, '', 0, 1, '');

        $this->fpdf->Cell(95, 5, 'Tahun Kendaraan', 0, 0, 'L');
        $this->fpdf->Cell(3, 5, ':', 0, 0, 'C');
        $this->fpdf->Cell(92, 5, $_SESSION['asuransi_kendaraan']['tahun_kendaraan'], 0, 1, 'L');

        $this->fpdf->Cell(95, 5, 'Jenis Kendaraan', 0, 0, 'L');
        $this->fpdf->Cell(3, 5, ':', 0, 0, 'C');
        $this->fpdf->Cell(92, 5, $_SESSION['asuransi_kendaraan']['jenis_kendaraan'], 0, 1, 'L');

        $this->fpdf->Cell(95, 5, 'Wilayah', 0, 0, 'L');
        $this->fpdf->Cell(3, 5, ':', 0, 0, 'C');
        $this->fpdf->MultiCell(92, 5, $_SESSION['asuransi_kendaraan']['kode_wilayah'].' ('.$_SESSION['asuransi_kendaraan']['nama_kota'].')', 0, 'L');

        $this->fpdf->Cell(95, 5, 'Basic Coverage', 0, 0, 'L');
        $this->fpdf->Cell(3, 5, ':', 0, 0, 'C');
        $this->fpdf->Cell(92, 5, $_SESSION['asuransi_kendaraan']['jenis_pertanggungan'], 0, 1, 'L');

        $this->fpdf->Cell(95, 5, 'Premi Basic Coverage', 'T', 0, 'L');
        $this->fpdf->Cell(3, 5, ':', 'T', 0, 'C');
        $this->fpdf->Cell(7, 5, 'Rp', 'T', 0, 'C');
        $this->fpdf->Cell(50, 5, $_SESSION['asuransi_kendaraan']['premi_dasar_str'], 'T', 0, 'R');
        $this->fpdf->Cell(35, 5, $_SESSION['asuransi_kendaraan']['persen_premi_str'], 'T', 1, 'R');

        $this->fpdf->Cell(95, 5, 'Loading Premi (Faktor Usia)', 0, 0, 'L');
        $this->fpdf->Cell(3, 5, ':', 0, 0, 'C');
        $this->fpdf->Cell(7, 5, 'Rp', 0, 0, 'C');
        $this->fpdf->Cell(50, 5, $_SESSION['asuransi_kendaraan']['loading_premi_str'], 0, 0, 'R');
        $this->fpdf->Cell(35, 5, $_SESSION['asuransi_kendaraan']['persen_usia_str'], 0, 1, 'R');

        $this->fpdf->Cell(95, 5, 'Premi Banjir termasuk Angin Topan', 0, 0, 'L');
        $this->fpdf->Cell(3, 5, ':', 0, 0, 'C');
        $this->fpdf->Cell(7, 5, 'Rp', 0, 0, 'C');
        $this->fpdf->Cell(50, 5, $_SESSION['asuransi_kendaraan']['premi_banjir_str'], 0, 0, 'R');
        $this->fpdf->Cell(35, 5, $_SESSION['asuransi_kendaraan']['persen_banjir_str'], 0, 1, 'R');

        $this->fpdf->Cell(95, 5, 'Premi Gempa Bumi, Tsunami', 0, 0, 'L');
        $this->fpdf->Cell(3, 5, ':', 0, 0, 'C');
        $this->fpdf->Cell(7, 5, 'Rp', 0, 0, 'C');
        $this->fpdf->Cell(50, 5, $_SESSION['asuransi_kendaraan']['premi_gempa_str'], 0, 0, 'R');
        $this->fpdf->Cell(35, 5, $_SESSION['asuransi_kendaraan']['persen_gempa_str'], 0, 1, 'R');

        $this->fpdf->Cell(95, 5, 'Premi Huru-hara dan Kerusuhan (SRCC)', 0, 0, 'L');
        $this->fpdf->Cell(3, 5, ':', 0, 0, 'C');
        $this->fpdf->Cell(7, 5, 'Rp', 0, 0, 'C');
        $this->fpdf->Cell(50, 5, $_SESSION['asuransi_kendaraan']['premi_huruhara_str'], 0, 0, 'R');
        $this->fpdf->Cell(35, 5, $_SESSION['asuransi_kendaraan']['persen_huruhara_str'], 0, 1, 'R');

        $this->fpdf->Cell(95, 5, 'Premi Terorisme dan Sabotase (TS)', 0, 0, 'L');
        $this->fpdf->Cell(3, 5, ':', 0, 0, 'C');
        $this->fpdf->Cell(7, 5, 'Rp', 0, 0, 'C');
        $this->fpdf->Cell(50, 5, $_SESSION['asuransi_kendaraan']['premi_terorisme_str'], 0, 0, 'R');
        $this->fpdf->Cell(35, 5, $_SESSION['asuransi_kendaraan']['persen_terorisme_str'], 0, 1, 'R');

        //multicell method
        $cellWidth = 45; //wrapped cell width
        $cellHeight = 5; //normal one-line cell height

        if ($_SESSION['asuransi_kendaraan']['premi_pap'] !== 0) {
            $ket_pap = '; '.$_SESSION['asuransi_kendaraan']['jml_seat'].' orang';
        } else {
            $ket_pap = '';
        }

        $dataPerluasan = array(
                                array('Premi Tanggung Jawab Hukum terhadap Pihak Ketiga (TJH)', $_SESSION['asuransi_kendaraan']['limit_tjh_str'], $_SESSION['asuransi_kendaraan']['premi_tjh_str'], 'Rate Progresif'),
                                array('Premi Kecelakaan Diri untuk Pengemudi (PAD)', $_SESSION['asuransi_kendaraan']['limit_pad_str'], $_SESSION['asuransi_kendaraan']['premi_pad_str'], $_SESSION['asuransi_kendaraan']['persen_pad_str']),
                                array('Premi Kecelakaan Diri untuk Penumpang (PAP)', $_SESSION['asuransi_kendaraan']['limit_pap_str'], $_SESSION['asuransi_kendaraan']['premi_pap_str'], $_SESSION['asuransi_kendaraan']['persen_pap_str'].$ket_pap),
                        );

        foreach ($dataPerluasan as $itemPerluasan) {
            //check whether the text is overflowing
            if ($this->fpdf->GetStringWidth($itemPerluasan[0]) < $cellWidth) {
                //if not, then do nothing
                $line = 1;
            } else {
                //if it is, then calculate the height needed for wrapped cell
                //by splitting the text to fit the cell width
                //then count how many lines are needed for the text to fit the cell

                $textLength = strlen($itemPerluasan[0]); //total text length
                //$errMargin = 10; //cell width error margin, just in case
                $errMargin = 0;
                $startChar = 0; //character start position for each line
                $maxChar = 0; //maximum character in a line, to be incremented later
                $textArray = array(); //to hold the strings for each line
                $tmpString = ''; //to hold the string for a line (temporary)

                while ($startChar < $textLength) {
                    //loop until end of text
                    //loop until maximum character reached
                    while ($this->fpdf->GetStringWidth($tmpString) < ($cellWidth - $errMargin) && ($startChar + $maxChar) < $textLength) {
                        ++$maxChar;
                        $tmpString = substr($itemPerluasan[0], $startChar, $maxChar);
                    }
                    //move startChar to next line
                    $startChar = $startChar + $maxChar;
                    //then add it into the array so we know how many line are needed
                    array_push($textArray, $tmpString);
                    //reset maxChar and tmpString
                    $maxChar = 0;
                    $tmpString = '';
                }
                //get number of line
                $line = count($textArray);
            }

            //write the cells

            //use MultiCell instead of Cell
            //but first, because MultiCell is always treated as line ending, we need to
            //manually set the xy position for the next cell to be next to it
            //remember the x and y position before writing the multicell
            $xPos = $this->fpdf->GetX();
            $yPos = $this->fpdf->GetY();
            $this->fpdf->MultiCell($cellWidth, $cellHeight, $itemPerluasan[0], 0, 'L');

            //return the position for next cell next to the multicell
            //and offset the x with multicell width
            $this->fpdf->SetXY($xPos + $cellWidth, $yPos);

            $this->fpdf->Cell(50, ($line * $cellHeight), $itemPerluasan[1], 0, 0, 'R');
            $this->fpdf->Cell(3, ($line * $cellHeight), ':', 0, 0, 'C');
            $this->fpdf->Cell(7, ($line * $cellHeight), 'Rp', 0, 0, 'C');
            $this->fpdf->Cell(50, ($line * $cellHeight), $itemPerluasan[2], 0, 0, 'R');
            $this->fpdf->Cell(35, ($line * $cellHeight), $itemPerluasan[3], 0, 1, 'R');
        }

        $this->fpdf->SetFont('Arial', 'B', 10);

        $this->fpdf->Cell(95, 5, 'Total Premi Setahun', 'T', 0, 'L');
        $this->fpdf->Cell(3, 5, ':', 'T', 0, 'C');
        $this->fpdf->Cell(7, 5, 'Rp', 'T', 0, 'C');
        $this->fpdf->Cell(50, 5, $_SESSION['asuransi_kendaraan']['total_premi_str'], 'T', 0, 'R');
        $this->fpdf->Cell(35, 5, '', 'T', 1, '');

        $this->fpdf->Cell(95, 5, 'Biaya Polis dan Materai', 0, 0, 'L');
        $this->fpdf->Cell(3, 5, ':', 0, 0, 'C');
        $this->fpdf->Cell(7, 5, 'Rp', 0, 0, 'C');
        $this->fpdf->Cell(50, 5, $_SESSION['asuransi_kendaraan']['biaya_polis_materai_str'], 0, 0, 'R');
        $this->fpdf->Cell(35, 5, '', 0, 1, '');

        $this->fpdf->Cell(95, 5, 'Total Bayar Setahun', 'TB', 0, 'L');
        $this->fpdf->Cell(3, 5, ':', 'TB', 0, 'C');
        $this->fpdf->Cell(7, 5, 'Rp', 'TB', 0, 'C');
        $this->fpdf->Cell(50, 5, $_SESSION['asuransi_kendaraan']['total_bayar_setahun_str'], 'TB', 0, 'R');
        $this->fpdf->Cell(35, 5, '', 'TB', 1, '');

        $this->fpdf->Output('Ilustrasi Perhitungan Premi Asuransi Kendaraan Bermotor.pdf', 'I');
    }

    //Fungsi callback untuk memeriksa nilai perlengkapan nonstandar yang diinputkan
    public function cek_nilai_perlengkapan($str_perlengkapan)
    {
        //Konversikan ke angka harga kendaraan (data yang di-post berupa string)
        $hrg_kendaraan = $this->fungsi->convert_to_number($this->input->post('harga_kendaraan'));

        //Konversikan ke angka nilai perlengkapan nonstandar (data yang di-post berupa string)
        $nilai_perlengkapan = $this->fungsi->convert_to_number($str_perlengkapan);

        //Nilai Total Perlengkapan Tambahan Nonstandar yang dapat dijamin adalah 10% dari Harga Pertanggungan Kendaraan Bermotor
        $dapat_dijamin = 10 / 100 * $hrg_kendaraan;

        if ($nilai_perlengkapan > $dapat_dijamin) {
            //Jika nilai total perlengkapan melebihi 10% dari nilai kendaraan, tampilkan pesan error
            $this->form_validation->set_message('cek_nilai_perlengkapan', 'Nilai Total Perlengkapan Tambahan Nonstandar yang dapat dijamin maksimal 10% dari Harga Kendaraan Bermotor');

            return false;
        } else {
            //Jika tidak, kembalikan nilai TRUE
            return true;
        }
    }

    //Fungsi callback untuk memeriksa Limit TJH. Jika Perluasan TJH dipilih, maka input Limit TJH tidak boleh kosong
    public function cek_input_tjh($str_limit_tjh)
    {
        //Tampung value dari perluasan jaminan ke variabel $arr_perluasan
        $arr_perluasan = $this->input->post('perluasan[]');

        //Periksa apakah ada perluasan yang dipilih
        if (isset($arr_perluasan)) {
            //Jika ada, periksa setiap indeks array pada $arr_perluasan dengan looping foreach
            foreach ($arr_perluasan as $value_jaminan):

                if ($value_jaminan === '5') { //Jika jaminan 'Tanggung Jawab Hukum Terhadap Pihak Ketiga' dipilih
                    //Ambil input Limit TJH dan konversikan ke angka
                    $angka_tjh = $this->fungsi->convert_to_number($str_limit_tjh);

                    //Jika Limit TJH == 0
                    if ($angka_tjh == 0) {
                        $this->form_validation->set_message('cek_input_tjh', 'Limit TJH tidak boleh kosong');

                        return false;
                    } elseif ($angka_tjh > 100000000) { //Jika Limit TJH melebihi Rp100.000.000
                        $this->form_validation->set_message('cek_input_tjh', 'Limit TJH tidak boleh melebihi Rp100.000.000');

                        return false;
                    } else {
                        return true;
                    }
                }
            endforeach;
        }
    }

    //Fungsi callback untuk memeriksa Limit PAD. Jika Perluasan PAD dipilih, maka input Limit PAD tidak boleh kosong
    public function cek_input_pad($str_limit_pad)
    {
        //Tampung value dari perluasan jaminan ke variabel $arr_perluasan
        $arr_perluasan = $this->input->post('perluasan[]');

        //Periksa apakah ada perluasan yang dipilih
        if (isset($arr_perluasan)) {
            //Jika ada, periksa setiap indeks array pada $arr_perluasan dengan looping foreach
            foreach ($arr_perluasan as $value_jaminan):

                if ($value_jaminan === '6') { //Jika jaminan 'Kecelakaan Diri untuk Pengemudi (PAD)' dipilih
                    //Ambil input Limit PAD dan konversikan ke angka
                    $angka_pad = $this->fungsi->convert_to_number($str_limit_pad);

                    //Jika Limit PAD = 0
                    if ($angka_pad == 0) {
                        $this->form_validation->set_message('cek_input_pad', 'Limit PAD tidak boleh kosong');

                        return false;
                    } else {
                        return true;
                    }
                }
            endforeach;
        }
    }

    //Fungsi callback untuk memeriksa Limit PAP. Jika Perluasan PAP dipilih, maka input Limit PAP tidak boleh kosong
    public function cek_input_pap($str_limit_pap)
    {
        //Tampung value dari perluasan jaminan ke variabel $arr_perluasan
        $arr_perluasan = $this->input->post('perluasan[]');

        //Periksa apakah ada perluasan yang dipilih
        if (isset($arr_perluasan)) {
            //Jika ada, periksa setiap indeks array pada $arr_perluasan dengan looping foreach
            foreach ($arr_perluasan as $value_jaminan):

                if ($value_jaminan === '7') { //Jika jaminan 'Kecelakaan Diri untuk Penumpang (PAP)' dipilih
                    //Ambil input Limit PAP dan konversikan ke angka
                    $angka_pap = $this->fungsi->convert_to_number($str_limit_pap);

                    //Jika Limit PAP = 0
                    if ($angka_pap == 0) {
                        $this->form_validation->set_message('cek_input_pap', 'Limit PAP tidak boleh kosong');

                        return false;
                    } else {
                        return true;
                    }
                }
            endforeach;
        }
    }

    public function aksi_upload()
    {
        $config['upload_path'] = './gambar/';
        $config['allowed_types'] = 'gif|jpg|png';
        $config['max_size'] = 100;
        $config['max_width'] = 1024;
        $config['max_height'] = 768;

        $this->load->library('upload', $config);

        if (!$this->upload->do_upload('berkas')) {
            $error = array('error' => $this->upload->display_errors());
            $this->load->view('v_upload', $error);
        } else {
            $data = array('upload_data' => $this->upload->data());
            $this->load->view('v_upload_sukses', $data);
        }
    }

    //Fungsi callback untuk memeriksa Jumlah Seat. Jika Perluasan PAP dipilih, maka input Jumlah Seat tidak boleh kosong
    public function cek_jml_seat($input_seat)
    {
        //Tampung value dari perluasan jaminan ke variabel $arr_perluasan
        $arr_perluasan = $this->input->post('perluasan[]');

        //Periksa apakah ada perluasan yang dipilih
        if (isset($arr_perluasan)) {
            //Jika ada, periksa setiap indeks array pada $arr_perluasan dengan looping foreach
            foreach ($arr_perluasan as $value_jaminan):

                if ($value_jaminan === '7') { //Jika jaminan 'Kecelakaan Diri untuk Penumpang (PAP)' dipilih
                    //Ambil input Jumlah Seat
                    $seat = $input_seat;

                    //Jika Jumlah Seat = 0
                    if ($seat == 0) {
                        $this->form_validation->set_message('cek_jml_seat', 'Jumlah Seat tidak boleh kosong');

                        return false;
                    } else {
                        return true;
                    }
                }
            endforeach;
        }
    }
}
/* End of file Asuransi_kendaraan.php
 Location: ./application/controllers/Asuransi_kendaraan.php */
