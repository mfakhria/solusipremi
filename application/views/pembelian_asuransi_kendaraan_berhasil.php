<div class="container message">
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<div class="message-box">
				<h3 class="text-center text-success">Pembelian Asuransi Kendaraan Anda Berhasil</h3>
				<p class="text-center text-success">
					Pembelian Asuransi Kendaraan Anda telah berhasil. Terima Kasih. Kami akan menghubungi Anda secepatnya untuk proses lebih lanjut.
				</p>
				<p class="text-center">
					<a href="<?php echo site_url(); ?>" class="btn btn-primary" role="button">Kembali ke Beranda</a>
				</p>
			</div>
		</div>
	</div>
</div>