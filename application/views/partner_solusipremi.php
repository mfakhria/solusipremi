<div class="container-fluid partner">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<h3 class="text-center">Partner SolusiPremi.com</h3>
			</div>
		</div>
		<div class="row">
			<center>
			<div class="col-xs-6 col-sm-3 col-md-3 col-lg-3">
				<img src="<?php echo base_url('assets/images/partner/asuransi-sinarmas.png'); ?>" class="img-responsive" alt="Asuransi Sinarmas">
			</div>
			<div class="col-xs-6 col-sm-3 col-md-3 col-lg-3">
				<img src="<?php echo base_url('assets/images/partner/china-taiping.png'); ?>" class="img-responsive" alt="China Taiping">
			</div>
			<div class="clearfix visible-xs"></div>
			<div class="col-xs-6 col-sm-3 col-md-3 col-lg-3">
				<img src="<?php echo base_url('assets/images/partner/asuransi-mag.png'); ?>" class="img-responsive" alt="Asuransi MAG">
			</div>
			<div class="col-xs-6 col-sm-3 col-md-3 col-lg-3">
				<img src="<?php echo base_url('assets/images/partner/mnc-insurance.png'); ?>" class="img-responsive" alt="MNC Insurance">
			</div>
			</center>
		</div>
	</div>
</div>