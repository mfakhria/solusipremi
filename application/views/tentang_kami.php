<div class="container-fluid about-us">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12"><br><br><br><br><br>
				<h2 class="text-center" style="color:#575757;letter-spacing:1px;">Tentang Kami</h2>
				<p align="justify" class="indent-paragraph">
					SolusiPremi.com merupakan portal layanan asuransi <em>online</em> untuk pasar Indonesia
					yang berdiri sejak Januari 2018. SolusiPremi.com berpusat pada jasa layanan keuangan berupa
					produk asuransi diantaranya asuransi kendaraan, asuransi properti, asuransi pengiriman,
					asuransi penjaminan, asuransi perjalanan, asuransi kesehatan, dan asuransi jiwa. Namun,
					untuk saat ini kami hanya menyediakan asuransi kendaraan dan asuransi properti.
				</p>
				<p align="justify" class="indent-paragraph">
					SolusiPremi.com menjalin kerja sama dengan beberapa perusahaan asuransi terpercaya di Indonesia, diantaranya
					adalah Asuransi Sinarmas, China Taiping, Asuransi MAG, dan MNC <em>Insurance</em>.
				</p>
				<p align="justify" class="indent-paragraph">
					Sejalan dengan <em>tag line</em> "<strong>Solusi Asuransi Anda</strong>", kami berusaha untuk menyediakan pilihan produk asuransi terbaik
					sesuai kebutuhan Anda dengan harga terjangkau. Kami membantu keperluan asuransi Anda secara mudah, cepat, dan aman.
				</p>
				<p align="center">
					Pilih produk asuransi kebutuhan Anda melalui SolusiPremi.com sekarang dan rasakan keunggulannya.
				</p>
			</div>
		</div>
	</div>
</div>
<script src="<?php echo base_url('assets/js/functions.js');?>"></script>