div class="container-fluid" style="background-color:#fff;">
    <div class="container margin_70">
        <div class="row">
            <div class="col-md-12 main_title textle">
                <h2 style="color:#575757;letter-spacing:1px;font-size: 23px;">Register</h2>
            </div>
            <div class="col-md-12" style="margin-top: -37px;">
                <hr>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3"></div>
            <div class="col-md-6">
                <div class="box_style_1 pad56">
                    <div class="row">
						  <?php echo form_open($action, 'role="form"'); ?>
								<div class="form-group">
									<label for="firstname" class="control-label">Nama Depan</label>
									<input type="text" name="firstname" id="firstname" value="<?php echo set_value('firstname'); ?>" class="form-control" placeholder="Nama Depan" required="required"></input>
									<?php echo form_error('firstname','<p class="text-danger">','</p>'); ?>
								</div>
								<div class="form-group">
									<label for="lastname" class="control-label">Nama Belakang</label>
									<input type="text" name="lastname" id="lastname" value="<?php echo set_value('lastname'); ?>" class="form-control" placeholder="Nama Belakang" required="required"></input>
									<?php echo form_error('lastname','<p class="text-danger">','</p>'); ?>
								</div>
								<div class="form-group">
									<label for="email" class="control-label">Email</label>
									<input type="email" name="email" id="email" value="<?php echo set_value('email'); ?>" class="form-control" placeholder="Email" required="required"></input>
									<?php echo form_error('email','<p class="text-danger">','</p>'); ?>
								</div>
								<div class="form-group">
									<label for="handphone" class="control-label">Nomor Handphone</label>
									<input type="text" name="handphone" id="handphone" value="<?php echo set_value('handphone'); ?>" class="form-control" placeholder="Nomor Handphone" required="required"></input>
									<span class="help-block">Contoh: 081234567890</span>
									<?php echo form_error('handphone','<p class="text-danger">','</p>'); ?>
								</div>
								<div class="form-group">
									<label for="password" class="control-label">Password</label>
									<input type="password" name="password" id="password" value="<?php echo set_value('password'); ?>" class="form-control" placeholder="Password" required="required"></input>
									<?php echo form_error('password','<p class="text-danger">','</p>'); ?>
								</div>
								<div class="form-group">
									<label for="confirmpassword" class="control-label">Konfirmasi Password</label>
									<input type="password" name="confirmpassword" id="confirmpassword" value="<?php echo set_value('confirmpassword'); ?>" class="form-control" placeholder="Konfirmasi Password" required="required"></input>
									<?php echo form_error('confirmpassword','<p class="text-danger">','</p>'); ?>
								</div>
								<div class="form-group text-center">
									<input type="submit" value="Daftar" class="btn_2 btn agelogbt btn-block"></input>
								</div>
								<p class="text-left">*Dengan mendaftar di solusipremi.com, Anda telah setuju dengan <a href="<?php echo site_url('syarat-dan-ketentuan'); ?>">Syarat dan Ketentuan</a> serta
								<a href="<?php echo site_url('kebijakan-privasi'); ?>">Kebijakan Privasi</a> yang berlaku.</p>
                            <div class="agentregfor">
                                <span>Already User?  <a href="<?php echo site_url('auth'); ?>">Login Now</a></span>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>