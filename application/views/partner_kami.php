<div class="container-fluid our-partner">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12"><br><br><br><br><br>
				<h2 class="text-center" style="color:#575757;letter-spacing:1px;">Partner Kami</h2>
				<p align="justify" class="indent-paragraph">
					SolusiPremi.com, solusi asuransi Anda telah bekerja sama dengan beberapa perusahaan
					asuransi terbaik dan terpercaya di Indonesia. Partner perusahaan asuransi kami adalah:
				</p>
				<div class="row our-partner-row">
					<center>
					<div class="col-xs-6 col-sm-3 col-md-3 col-lg-3">
						<img src="<?php echo base_url('assets/images/partner/asuransi-sinarmas.png'); ?>" class="img-responsive our-partner-img" alt="Asuransi Sinarmas">
						<a href="<?php echo site_url('tentang-solusipremi/partner-kami/asuransi-sinarmas'); ?>"><h5>Asuransi Sinarmas</h5></a>
					</div>
					<div class="col-xs-6 col-sm-3 col-md-3 col-lg-3">
						<img src="<?php echo base_url('assets/images/partner/china-taiping.png'); ?>" class="img-responsive our-partner-img" alt="China Taiping">
						<a href="<?php echo site_url('tentang-solusipremi/partner-kami/china-taiping'); ?>"><h5>China Taiping</h5></a>
					</div>
					<div class="clearfix visible-xs"></div>
					<div class="col-xs-6 col-sm-3 col-md-3 col-lg-3">
						<img src="<?php echo base_url('assets/images/partner/asuransi-mag.png'); ?>" class="img-responsive our-partner-img" alt="Asuransi MAG">
						<a href="<?php echo site_url('tentang-solusipremi/partner-kami/asuransi-mag'); ?>"><h5>Asuransi MAG</h5></a>
					</div>
					<div class="col-xs-6 col-sm-3 col-md-3 col-lg-3">
						<img src="<?php echo base_url('assets/images/partner/mnc-insurance.png'); ?>" class="img-responsive our-partner-img" alt="MNC Insurance">
						<a href="<?php echo site_url('tentang-solusipremi/partner-kami/mnc-insurance'); ?>"><h5>MNC Insurance</h5></a>
					</div>
					</center>
				</div>
				<p align="justify" class="indent-paragraph">
					Kami memberikan Anda kemudahan dalam membandingkan produk asuransi dari partner perusahaan asuransi kami
					sesuai dengan produk yang kami jual di SolusiPremi.com dan memastikan Anda mendapatkan informasi lengkap
					sebelum Anda memilih produk dari perusahaan asuransi mana sesuai kebutuhan Anda. Kami akan selalu memperbarui
					informasi produk sesuai dengan informasi terbaru dari partner perusahaan asuransi kami. Selain itu, kami akan
					menambah jumlah partner kami untuk memastikan kebutuhan informasi asuransi Anda terpenuhi.
				</p>
			</div>
		</div>
	</div>
</div>
<script src="<?php echo base_url('assets/js/functions.js');?>"></script>