detadiv class="container-fluid premium-illustration-content">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-offset-1 col-lg-offset-1 col-md-10 col-lg-10 premium-table-section">
				<div class="table-responsive">
					<table class="table table-condensed"><br><br><br>
						<caption class="text-center">Ilustrasi Perhitungan Premi Asuransi Kendaraan Bermotor</caption>
						<thead>
							<tr>
								<th colspan="6">Ilustrasi Perhitungan Premi Asuransi Kendaraan Bermotor</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>Harga Kendaraan</td>
								<td></td>
								<td>:</td>
								<td>Rp</td>
								<td align="right"><?php echo $v_harga_kendaraan; ?></td>
								<td></td>
							</tr>
							<tr>
								<td>Perlengkapan Tambahan Nonstandar</td>
								<td></td>
								<td>:</td>
								<td>Rp</td>
								<td align="right"><?php echo $v_perlengkapan_nonstd; ?></td>
								<td></td>
							</tr>
							<tr>
								<td>Total Harga Pertanggungan</td>
								<td></td>
								<td>:</td>
								<td>Rp</td>
								<td align="right"><?php echo $v_total_harga_pertanggungan; ?></td>
								<td></td>
							</tr>
							<tr>
								<td>Tahun Kendaraan</td>
								<td></td>
								<td>:</td>
								<td colspan="3"><?php echo $v_tahun_kendaraan; ?></td>
							</tr>
							<tr>
								<td>Jenis Kendaraan</td>
								<td></td>
								<td>:</td>
								<td colspan="3"><?php echo $v_jenis_kendaraan; ?></td>
							</tr>
							<tr>
								<td>Wilayah</td>
								<td></td>
								<td>:</td>
								<td colspan="2"><?php echo $v_kode_wilayah.' ('.$v_nama_kota.')'; ?></td>
								<td></td>
							</tr>
							<tr>
								<td>Basic Coverage</td>
								<td></td>
								<td>:</td>
								<td colspan="3"><?php echo $v_jenis_pertanggungan; ?></td>
							</tr>
							<tr>
								<td>Premi Basic Coverage</td>
								<td></td>
								<td>:</td>
								<td>Rp</td>
								<td align="right"><?php echo $v_premi_dasar; ?></td>
								<td align="right"><?php echo $v_persen_premi; ?></td>
							</tr>
							<tr>
								<td>Loading Premi (Faktor Usia)</td>
								<td></td>
								<td>:</td>
								<td>Rp</td>
								<td align="right"><?php echo $v_loading_premi; ?></td>
								<td align="right"><?php echo $v_persen_usia; ?></td>
							</tr>
							<tr>
								<td>Premi Banjir termasuk Angin Topan</td>
								<td></td>
								<td>:</td>
								<td>Rp</td>
								<td align="right"><?php echo $v_premi_banjir; ?></td>
								<td align="right"><?php echo $v_persen_banjir; ?></td>
							</tr>
							<tr>
								<td>Premi Gempa Bumi, Tsunami</td>
								<td></td>
								<td>:</td>
								<td>Rp</td>
								<td align="right"><?php echo $v_premi_gempa; ?></td>
								<td align="right"><?php echo $v_persen_gempa; ?></td>
							</tr>
							<tr>
								<td>Premi Huru-hara dan Kerusuhan (SRCC)</td>
								<td></td>
								<td>:</td>
								<td>Rp</td>
								<td align="right"><?php echo $v_premi_huruhara; ?></td>
								<td align="right"><?php echo $v_persen_huruhara; ?></td>
							</tr>
							<tr>
								<td>Premi Terorisme dan Sabotase (TS)</td>
								<td></td>
								<td>:</td>
								<td>Rp</td>
								<td align="right"><?php echo $v_premi_terorisme; ?></td>
								<td align="right"><?php echo $v_persen_terorisme; ?></td>
							</tr>
							<tr>
								<td>Premi Tanggung Jawab Hukum terhadap Pihak Ketiga (TJH)</td>
								<td align="right"><?php echo $v_limit_tjh; ?></td>
								<td>:</td>
								<td>Rp</td>
								<td align="right"><?php echo $v_premi_tjh; ?></td>
								<td align="left">Rate Progresif</td>
							</tr>
							<tr>
								<td>Premi Kecelakaan Diri untuk Pengemudi (PAD)</td>
								<td align="right"><?php echo $v_limit_pad; ?></td>
								<td>:</td>
								<td>Rp</td>
								<td align="right"><?php echo $v_premi_pad; ?></td>
								<td align="right"><?php echo $v_persen_pad; ?></td>
							</tr>
							<tr>
								<td>Premi Kecelakaan Diri untuk Penumpang (PAP)</td>
								<td align="right"><?php echo $v_limit_pap; ?></td>
								<td>:</td>
								<td>Rp</td>
								<td align="right"><?php echo $v_premi_pap; ?></td>
								<td align="<?php echo $align; ?>"><?php echo $v_persen_pap_ket; ?></td>
							</tr>
							<tr>
								<td><b>Total Premi Setahun</b></td>
								<td></td>
								<td><b>:</b></td>
								<td><b>Rp</b></td>
								<td align="right"><b><?php echo $v_total_premi; ?></b></td>
								<td></td>
							</tr>
							<tr>
								<td><b>Biaya Polis dan Materai</b></td>
								<td></td>
								<td><b>:</b></td>
								<td><b>Rp</b></td>
								<td align="right"><b><?php echo $v_biaya_polis_materai; ?></b></td>
								<td></td>
							</tr>
							<tr>
								<td><b>Total Bayar Setahun</b></td>
								<td></td>
								<td><b>:</b></td>
								<td><b>Rp</b></td>
								<td align="right"><b><?php echo $v_total_bayar_setahun; ?></b></td>
								<td></td>
							</tr>
						</tbody>
					</table>
				</div>
				<center>
					<a href="<?php echo site_url('asuransi-kendaraan/cetak-ilustrasi'); ?>" class="btn btn-primary" role="button"><span class="glyphicon glyphicon-print"></span> Cetak Ilustrasi</a>
				</center>	
			</div>
		</div>
	</div>
</div>
<div class="container-fluid partner-option-content">
	<div class="container">
		<?php $jumlah_partner = $list_partner; //$list_partner berasal dari data yang dilempar dari controller, yaitu $data['list_partner'], berisi result_array() ?>
		<?php if($jumlah_partner > 0) //Apabila data partner yang ada di dalam database lebih dari 0 maka baru ditampilkan ?>
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-offset-1 col-lg-offset-1 col-md-10 col-lg-10 partner-option-section">
				<div class="table-responsive">
					<table class="table table-bordered table-condensed">
						<caption class="text-center">Tabel Pilihan Asuransi</caption>
						<thead>
							<tr>
								<th><center>Asuransi</center></th>
								<th><center>Total Premi Setahun</center></th>
								<th colspan="2"><center>Biaya Tambahan</center></th>
								<th></th>
							</tr>
						</thead>
						<tbody>
							<?php
								foreach($list_partner as $row)
								{
							?>
							<tr>
								<td>
									<center>
									<img src="<?php echo base_url('assets/images/partner/'.$row['nama_gambar_kecil'].''); ?>" class="img-responsive" alt="<?php echo $row['nama_partner']; ?>">
									</center>
									</td>
								<td>
									<center>Rp<?php echo $v_total_premi; ?>
									</center>
									</td>
								<td>Biaya Polis dan Materai</td>
								<td align="right">Rp<?php echo $v_biaya_polis_materai; ?></td>
								<td>
									<center>
									<!-- Button trigger modal -->
									<button class="view_data btn btn-info" id="<?php echo $row['id'];?>" data-toggle="modal"
										data-target="#myModal">Fitur
									</button>
									<a href="<?php echo site_url('asuransi-kendaraan/isi-data/'.$row['id'].''); ?>" class="btn btn-primary" role="button">Beli</a>
									</center>
									</td>
							</tr>
							<?php
								}
							?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog"
	aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close"
					data-dismiss="modal" aria-hidden="true">
					&times;
				</button>
				<h4 class="modal-title" id="myModalLabel">
					<br>Fitur
				</h4>
			</div>
			<div class="modal-body" id="detail_produk">
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default"
					data-dismiss="modal">Close
				</button>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script>
	$(document).ready(function(){
		$('.view_data').click(function(){
			var id = $(this).attr("id");
			
			$.ajax({
				url: '<?php echo site_url(); ?>/asuransi-kendaraan/view-detail',
				method: 'post',
				data: {id:id},
				success:function(data){
					$('#detail_produk').html(data);
					$('#myModal').modal("show");
				}
			});
		});
	});
</script>