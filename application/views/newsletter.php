<div class="container-fluid text-center newsletter">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<h3>Berlangganan <em>Newsletter</em></h3>
				<p>Dapatkan informasi terbaru seputar tips, diskon, atau promo dari SolusiPremi.com, <strong>Gratis!</strong></p>
			
				<?php echo validation_errors('<p class="text-danger">', '</p>'); ?>
				<?php echo form_open('subscribe', 'class="form-inline" role="form"'); ?>
					<div class="form-group">
						<div class="input-group">
							<span class="input-group-addon glyphicon glyphicon-envelope"></span>
							<input type="email" name="email" id="email" value="<?php echo set_value('email'); ?>" class="form-control" placeholder="Masukkan email Anda"></input>
						</div>
					</div>
					<input type="submit" value="Berlangganan Sekarang" class="btn btn-primary"></input>
				</form>
			</div>
		</div>
	</div>
</div>