<div class="container message">
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<div class="message-box">
				<h3 class="text-center text-success">Pesan Anda Berhasil Dikirim</h3>
				<p class="text-center text-success">
					Pesan Anda telah berhasil dikirim. Pihak kami Akan menghubungi Anda secepatnya.
					Silakan tunggu respon dari kami.
				</p>
			</div>
		</div>
	</div>
</div>