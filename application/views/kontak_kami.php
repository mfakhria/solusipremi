<div class="container-fluid test3_bg ">
    <div class="container margin_60 marbot52">
        <div class="col-md-12 main_title"><br><br>
            <h2 style="color:#575757;letter-spacing:1px;">contact</h2>
        </div>
        <div class="row">
            <div class="col-sm-12">
            <div class="embed-responsive embed-responsive-4by3" style="padding-bottom:35%;"><iframe class="embed-responsive-item"  src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d8050616.318603007!2d71.32586858858775!3d9.853303478446444!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3a5265ea4f7d3361%3A0x6e61a70b6863d433!2sChennai%2C+Tamil+Nadu%2C+India!5e0!3m2!1sen!2sus!4v1519820161671" width="100%" height="250" frameborder="0" style="border:0" allowfullscreen></iframe></div>
            </div>
        </div>
    </div>
</div>

<div class="container-fluid test3_bg">
    <div class="container margin_60">
        <div class="col-md-12 main_title">
            <h2 style="color:#575757;letter-spacing:1px;">We are excited to hear from you</h2>
        </div>

        <div class="row">
            <div class="col-md-6 col-sm-6">
                <div class="conpara"><h4 style="font-weight:bold;">Contact details</h4></div>
                <div class="condetail">
                    <div class="col-xs-1 martop10"><i class="icon-location-6 "></i></div>
                    <div class="col-xs-11">
                        <h4>Address</h4>
                        <p>Place Charles de Gaulle, 75008 Paris</p>
                    </div>

                    <div class="col-xs-1 martop10"><i class="icon-phone"></i></div>
                    <div class="col-xs-11">
                        <h4>Phone</h4>
                        <p>+91 9876543210</p>
                    </div>
                    
                    <div class="col-xs-1 martop10"><i class="icon-mail"></i>
                    </div>
                    <div class="col-xs-11">
                        <h4>Email</h4>
                        <p>policyplan@socialcommunityscript.com</p>
                    </div>
                </div>
            </div>

            <div class="col-md-6 col-sm-6">
                <div class="step">
                    <form method="post">
                        <div class="row">
                                                        <div class="form-group">
                                <input type="text" class="form-control contactin" name="cName" value="" placeholder="Name" data-validation="required" data-validation-error-msg-required="Enter your name">
                            </div>

                            <div class="form-group">
                                <input type="email" class="form-control contactin" name="cEmail" value="" placeholder="Email" data-validation="required email" data-validation-error-msg-required="Enter your email address" data-validation-error-msg-email="Invalid email address">
                            </div>

                            <div class="form-group">
                                <input type="text" class="form-control contactin" name="cMobile" value="" placeholder="Phone Number">
                            </div>

                            <div class="form-group">
                                <input type="text" class="form-control contactin" name="cSubject" value="" placeholder="Subject" data-validation="required" data-validation-error-msg-required="Enter the subject">
                            </div>

                            <div class="form-group">
                                <textarea class="form-control contactin" name="cMessage" placeholder="Message" rows="6" data-validation="required" data-validation-error-msg-required="Enter the message"></textarea>
                            </div>

                            <div class="form-group">
                                <button class="btn btn-block btn-success contactin" type="submit" name="frmSubmit">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="<?php echo base_url('assets/js/functions.js');?>"></script>