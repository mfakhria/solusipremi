<script src="<?php echo base_url('assets/jquery-maskmoney/jquery.maskMoney.min.js'); ?>" type="text/javascript"></script>
<section class="parallax-window" data-parallax="scroll" style="background-image: url('<?php echo base_url(); ?>assets/images/header/auto-insurance-bg.jpg'); background-repeat: no-repeat; background-size: 100%" data-natural-width="1400" data-natural-height="470">
    <div class="parallax-content-1">
        <div class="animated fadeInDown">
            <div id="search_bar_container" style="background:transparent;">
                <div class="container">
                	<div class="row">
						<div class="col-xs-12 col-sm-12 col-lg-12">
							<div class="page-heading">
								<h1>Asuransi Kendaraan</h1>
								<p>
									Asuransi Kendaraan melindungi kendaraan Anda dari berbagai resiko,
									baik kecelakaan maupun tindakan kejahatan.
								</p>
						    </div>
						</div>
					</div>
                </div>
            </div>
        </div>
    </div>
</section>
<div class="container-fluid auto-premium-search-form">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 premium-search-box">
				<h3 class="text-center">Asuransi Kendaraan</h3>
				<hr>
				<?php echo form_open('asuransi-kendaraan/cari-asuransi', 'name="vehicleinsurance" role="form"'); ?>
					<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3 form-group">
						<label for="harga_kendaraan" class="control-label">Harga Kendaraan</label>
						<div class="input-group">
							<span class="input-group-addon">Rp</span>
							<input type="text" name="harga_kendaraan" id="harga_kendaraan" value="<?php echo set_value('harga_kendaraan'); ?>" class="form-control" placeholder="Jumlah" required="required"></input>
							<span class="input-group-addon">,00</span>
						</div>	
						<?php echo form_error('harga_kendaraan','<p class="text-danger">','</p>'); ?>
					</div>
					<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3 form-group">
						<label for="perlengkapan_nonstd" class="control-label">Perlengkapan Tambahan Nonstandar</label>
						<div class="input-group">
							<span class="input-group-addon">Rp</span>
							<input type="text" name="perlengkapan_nonstd" id="perlengkapan_nonstd" value="<?php echo set_value('perlengkapan_nonstd'); ?>" class="form-control" placeholder="Jumlah"></input>
							<span class="input-group-addon">,00</span>
						</div>
						<?php echo form_error('perlengkapan_nonstd','<p class="text-danger">','</p>'); ?>
					</div>
					<div class="clearfix visible-sm"></div>
					<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3 form-group">
						<label for="jenis_pertanggungan" class="control-label">Jenis Pertanggungan</label>
						<select name="jenis_pertanggungan" class="form-control">
							<option value="1" <?php echo set_select('jenis_pertanggungan', '1', TRUE); ?> >Comprehensive</option>
							<option value="2" <?php echo set_select('jenis_pertanggungan', '2'); ?> >Total Loss Only (TLO)</option>
						</select>
					</div>
					<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3 form-group">
						<label for="jenis_kendaraan" class="control-label">Jenis Kendaraan</label>
						<select name="jenis_kendaraan" class="form-control">
							<option value="1" <?php echo set_select('jenis_kendaraan', '1', TRUE); ?> >Penumpang (non Truk/Pick-Up/Bus)</option>
							<option value="2" <?php echo set_select('jenis_kendaraan', '2'); ?> >Truk/Pick-Up</option>
							<option value="3" <?php echo set_select('jenis_kendaraan', '3'); ?> >Bus</option>
							<option value="4" <?php echo set_select('jenis_kendaraan', '4'); ?> >Roda Dua (Sepeda Motor)</option>
						</select>
					</div>
					<div class="clearfix visible-sm visible-md visible-lg"></div>
					<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 form-group">
						<label for="tahun" class="control-label">Tahun Pembuatan Kendaraan</label>
						<select name="tahun" class="form-control">
							<option value="2018" <?php echo set_select('tahun', '2018', TRUE); ?> >2018</option>
							<option value="2017" <?php echo set_select('tahun', '2017'); ?> >2017</option>
							<option value="2016" <?php echo set_select('tahun', '2016'); ?> >2016</option>
							<option value="2015" <?php echo set_select('tahun', '2015'); ?> >2015</option>
							<option value="2014" <?php echo set_select('tahun', '2014'); ?> >2014</option>
							<option value="2013" <?php echo set_select('tahun', '2013'); ?> >2013</option>
							<option value="2012" <?php echo set_select('tahun', '2012'); ?> >2012</option>
							<option value="2011" <?php echo set_select('tahun', '2011'); ?> >2011</option>
							<option value="2010" <?php echo set_select('tahun', '2010'); ?> >2010</option>
							<option value="2009" <?php echo set_select('tahun', '2009'); ?> >2009</option>
							<option value="2008" <?php echo set_select('tahun', '2008'); ?> >2008</option>
							<option value="2007" <?php echo set_select('tahun', '2007'); ?> >2007</option>
							<option value="2006" <?php echo set_select('tahun', '2006'); ?> >2006</option>
							<option value="2005" <?php echo set_select('tahun', '2005'); ?> >2005</option>
							<option value="2004" <?php echo set_select('tahun', '2004'); ?> >2004</option>
							<option value="2003" <?php echo set_select('tahun', '2003'); ?> >2003</option>
							<option value="2002" <?php echo set_select('tahun', '2002'); ?> >2002</option>
							<option value="2001" <?php echo set_select('tahun', '2001'); ?> >2001</option>
							<option value="2000" <?php echo set_select('tahun', '2000'); ?> >2000</option>
							<option value="1999" <?php echo set_select('tahun', '1999'); ?> >1999</option>
							<option value="1998" <?php echo set_select('tahun', '1998'); ?> >1998</option>
							<option value="1997" <?php echo set_select('tahun', '1997'); ?> >1997</option>
						</select>
					</div>
					<div class="col-xs-12 col-sm-6 col-md-8 col-lg-8 form-group">
						<label for="pelat_nomor" class="control-label">Pelat Nomor</label>
						<select name="pelat_nomor" class="form-control">
							<option value="12" <?php echo set_select('pelat_nomor', '12', TRUE); ?> >A - Banten, Serang, Pandeglang, Cilegon, Lebak, sebagian Tangerang</option>
							<option value="23" <?php echo set_select('pelat_nomor', '23'); ?> >AA - Magelang, Purworejo, Kebumen, Temanggung, Wonosobo</option>
							<option value="24" <?php echo set_select('pelat_nomor', '24'); ?> >AB - Yogyakarta, Bantul, Gunung Kidul, Sleman, Kulon Progo</option>
							<option value="25" <?php echo set_select('pelat_nomor', '25'); ?> >AD - Surakarta, Sukoharjo, Boyolali, Sragen, Karanganyar, Wonogiri, Klaten</option>
							<option value="32" <?php echo set_select('pelat_nomor', '32'); ?> >AE - Madiun, Ngawi, Magetan, Ponorogo, Pacitan</option>
							<option value="33" <?php echo set_select('pelat_nomor', '33'); ?> >AG - Kediri, Blitar, Tulungagung, Nganjuk, Trenggalek</option>
							<option value="13" <?php echo set_select('pelat_nomor', '13'); ?> >B - DKI Jakarta, Tangerang, Bekasi, Depok</option>
							<option value="4" <?php echo set_select('pelat_nomor', '4'); ?> >BA - Sumatera Barat</option>
							<option value="2" <?php echo set_select('pelat_nomor', '2'); ?> >BB - Sumatera Utara Bagian Barat (Tapanuli)</option>
							<option value="10" <?php echo set_select('pelat_nomor', '10'); ?> >BD - Bengkulu</option>
							<option value="9" <?php echo set_select('pelat_nomor', '9'); ?> >BE - Lampung</option>
							<option value="7" <?php echo set_select('pelat_nomor', '7'); ?> >BG - Sumatera Selatan</option>
							<option value="11" <?php echo set_select('pelat_nomor', '11'); ?> >BH - Jambi</option>
							<option value="3" <?php echo set_select('pelat_nomor', '3'); ?> >BK - Sumatera Utara</option>
							<option value="1" <?php echo set_select('pelat_nomor', '1'); ?> >BL - Nanggroe Aceh Darussalam</option>
							<option value="5" <?php echo set_select('pelat_nomor', '5'); ?> >BM - Riau</option>
							<option value="8" <?php echo set_select('pelat_nomor', '8'); ?> >BN - Kepulauan Bangka Belitung</option>
							<option value="6" <?php echo set_select('pelat_nomor', '6'); ?> >BP - Kepulauan Riau</option>
							<option value="14" <?php echo set_select('pelat_nomor', '14'); ?> >D - Bandung, Cimahi, Bandung Barat</option>
							<option value="41" <?php echo set_select('pelat_nomor', '41'); ?> >DA - Kalimantan Selatan</option>
							<option value="44" <?php echo set_select('pelat_nomor', '44'); ?> >DB - Manado, Tomohon, Bitung, Bolaang Mongondow, Minahasa</option>
							<option value="50" <?php echo set_select('pelat_nomor', '50'); ?> >DC - Sulawesi Barat</option>
							<option value="49" <?php echo set_select('pelat_nomor', '49'); ?> >DD - Sulawesi Selatan</option>
							<option value="51" <?php echo set_select('pelat_nomor', '51'); ?> >DE - Maluku</option>
							<option value="52" <?php echo set_select('pelat_nomor', '52'); ?> >DG - Maluku Utara</option>
							<option value="37" <?php echo set_select('pelat_nomor', '37'); ?> >DH - Kupang, TTU, TTS, Rote Ndao</option>
							<option value="34" <?php echo set_select('pelat_nomor', '34'); ?> >DK - Bali</option>
							<option value="45" <?php echo set_select('pelat_nomor', '45'); ?> >DL - Kepulauan Sangihe, Kepulauan Talaud</option>
							<option value="46" <?php echo set_select('pelat_nomor', '46'); ?> >DM - Gorontalo</option>
							<option value="47" <?php echo set_select('pelat_nomor', '47'); ?> >DN - Sulawesi Tengah</option>
							<option value="35" <?php echo set_select('pelat_nomor', '35'); ?> >DR - Mataram, Lombok Barat, Lombok Timur, Lombok Tengah</option>
							<option value="53" <?php echo set_select('pelat_nomor', '53'); ?> >DS - Papua dan Papua Barat</option>
							<option value="48" <?php echo set_select('pelat_nomor', '48'); ?> >DT - Sulawesi Tenggara</option>
							<option value="15" <?php echo set_select('pelat_nomor', '15'); ?> >E - Cirebon, Indramayu, Majalengka, Kuningan</option>
							<option value="36" <?php echo set_select('pelat_nomor', '36'); ?> >EA - Sumbawa Barat, Sumbawa, Dompu, Bima</option>
							<option value="38" <?php echo set_select('pelat_nomor', '38'); ?> >EB - Manggarai Barat, Manggarai, Ngada, Ende, Sikka, Flores Timur, Lembata, Alor</option>
							<option value="39" <?php echo set_select('pelat_nomor', '39'); ?> >ED - Sumba Barat, Sumba Timur</option>
							<option value="16" <?php echo set_select('pelat_nomor', '16'); ?> >F - Bogor, Cianjur, Sukabumi</option>
							<option value="19" <?php echo set_select('pelat_nomor', '19'); ?> >G - Pekalongan, Tegal, Brebes, Batang, Pemalang</option>
							<option value="20" <?php echo set_select('pelat_nomor', '20'); ?> >H - Semarang, Salatiga, Kendal, Demak</option>
							<option value="21" <?php echo set_select('pelat_nomor', '21'); ?> >K - Pati, Kudus, Jepara, Rembang, Blora, Grobogan</option>
							<option value="40" <?php echo set_select('pelat_nomor', '40'); ?> >KB - Kalimantan Barat</option>
							<option value="42" <?php echo set_select('pelat_nomor', '42'); ?> >KH - Kalimantan Tengah</option>
							<option value="43" <?php echo set_select('pelat_nomor', '43'); ?> >KT - Kalimantan Timur</option>
							<option value="26" <?php echo set_select('pelat_nomor', '26'); ?> >L - Surabaya</option>
							<option value="27" <?php echo set_select('pelat_nomor', '27'); ?> >M - Madura, Pamekasan, Sumenep, Sampang, Bangkalan</option>
							<option value="28" <?php echo set_select('pelat_nomor', '28'); ?> >N - Malang, Probolinggo, Pasuruan, Lumajang, Batu</option>
							<option value="29" <?php echo set_select('pelat_nomor', '29'); ?> >P - Besuki, Bondowoso, Situbondo, Jember, Banyuwangi</option>
							<option value="22" <?php echo set_select('pelat_nomor', '22'); ?> >R - Banyumas, Cilacap, Purbalingga, Banjarnegara</option>
							<option value="30" <?php echo set_select('pelat_nomor', '30'); ?> >S - Bojonegoro, Mojokerto, Tuban, Lamongan, Jombang</option>
							<option value="17" <?php echo set_select('pelat_nomor', '17'); ?> >T - Purwakarta, Karawang, sebagian Bekasi, Subang</option>
							<option value="31" <?php echo set_select('pelat_nomor', '31'); ?> >W - Sidoarjo, Gresik</option>
							<option value="18" <?php echo set_select('pelat_nomor', '18'); ?> >Z - Garut, Tasikmalaya, Sumedang, Ciamis, Banjar</option>
						</select>
					</div>
					<div class="clearfix visible-sm visible-md visible-lg"></div>
						
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<label for="Perluasan Jaminan" class="control-label">Perluasan Jaminan:</label>
					</div>
					<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
						<div class="checkbox">
							<label><input type="checkbox" name="perluasan[]" value="1" <?php echo set_checkbox('perluasan[]', '1'); ?> ></input>Banjir termasuk Angin Topan</label>
						</div>
					</div>
					<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
						<div class="checkbox">
							<label><input type="checkbox" name="perluasan[]" value="2" <?php echo set_checkbox('perluasan[]', '2'); ?> ></input>Gempa Bumi, Tsunami</label>
						</div>
					</div>
					<div class="clearfix visible-sm"></div>
					<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
						<div class="checkbox">
							<label><input type="checkbox" name="perluasan[]" value="3" <?php echo set_checkbox('perluasan[]', '3'); ?> ></input>Huru-hara dan Kerusuhan (SRCC)</label>
						</div>
					</div>
					<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
						<div class="checkbox">
							<label><input type="checkbox" name="perluasan[]" value="4" <?php echo set_checkbox('perluasan[]', '4'); ?> ></input>Terorisme dan Sabotase (TS)</label>
						</div>
					</div>
					<div class="clearfix visible-sm visible-md visible-lg"></div>
					<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
						<div class="checkbox">
							<label><input type="checkbox" name="perluasan[]" id="tjh" value="5" <?php echo set_checkbox('perluasan[]', '5'); ?> ></input>Tanggung Jawab Hukum terhadap Pihak Ketiga (TJH)</label>
							<div id="grup_tjh">
								<div class="input-group">
									<span class="input-group-addon">Rp</span>
									<input type="text" name="limit_tjh" id="limit_tjh" value="<?php echo set_value('limit_tjh'); ?>" class="form-control" placeholder="Limit TJH"></input>
									<span class="input-group-addon">,00</span>
								</div>
								<?php echo form_error('limit_tjh','<p class="text-danger">','</p>'); ?>
							</div>
						</div>
					</div>
					<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
						<div class="checkbox">
							<label><input type="checkbox" name="perluasan[]" id="pad" value="6" <?php echo set_checkbox('perluasan[]', '6'); ?> ></input>Kecelakaan Diri untuk Pengemudi (PA Driver)</label>
							<div id="grup_pad">
								<div class="input-group">
									<span class="input-group-addon">Rp</span>
									<input type="text" name="limit_pad" id="limit_pad" value="<?php echo set_value('limit_pad'); ?>" class="form-control" placeholder="Limit PAD"></input>
									<span class="input-group-addon">,00</span>
								</div>
								<?php echo form_error('limit_pad','<p class="text-danger">','</p>'); ?>
							</div>
						</div>
					</div>
					<div class="clearfix visible-sm"></div>
					<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
						<div class="checkbox">
							<label><input type="checkbox" name="perluasan[]" id="pap" value="7" <?php echo set_checkbox('perluasan[]', '7'); ?> ></input>Kecelakaan Diri untuk Penumpang (PA Passenger)</label>
							<div id="grup_pap">
								<div class="input-group">
									<span class="input-group-addon">Rp</span>
									<input type="text" name="limit_pap" id="limit_pap" value="<?php echo set_value('limit_pap'); ?>" class="form-control" placeholder="Limit PAP"></input>
									<span class="input-group-addon">,00</span>
								</div>
								<?php echo form_error('limit_pap','<p class="text-danger">','</p>'); ?>
							</div>
						</div>
					</div>
					<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3 form-group" id="grup_seat">
						<label for="jml_seat" class="control-label">Jumlah <em>Seat</em> yang Di-<em>cover</em> untuk Penumpang</label>
						<div class="input-group">
							<input type="text" name="jml_seat" id="jml_seat" value="<?php echo set_value('jml_seat'); ?>" class="form-control" placeholder="Jumlah Seat"></input>
						</div>
						<?php echo form_error('jml_seat','<p class="text-danger">','</p>'); ?>
					</div>
					<div class="clearfix visible-sm visible-md visible-lg"></div>	
					<div class="clearfix">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group">
							<div class="pull-right text-center">
								<input type="submit" value="Cari Premi" class="btn btn-primary"></input>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<div class="container-fluid search-auto-price">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<h3 class="text-center">Bingung Memperkirakan Harga Kendaraan Anda?</h3>
				<p class="text-center">Jika Anda mengalami kesulitan dalam memperkirakan harga kendaraan Anda,
				coba cari harga kendaraan Anda melalui pencarian OLX.co.id di bawah ini.</p>
				<center>
				<form name="cariolx" class="form-inline" id="cariOlx" role="form">
					<div class="form-group">
						Cari melalui <img src="<?php echo base_url('assets/images/interface/olx.png'); ?>" width="50" height="auto">&nbsp
						<div class="input-group">
							<span class="input-group-addon glyphicon glyphicon-search"></span>
							<input type="search" name="keyword" id="keyword" class="form-control" placeholder="Masukkan pencarian Anda" required="required"></input>
						</div>
					</div>
					<input type="submit" value="Cari" class="btn btn-primary"></input>
				</form>
				</center>
			</div>
		</div>
	</div>
</div>
<div class="container-fluid learn-auto-insurance">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<h3 class="text-center">Pelajari Asuransi Kendaraan</h3>
				<h4>Asuransi Kendaraan</h4>
				<p align="justify">
					<strong class="text-info">Nama Produk</strong>
					<br>
					Polis Standar Asuransi Kendaraan Bermotor Indonesia
				</p>
				<p align="justify">
					<strong class="text-info">Definisi</strong>
					<br>
					Merupakan polis standar yang dikeluarkan oleh AAUI yang digunakan di Indonesia dan dirancang untuk menutup pertanggungan gabungan yakni:
					<ul>
						<li>Pertanggungan atas kendaraan bermotor itu sendiri.</li>
						<li>Pertanggungan tanggung jawab hukum (TJH) terhadap pihak ketiga.</li>
					</ul>
				</p>
				<p align="justify">
					<strong class="text-info">Manfaat</strong>
					<br>
					Jenis Pertanggungan adalah:
					<ul>
						<li>
							<strong>Gabungan (<em>Comprehensive</em>)</strong>
							<br>
							Kondisi ini memberikan jaminan kerugian atau kerusakan baik sebagian maupun total loss atas
							kendaraan bermotor yang diakibatkan oleh resiko-resiko yang disebutkan di dalam polis.
							<br>
							Resiko-resiko yang dijamin dalam polis:
							<ul>
								<li>Tabrakan, benturan, terbalik, tergelincir atau terperosok.</li>
								<li>Perbuatan jahat.</li>
								<li>Pencurian termasuk pencurian yang didahului atau diikuti tindak kekerasan.</li>
								<li>Kebakaran.</li>
								<li>Sebab-sebab selama penyeberangan dengan <em>ferry</em>.</li>
								<li>Kerusakan roda bila mengakibatkan kerusakan kendaraan akibat kecelakaan.</li>
								<li>Biaya-biaya penjagaan/pengangkutan ke bengkel terdekat.</li>
							</ul>
						</li>
						<li>
							<strong>Kerugian <em>Total Loss</em> (TLO)</strong>
							<br>
							Memberikan jaminan atas kerugian yang diakibatkan oleh resiko yang disebutkan di dalam polis
							dimana biaya perbaikannya sama atau lebih besar dari 75% harga kendaraan atau kendaraan
							hilang dicuri dan tidak diketemukan dalam waktu 60 hari.
						</li>
						<li>
							<strong>Perluasan Jaminan</strong>
							<br>
							Dengan tambahan premi jaminan bisa diperluas dengan resiko-resiko sebagai berikut:
							<ul>
								<li>Tanggung jawab hukum terhadap pihak ke-3.</li>
								<li>Kecelakaan diri terhadap penumpang dan atau pengemudi.</li>
								<li>Kerusuhan, pemogokan, penghalangan bekerja, perbuatan jahat serta penjarahan yang terjadi selama kerusuhan, huru hara, teroris dan sabotase.</li>
								<li>Bencana Alam seperti banjir, gempa bumi dan lain-lain.</li>
							</ul>
						</li>
					</ul>
				</p>
				<p align="justify">
					<strong class="text-info">Yang wajib dilaksanakan ketika membeli produk tersebut</strong>
					<ul>
						<li>Mempelajari dengan baik proposal penawaran yang diajukan oleh agen/broker terutama atas resiko yang dijamin dan tidak dijamin, persyaratan-persyaratan yang harus dipenuhi, cara pembayaran premi, kewajiban tertanggung dalam hal terjadi kerugian atau kerusakan.</li>
						<li>Memastikan kesehatan keuangan dari perusahaan Asuransi yang akan menjamin resiko dan manfaat-manfaat tambahan yang dimiliki oleh perusahaan Asuransi seperti Bengkel, Mobil Derek, Mobil Pengganti dan lainnya.</li>
						<li>Menanyakan kartu keagenan dari agen yang menawarkan jika melalui agen.</li>
						<li>Mengisi Surat Permohonan Penutupan Asuransi dengan data yang sebenar-benarnya secara lengkap dan ditandatangani oleh calon tertanggung sendiri.</li>
					</ul>
					Data yang diminta biasanya terkait dengan:
					<ul>
						<li>Jenis kendaraan.</li>
						<li>Spesifikasi kendaraan seperti no rangka, no mesin, tahun produksi.</li>
						<li>Penggunaan Kendaraan.</li>
						<li>Perlengkapan tambahan.</li>
						<li>Nilai pertanggungan (harga kendaraan).</li>
						<li>Periode pertanggungan.</li>
						<li><em>Loss record</em>/pengalaman klaim.</li>
						<li>Membantu <em>surveyor</em> dari perusahaan Asuransi jika ditunjuk untuk melakukan <em>survey</em> ke objek Asuransi sebelum penutupan Asuransi.</li>
					</ul>
				</p>
				<p align="justify">
					<strong class="text-info">Dengan siapa produk tersebut bisa didapatkan?</strong>
					<br>Produk tersebut bisa didapatkan melalui:
					<ul>
						<li>Agen Asuransi yang bersertifikat.</li>
						<li>Broker Asuransi terutama untuk resiko yang komplit.</li>
						<li>Langsung menghubungi perusahaan Asuransi yang menjamin resiko tersebut baik melalui <em>call center</em>, <em>internet</em>, atau mendatangi langsung.</li>
					</ul>
				</p>
				<p align="justify">
					<strong class="text-info">Apa yang harus diperhatikan dalam membeli produk tersebut?</strong>
					<ul>
						<li>Surat penawaran dari perusahaan.</li>
						<li>Memastikan agen yang bersertifikat.</li>
						<li>SPPA.</li>
						<li>Memastikan data-data dalam SPPA telah sesuai dengan kondisi yang sebenarnya.</li>
						<li>Membaca kontrak/polis secara seksama dan menanyakan ke agen/perusahaan jika terdapat keraguan atas kondisi polis.</li>
						<li>Meminta perubahan (<em>endorsement</em>) jika terdapat kesalahan data dalam polis yang diberikan.</li>
						<li>Besaran <em>Own Retention</em>/<em>Deductible</em> per kejadian.</li>
					</ul>
				</p>
				<p align="justify">
					<strong class="text-info">Apa yang harus dilakukan ketika tidak sesuai dengan apa yang diperjanjikan?</strong>
					<br>
					Sesuai kondisi polis dalam PSAKBI yang dapat dilakukan adalah:
					<ul>
						<li>Meminta klarifikasi ke perusahaan baik melalui agen maupun langsung ke perusahaan untuk proses perdamaian atau musyawarah antara pihak-pihak.</li>
						<li>Mengadukan ke Badan Mediasi Asuransi Indonesia untuk nilai klaim yang bermasalah hingga Rp750.000.000,-</li>
						<li>Jika masih belum menemukan titik temu, dapat memilih penyelesaian sengketa melalui arbitrase atau penyelesaian sengketa melalui pengadilan.</li>
					</ul>
				</p>
				<small>
				sumber: 
					<cite>https://sikapiuangmu.ojk.go.id/FrontEnd/CMS/Category/52</cite>
				</small>
			</div>
		</div>
	</div>
</div>