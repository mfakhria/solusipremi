<script src="<?php echo base_url('assets/jquery-maskmoney/jquery.maskMoney.min.js'); ?>" type="text/javascript"></script>
<script src="<?php echo base_url('assets/jquery-chained/jquery.chained.min.js'); ?>"></script>
<section class="parallax-window" data-parallax="scroll" style="background-image: url('<?php echo base_url(); ?>assets/images/header/property-insurance-bg.jpg'); background-repeat: no-repeat; background-size: 100%" data-natural-width="1400" data-natural-height="470">
    <div class="parallax-content-1">
        <div class="animated fadeInDown">
            <div id="search_bar_container" style="background:transparent;">
                <div class="container">
                	<div class="row">
						<div class="col-xs-12 col-sm-12 col-lg-12">
							<div class="page-heading">
								<h1>Asuransi Properti</h1>
								<p>
									Asuransi Properti melindungi properti Anda dari resiko kebakaran,
									banjir, gempa bumi, maupun tindakan kejahatan dan lainnya.
								</p>
						    </div>
						</div>
					</div>
                </div>
            </div>
        </div>
    </div>
</section>
<div class="container-fluid property-premium-search-form">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 premium-search-box">
				<h3 class="text-center">Asuransi Properti</h3>
				<hr>
				<?php echo form_open('asuransi-properti/cari-asuransi', 'role="form"'); ?>
					<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3 form-group">
						<label for="nilai_bangunan" class="control-label">Nilai Bangunan</label>
						<span class="glyphicon glyphicon-question-sign nilai-bangunan" style="cursor: pointer;" title="<strong>Nilai Bangunan</strong>" 
							data-container="body" data-toggle="popover" data-placement="right" data-trigger="click hover" 
							data-content="<p>Rumus:<br>Luas Bangunan Keseluruhan &times; Rp3.000.000 per m<sup>2</sup></p>
							<p>Contoh:<br>
							Rumah dua lantai. Luas tiap lantai adalah 50m<sup>2</sup>.<br>
							Maka, Luas Bangunan Keseluruhan:<br>
							50m<sup>2</sup> &times; 2 lantai = 100m<sup>2</sup>.<br>
							Berarti, Nilai Bangunan tersebut menurut estimasi asuransi adalah:<br>
							100m<sup>2</sup> &times; Rp3.000.000 per m<sup>2</sup> = Rp300.000.000</p>"></span>
						<div class="input-group">
							<span class="input-group-addon">Rp</span>
							<input type="text" name="nilai_bangunan" id="nilai_bangunan" value="<?php echo set_value('nilai_bangunan'); ?>" class="form-control" placeholder="Jumlah"></input>
							<span class="input-group-addon">,00</span>
						</div>
						<?php echo form_error('nilai_bangunan','<p class="text-danger">','</p>'); ?>
					</div>
					<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3 form-group">
						<label for="nilai_perabotan" class="control-label">Nilai Perabotan</label>
						<div class="input-group">
							<span class="input-group-addon">Rp</span>
							<input type="text" name="nilai_perabotan" id="nilai_perabotan" value="<?php echo set_value('nilai_perabotan'); ?>" class="form-control" placeholder="Jumlah"></input>
							<span class="input-group-addon">,00</span>
						</div>
						<?php echo form_error('nilai_perabotan','<p class="text-danger">','</p>'); ?>
					</div>
					<div class="clearfix visible-sm"></div>
					<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3 form-group">
						<label for="nilai_mesin" class="control-label">Nilai Mesin</label>
						<div class="input-group">
							<span class="input-group-addon">Rp</span>
							<input type="text" name="nilai_mesin" id="nilai_mesin" value="<?php echo set_value('nilai_mesin'); ?>" class="form-control" placeholder="Jumlah"></input>
							<span class="input-group-addon">,00</span>
						</div>
						<span class="help-block">Apabila merupakan pabrik/industri.</span>
						<?php echo form_error('nilai_mesin','<p class="text-danger">','</p>'); ?>
					</div>
					<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3 form-group">
						<label for="nilai_stok" class="control-label">Nilai Stok</label>
						<div class="input-group">
							<span class="input-group-addon">Rp</span>
							<input type="text" name="nilai_stok" id="nilai_stok" value="<?php echo set_value('nilai_stok'); ?>" class="form-control" placeholder="Jumlah"></input>
							<span class="input-group-addon">,00</span>
						</div>
						<span class="help-block">Apabila merupakan pabrik, industri, toko, gudang, dan sejenisnya.</span>
						<?php echo form_error('nilai_stok','<p class="text-danger">','</p>'); ?>
					</div>
					<div class="clearfix visible-sm visible-md visible-lg"></div>
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group ui-widget">
						<label for="penggunaan_bangunan" class="control-label">Penggunaan Bangunan</label>
						<?php echo form_error('penggunaan_bangunan','<p class="text-danger">','</p>'); ?>
						<input type="text" class="form-control" name="penggunaan_bangunan" id="penggunaan_bangunan" placeholder="Penggunaan Bangunan sebagai" />
					</div>
					<div class="col-xs-12 col-sm-4 col-md-3 col-lg-3 form-group">
						<label for="kelas_konstruksi" class="control-label">Kelas Konstruksi</label>
						<select name="kelas_konstruksi" id="kelas_konstruksi" onchange="keterangan()" class="form-control">
							<option value="1">Kelas Konstruksi 1</option>
							<option value="2">Kelas Konstruksi 2</option>
							<option value="3">Kelas Konstruksi 3</option>
						</select>
						<script type="text/javascript">
						   function keterangan() {
							var ket = document.getElementById("kelas_konstruksi").value;
							if (ket == 1) {
								document.getElementById('keterangan').innerHTML = "Bangunan dikatakan berkonstruksi kelas 1 (satu) apabila dinding, lantai, dan semua komponen penunjang strukturalnya serta penutup atap terbuat seluruhnya dan sepenuhnya dari bahan yang tidak mudah terbakar. Jendela dan/atau pintu beserta kerangkanya, dinding partisi, dan penutup lantai boleh diabaikan.";
							}else if (ket == 2) {
						        document.getElementById('keterangan').innerHTML = "Bangunan dikatakan berkonstruksi kelas 2 (dua) adalah bangunan yang kriterianya sama seperti apa yang disebutkan dalam bangunan berkonstruksi kelas 1 (satu), dengan kelonggaran penutup atap boleh terbuat dari sirap kayu keras, dinding boleh mengandung bahan yang dapat terbakar sampai maksimun 20% (dua puluh persen) dari luas dinding, serta lantai dan struktur penunjangnya boleh terbuat dari kayu.";
							}else{
								document.getElementById('keterangan').innerHTML = "Semua bangunan selain yang disebutkan pada kelas konstruksi 1 (satu) dan konstruksi 2 (dua).";
							}
						}
						
						</script>
					</div>
					<div class="col-xs-12 col-sm-8 col-md-9 col-lg-9 form-group">
						<label for="keterangan" class="control-label">Keterangan</label>
						<p class="text-info" id="keterangan"></p>
					</div>
					<div class="clearfix visible-sm visible-md visible-lg"></div>
					<div class="col-xs-12 col-sm-2 col-md-2 col-lg-2 form-group">
						<label for="jumlah_lantai" class="control-label">Jumlah Lantai</label>
						<input type="text" name="jumlah_lantai" id="jumlah_lantai" value="<?php echo set_value('jumlah_lantai'); ?>" class="form-control" placeholder="Jumlah" required="required"></input>
						<?php echo form_error('jumlah_lantai','<p class="text-danger">','</p>'); ?>
					</div>
					<div class="col-xs-12 col-sm-5 col-md-5 col-lg-5 form-group">
						<label for="provinsi" class="control-label">Provinsi</label>
						<select name="provinsi" id="provinsi" class="form-control">
							<?php
								foreach ($provinsi as $prov)
								{
							?>
								<option <?php echo $provinsi_selected == $prov->id_provinsi ? 'selected="selected"' : ''; ?> 
								value="<?php echo $prov->id_provinsi; ?>" <?php echo set_select('provinsi', $prov->id_provinsi); ?> ><?php echo $prov->nama_provinsi; ?></option>
							<?php
								}
							?>
						</select>
					</div>
					<div class="col-xs-12 col-sm-5 col-md-5 col-lg-5 form-group">
						<label for="kab_kota" class="control-label">Kota atau Kabupaten</label>
						<select name="kab_kota" id="kab_kota" class="form-control">
							<?php
								foreach ($kota as $kot)
								{
							?>
								<option <?php echo $kota_selected == $kot->id_kab_kota ? 'selected="selected"' : ''; ?>
								class="<?php echo $kot->id_provinsi; ?>" value="<?php echo $kot->id_kab_kota; ?>" <?php echo set_select('kab_kota', $kot->id_kab_kota); ?> ><?php echo $kot->nama_kab_kota; ?></option>
							<?php
								}
							?>
						</select>
					</div>
					<div class="clearfix visible-sm visible-md visible-lg"></div>
					
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<label for="Perluasan Jaminan" class="control-label">Perluasan Jaminan:</label>
					</div>
					<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
						<div class="checkbox">
							<label><input type="checkbox" name="perluasan[]" value="1" <?php echo set_checkbox('perluasan[]', '1'); ?> ></input>Banjir</label>
						</div>
					</div>
					<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
						<div class="checkbox">
							<label><input type="checkbox" name="perluasan[]" value="2" <?php echo set_checkbox('perluasan[]', '2'); ?> ></input>Gempa Bumi</label>
						</div>
					</div>
					<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
						<div class="checkbox">
							<label><input type="checkbox" name="perluasan[]" value="3" <?php echo set_checkbox('perluasan[]', '3'); ?> ></input>RSMD CC + Others</label>
						</div>
					</div>
					<div class="clearfix visible-sm visible-md visible-lg"></div>
					<div class="clearfix">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group">
							<div class="pull-right text-center">
								<input type="submit" value="Cari Premi" class="btn btn-primary"></input>
							</div>
						</div>
					</div>	
				</form>
			</div>
		</div>
	</div>
</div>
<div class="container-fluid learn-property-insurance">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<h3 class="text-center">Pelajari Asuransi Properti</h3>
				<h4>Asuransi Properti</h4>
				<p align="justify">
					<strong class="text-info">Nama Produk</strong>
					<br>
					<em>Property All Risk</em> / <em>Industrial All Risk</em>
				</p>
				<p align="justify">
					<strong class="text-info">Definisi</strong>
					<br>
					Merupakan polis Asuransi kebakaran yang bersifat <em>unnamed perils</em>, yaitu memberikan jaminan untuk seluruh resiko yang terjadi pada harta benda dan atau kepentingan yang dipertanggungkan kecuali resiko-resiko yang terdapat pada pengecualian, yaitu:
					<ul>
						<li>Kerusakan mesin karena pemakaian.</li>
						<li><em>Wear and tear and gradual</em>.</li>
						<li>Karena sifat benda atau barang itu sendiri.</li>
						<li>Nuklir, reaksi atom, radio aktif dan sejenisnya.</li>
						<li>Perang termasuk perang saudara.</li>
						<li><em>Property</em> dalam pengangkutan atau berada di tempat lain.</li>
						<li><em>Waterbome or airbome property</em>.</li>
						<li><em>Unexplained disappearance</em>.</li>
						<li><em>Testing involving abnormal conditions or intentional overloading</em>.</li>
						<li>Niat jahat dari orang-orang atau pekerja tertanggung.</li>
					</ul>
				</p>
				<p align="justify">
					<strong class="text-info">Manfaat</strong>
					<br>
					<ul>
						<li>PAR untuk bangunan nonindustri seperti kantor, rumah tinggal, rumah sakit, sekolah, dll.</li>
						<li>IAR untuk bangunan industri seperti pabrik, gudang, toko, <em>mall</em>, dll.</li>
					</ul>
				</p>
				<p align="justify">
					<strong class="text-info">Yang wajib dilaksanakan ketika membeli produk tersebut</strong>
					<ul>
						<li>Mempelajari dengan baik proposal penawaran yang diajukan oleh agen/broker terutama atas resiko yang dijamin dan tidak dijamin, persyaratan-persyaratan yang harus dipenuhi, cara pembayaran premi, kewajiban tertanggung dalam hal terjadi kerugian atau kerusakan.</li>
						<li>Memastikan kesehatan keuangan dari perusahaan Asuransi yang akan menjamin resiko.</li>
						<li>Menanyakan kartu keagenan dari agen yang menawarkan jika melalui agen.</li>
						<li>Mengisi Surat Permohonan Penutupan Asuransi dengan data yang sebenar-benarnya secara lengkap dan ditandatangani oleh calon tertanggung sendiri.</li>
					</ul>
					Data yang diminta biasanya terkait dengan:
					<ul>
						<li>Jenis obyek pertanggungan.</li>
						<li>Konstruksi bangunan.</li>
						<li>Okupasi obyek pertanggungan.</li>
						<li>Jenis kegiatan Tertanggung.</li>
						<li><em>Surrounding risk</em>.</li>
						<li>Sarana di lokasi obyek pertanggungan.</li>
						<li>Luas jaminan yang diminta.</li>
						<li>Nilai pertanggungan.</li>
						<li>Periode pertanggungan.</li>
						<li><em>Loss record</em>/pengalaman klaim.</li>
						<li>Membantu <em>surveyor</em> dari perusahaan Asuransi jika ditunjuk untuk melakukan <em>survey</em> ke objek Asuransi sebelum penutupan Asuransi.</li>
					</ul>
				</p>
				<p align="justify">
					<strong class="text-info">Dengan siapa produk tersebut bisa didapatkan?</strong>
					<br>Produk tersebut bisa didapatkan melalui:
					<ul>
						<li>Agen Asuransi yang bersertifikat.</li>
						<li>Broker Asuransi terutama untuk resiko yang komplit.</li>
						<li>Langsung menghubungi perusahaan Asuransi yang menjamin resiko tersebut.</li>
					</ul>
				</p>
				<p align="justify">
					<strong class="text-info">Apa yang harus diperhatikan dalam membeli produk tersebut?</strong>
					<ul>
						<li>Surat penawaran dari perusahaan.</li>
						<li>Memastikan agen yang bersertifikat.</li>
						<li>SPPA.</li>
						<li>Memastikan data-data dalam SPPA telah sesuai dengan kondisi yang sebenarnya.</li>
						<li>Membaca kontrak/polis secara seksama dan menanyakan ke agen/perusahaan jika terdapat keraguan atas kondisi polis.</li>
						<li>Meminta perubahan (<em>endorsement</em>) jika terdapat kesalahan data dalam polis yang diberikan.</li>
					</ul>
				</p>
				<p align="justify">
					<strong class="text-info">Apa yang harus dilakukan ketika tidak sesuai dengan apa yang diperjanjikan?</strong>
					<br>Mengacu kepada kondisi polis yang telah disepakati dalam penyelesaian perselisihan, tindakan yang dapat dilakukan antara lain:
					<ul>
						<li>Meminta klarifikasi ke perusahaan baik melalui agen maupun langsung ke perusahaan untuk proses perdamaian atau musyawarah antara pihak-pihak.</li>
						<li>Mengadukan ke Badan Mediasi Asuransi Indonesia untuk nilai klaim yang bermasalah hingga Rp750.000.000,-</li>
						<li>Jika masih belum menemukan titik temu dapat memilih penyelesaian sengketa melalui arbitrase atau penyelesaian sengketa melalui pengadilan.</li>
					</ul>
				</p>
				<small>
				sumber: 
					<cite>https://sikapiuangmu.ojk.go.id/FrontEnd/CMS/Category/53</cite>
				</small>
			</div>
		</div>
	</div>
</div>
<script src="<?php echo base_url('assets/js/my_jquery_function.js'); ?>" type="text/javascript"></script>
