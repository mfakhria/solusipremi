<footer>
    <div class="container-fluid footer_bg pdt5i pdb20i">
        <div class="container">
            <div class="row">
                <div class="col-sm-3">
                    <div style="margin-top: 20px;">
                        <a href="<?php echo base_url()?>"><img src="<?php echo base_url('assets/images/interface/logo-solusipremi.png'); ?>" class="img-responsive" width="150" height="auto" alt="solusipremi.com">
						</a>
                        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>                    </div>
                </div>
                <div class=" col-sm-3">
                    <h3>Recent Policies</h3>
                    <ul>
						<li><a href="../policy/car-insurance-397/index.html">car insurance</a></li><li><a href="../policy/test/index.html">test</a></li><li><a href="../policy/car-insurance-451/index.html">car insurance </a></li><li><a href="../policy/single-trip-travel-insurance/index.html">Single Trip Travel Insurance</a></li>                    </ul>
                </div>
                <div class=" col-sm-3">
                    <h3>Top Categories</h3>
                    <ul>
                        <li><a href="../policies/indexcba9.html?catId=NA">Vehicle</a></li><li><a href="../policies/index43cf.html?catId=MQ">Health</a></li><li>                    </ul>
                </div>
                <div class="col-sm-3">
                    <h3>Contact Us</h3>
                    <p class="text-left">
                        Place Charles de Gaulle, 75008 Paris</p>
                        <div class="social_footer">
                        <ul>
                            <li><a href="#" class="fa fa-facebook" style="text-decoration : none"></i></a></li>
                            <li><a href="#" class="fa fa-twitter" style="text-decoration : none"></i></a></li>
                            <li><a href="#" class="fa fa-google" style="text-decoration : none"></i></a></li>
                            <li><a href="#" class="fa fa-instagram" style="text-decoration : none"></i></a></li>
                            <li><a href="#" class="fa fa-linkedin" style="text-decoration : none"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid footer_btm_bg">
        <div class="container">
            <div class="row">
                <div class="row pdt10i pdb10i">
                    <div class="col-sm-12 text-center">
                        <p><a href="terms/index.html">copyright ©2018 solusipremi.com. All Rights Reserved.</a></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<script src="<?php echo base_url('assets/js/functions.js');?>"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.11/jquery-ui.min.js"></script>