<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><?php echo $title; ?></title>
		<meta name="keywords" content="asuransi, asuransi kendaraan, asuransi properti, asuransi pengiriman barang,
		asuransi penjaminan, asuransi perjalanan, asuransi kesehatan, asuransi jiwa, portal asuransi, portal asuransi terbaik,
		portal asuransi terbaik di indonesia, cek premi, bandingkan asuransi, bandingkan premi, beli asuransi, klaim asuransi"></meta>
		<meta name="description" content="solusipremi.com adalah portal asuransi yang tepat untuk mendapatkan kebutuhan asuransi terbaik Anda.
		Kami bantu Anda mengurus kebutuhan asuransi Anda dengan mitra perusahaan asuransi terbaik kami, tanpa biaya tambahan."></meta>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"></meta>
		<meta name="viewport" content="width=device-width, initialscale=1.0">
	
		<link rel="stylesheet" href="<?php echo base_url('assets/ui/1.12.1/jquery-ui.css'); ?>">
		<!-- Bootstrap -->
		<link rel="stylesheet" href="<?php echo base_url('assets/css/responsive.css'); ?>" type="text/css">
		<link rel="stylesheet" href="<?php echo base_url('assets/css/style.css'); ?>" type="text/css">
		<link rel="stylesheet" href="<?php echo base_url('assets/css/style2.css'); ?>" type="text/css">
		<link rel="stylesheet" href="<?php echo base_url('assets/css/style-real.css'); ?>" type="text/css">
		<link rel="shortcut icon" href="<?php echo base_url('assets/img/favicon.png'); ?>" type="image/x-icon">
	    <link rel="apple-touch-icon" type="image/x-icon" href="<?php echo base_url('assets/img/apple-touch-icon-57x57-precomposed.html'); ?>">
	    <link rel="apple-touch-icon" type="image/x-icon" sizes="72x72" href="<?php echo base_url('assets/img/apple-touch-icon-72x72-precomposed.html'); ?>">
	    <link rel="apple-touch-icon" type="image/x-icon" sizes="114x114" href="<?php echo base_url('assets/img/apple-touch-icon-114x114-precomposed.html'); ?>">
	    <link rel="apple-touch-icon" type="image/x-icon" sizes="144x144" href="<?php echo base_url('assets/img/apple-touch-icon-144x144-precomposed.html'); ?>">
	    <link href="<?php echo base_url('assets/css/base.css'); ?>" rel="stylesheet">
	    <link href="<?php echo base_url('assets/css/hover.css'); ?>" rel="stylesheet">
		<link rel="stylesheet" href="<?php echo base_url('assets/rs-plugin/css/settings.css'); ?>">
		<link href="<?php echo base_url('assets/css/extralayers.css'); ?>" rel="stylesheet">
		<link href="<?php echo base_url('assets/css/skins/square/grey.css'); ?>" rel="stylesheet">
		<link href="<?php echo base_url('assets/css/ion.rangeSlider.css'); ?>" rel="stylesheet" >
	    <link href="<?php echo base_url('assets/css/ion.rangeSlider.skinFlat.css'); ?>" rel="stylesheet">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/plugins/datatable/datatables.min.css'); ?>">
	    <link href='http://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
	    <link href='http://fonts.googleapis.com/css?family=Gochi+Hand' rel='stylesheet' type='text/css'>
	    <link href='http://fonts.googleapis.com/css?family=Lato:300,400' rel='stylesheet' type='text/css'>
	    <link href='http://fonts.googleapis.com/css?family=Lato:300,400' rel='stylesheet' type='text/css'>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
		<meta name="viewport" content="width=device-width, initial-scale=1">

  		
  		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
		<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
		<!--<script src="https://code.jquery.com/jquery.js"></script> -->
		<script src="<?php echo base_url('assets/js/jquery.js'); ?>"></script>
		
		
		<!-- Include all compiled plugins (below), or include individual files as needed -->
		<script src="<?php echo base_url('assets/bootstrap/js/bootstrap.js'); ?>"></script>
	
		
		<link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.css'); ?>" type="text/css">
	
		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and
		media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page
		via file:// -->
		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
			<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
		<script type="text/javascript">
		        $(document).ready(function(){
		            $( "#penggunaan_bangunan" ).autocomplete({
		            	minLength:1,
		            	scroll:true,
		              source: "<?php echo site_url('asuransi-properti/get-nama-okupasi/?');?>"
		            });
		        });
		    </script>
		<style>
	        .ui-autocomplete { 
	            cursor:pointer; 
	            height:120px; 
	            overflow-y:scroll;
	        }    
	    </style>
		<script type="text/javascript">
		function Resolusi() {
			var w = window,
				d = document,
				e = d.documentElement,
				g = d.body,
				x = w.innerWidth || e.clientWidth ||g.clientWidth,
				y = w.innerHeight || e.clientHeight ||g.clientHeight;

				if (x >= 1239 && y >= 600 ) {
					document.getElementById('tombol-modal-depan').innerHTML = "<button type='button' data-toggle='modal' data-target='#myModal'>Open Modal</button>";
					document.getElementById('tombol-modal-kiri').innerHTML = "<button type='button' data-toggle='modal' data-target='#myModal'>Open Modal</button>";
					document.getElementById('tombol-modal-belakang').innerHTML = "<button type='button' data-toggle='modal' data-target='#myModal'>Open Modal</button>";
					document.getElementById('tombol-modal-kanan').innerHTML = "<button type='button' data-toggle='modal' data-target='#myModal'>Open Modal</button>";
				}else{
					document.getElementById('tombol-andro-depan').innerHTML = "<input type='file' name='foto_kendaraan_depan' accept='image/*' capture />";
					document.getElementById('tombol-andro-kiri').innerHTML = "<input type='file' name='foto_kendaraan_kiri' accept='image/*' capture />";
					document.getElementById('tombol-andro-belakang').innerHTML = "<input type='file' name='foto_kendaraan_belakang' accept='image/*' capture />";
					document.getElementById('tombol-andro-kanan').innerHTML = "<input type='file' name='foto_kendaraan_kanan' accept='image/*' capture />";
				}
		}
	</script>

	</head>
	<body onload="Resolusi()">
	 <div class="layer"></div>
    <header id="plain">
        <div class="container" style="height: 50px">
            <div class="row">
                <div class="col-md-2 col-sm-3 col-xs-3" style="margin-top: -10px">
                    <div id="logo_home" style="margin-top: 20px; width: 175px">
                    	<a href="<?php echo base_url(); ?>"><img src="<?php echo base_url('assets/images/interface/logo-solusipremi-black.png'); ?>" class="img-responsive" alt="solusipremi.com">
						</a>
                    </div>
                </div>
                <nav class="col-md-10 col-sm-12 col-xs-12" style="margin-top: 5px">
                    <a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);" style="margin-top: -25px"><span>Menu mobile</span></a>
                    <div class="main-menu">
                        <div id="header_menu">
                            <img src="<?php echo base_url('assets/images/interface/logo-solusipremi-black.png'); ?>" width="160" height="34" alt="solusipremi" data-retina="true">
                        </div>
                        <a href="#" class="open_close" id="close_in"><i class="icon_set_1_icon-77"></i></a>
                        <ul>
                            <li class="submenu">
                            	<a href="<?php echo base_url(); ?>" class="show-submenu" style="text-decoration : none">Home</a>
                            </li>
                            <li class="submenu">
                                <a href="<?php echo site_url('tentang-solusipremi/tentang-kami'); ?>" class="show-submenu" style="text-decoration : none">About Us </a>
                            </li>
                            <li class="submenu">
                                <a href="<?php echo site_url('tentang-solusipremi/partner-kami'); ?>" class="show-submenu" style="text-decoration : none">Search </a>
                            </li>
                            <li class="submenu">
                                <a href="<?php echo site_url('tentang-solusipremi/faq'); ?>" class="show-submenu" style="text-decoration : none">FAQ </a>
                            </li>
                            <li class="submenu">
                                <a href="<?php echo site_url('tentang-solusipremi/kontak-kami'); ?>" class="show-submenu" style="text-decoration : none">Contact </a>
                            </li>
                        <li class="submenu">
                        <?php
                         if (!isset($_SESSION['logged_in'])) { //Jika user belum login
                             echo '<li><a href="'.site_url('auth').'" class="buton_out out ft600" style="text-decoration : none">Login</a></li>';
                             echo '<li><a href="'.site_url('daftar').'" class="buton_out out ft600" style="text-decoration : none">Register</a></li>';
                         } else { //Jika user sudah login
                             echo '<li><a href="'.site_url('account').'" class="buton_out out ft600" style="text-decoration : none"><span class="glyphicon glyphicon-user"></span> '.$_SESSION['username'].'</a></li>';
                             echo '<li><a href="'.site_url('auth/logout').'" class="buton_out out ft600" style="text-decoration : none">Logout</a></li>';
                         }
                        ?>
                        </li>
                    </ul>
                </nav>
            </div>
        </div>
    </header>
    <script src="../assets/js/functions.js"></script>