<div class="container-fluid faq">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12"><br><br><br><br><br>
				<h2 class="text-center" style="color:#575757;letter-spacing:1px;">FAQ</h2>
				<ol>
					<li class="text-info">Apa itu SolusiPremi.com?</li>
						<p align="justify">SolusiPremi.com merupakan salah satu perantara asuransi di Indonesia berupa portal layanan asuransi <em>online</em>.
						Kami membantu mengurus segala keperluan asuransi Anda.</p>
					<li class="text-info">Apa SolusiPremi.com itu perusahaan asuransi?</li>
						<p align="justify">Bukan. SolusiPremi.com <strong>bukan perusahaan asuransi</strong>. SolusiPremi.com merupakan perantara asuransi
						yang menjalin kerja sama dengan perusahaan asuransi terbaik dan terpercaya di Indonesia. Partner asuransi kami telah
						terdaftar dan diawasi oleh Otoritas Jasa Keuangan (OJK).</p>
					<li class="text-info">Mengapa memilih SolusiPremi.com?</li>
						<p align="justify">Kami membantu Anda untuk mencari, menemukan, dan mendapatkan produk asuransi Anda secara <em>online</em> dengan
						mendaftar ke situs kami secara <em>online</em>. Kami akan memberikan informasi secara detail tentang pilihan produk Asuransi
						sesuai kebutuhan Anda sehingga Anda akan mendapatkan hasil terbaik untuk pilihan produk Anda. Sebelum Anda memutuskan produk
						asuransi mana yang ingin Anda ambil, Anda hanya perlu:
						<ul>
							<li>Pilih produk Asuransi dan isi data sesuai keadaan Anda</li>
							<li>Kami akan memberikan perhitungan harga dari partner perusahaan asuransi kami di Indonesia</li>
							<li>Beli secara <em>online</em> dan polis Anda akan aktif</li>
						</ul>
						SolusiPremi.com dapat menjadi referensi bagi Anda untuk mencari harga sebelum menentukan
						untuk membeli produk asuransi kebutuhan Anda.</p>
					<li class="text-info">SolusiPremi.com menyediakan produk asuransi apa saja?</li>
						<p align="justify">Kami menyediakan produk asuransi berupa asuransi kendaraan, asuransi properti, asuransi pengiriman,
						asuransi penjaminan, asuransi perjalanan, asuransi kesehatan, dan asuransi jiwa. Namun, untuk saat ini
						kami hanya menyediakan asuransi kendaraan dan asuransi properti.</p>
					<li class="text-info">Apa saya bisa mendaftar asuransi di SolusiPremi.com?</li>
						<p align="justify">Ya, Anda bisa mendaftar untuk membeli produk asuransi dari partner kami melalui kami.
						Anda akan merasakan kemudahan dalam melakukan perhitungan harga dan mendaftar asuransi secara <em>online</em>.</p>
					<li class="text-info">Apa pendaftaran asuransi di SolusiPremi.com <strong class="text-success">GRATIS</strong>?</li>
						<p align="justify">Ya, Anda bisa mendaftar asuransi di SolusiPremi.com secara <strong class="text-success">GRATIS</strong>. Tidak ada pungutan biaya apapun kepada Anda.</p>
					<li class="text-info">Apa data kami yang dimasukkan ke SolusiPremi.com <strong class="text-success">AMAN</strong>?</li>
						<p align="justify">Ya, seluruh data Anda akan <strong class="text-success">AMAN</strong> bersama kami. Kami memiliki komitmen untuk menjaga privasi terhadap seluruh data dan informasi Anda.</p>
					<li class="text-info">Setelah saya memilih paket asuransi, apa saya bisa mendaftar dan membayar ke perusahaan asuransi tersebut secara langsung?</li>
						<p align="justify">Ya, kami akan menyediakan kontrak Anda dengan perusahaan asuransi. Selanjutnya, Anda bisa proses ke kantor
						perusahaan asuransi tersebut dan membayar kepada mereka secara langsung.</p>
					<li class="text-info">Ketika saya memiliki klaim, bagaimana cara SolusiPremi.com membantu saya?</li>
						<p align="justify">Ketika Anda memiliki klaim, para ahli asuransi kami akan membantu Anda hingga proses klaim selesai. Ketika Anda mengalami kejadian
						yang mana ditanggung oleh polis asuransi Anda, segera hubungi kami di ______________ untuk mengajukan klaim. Kemudian, ahli asuransi
						kami akan membantu Anda untuk melengkapi dokumen dan seluruh informasi yang diperlukan dalam memproses klaim. Selama proses klaim berlangsung,
						ahli asuransi kami akan terus melaporkan perkembangan proses klaim. Ketika proses klaim selesai, ahli asuransi kami akan menghubungi Anda secepatnya.</p>
				</ol>
			</div>
		</div>
	</div>
</div>
<script src="<?php echo base_url('assets/js/functions.js');?>"></script>