<script>
	$(function() {
		$.datepicker.setDefaults({
			dateFormat: "d MM yy",
			changeYear: true,
			currentText: "Hari ini",
			monthNames: [ "Januari","Februari","Maret","April","Mei","Juni","Juli","Agustus","September","Oktober","Nopember","Desember" ],
			monthNamesShort: [ "Jan","Feb","Mar","Apr","Mei","Jun","Jul","Agus","Sep","Okt","Nop","Des" ],
			dayNames: [ "Minggu","Senin","Selasa","Rabu","Kamis","Jumat","Sabtu" ],
			dayNamesShort: [ "Min","Sen","Sel","Rab","Kam","Jum","Sab" ],
			dayNamesMin: [ "Mg","Sn","Sl","Rb","Km","Jm","Sb" ],
			weekHeader: "Mg"
		});
		
		$("#periode_mulai").datepicker({
			minDate: 0,
			appendText: "d MM yyyy",
			onSelect: function(dateStr) {
				var d = $.datepicker.parseDate('d MM yy', dateStr);
				d.setFullYear(d.getFullYear() + 1);
				$("#periode_sampai").datepicker('setDate', d);
			}
		});
		
		$("#periode_sampai").datepicker({
			//dateFormat: "d MM yy",
			minDate: "+1d",
			appendText: "d MM yyyy"
		});
		
	});

</script>
<div class="container-fluid fill-closing-form">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<h3 class="text-center">Isi Data</h3>
				<p align="center">
					Anda akan membeli Asuransi Kendaraan dari partner kami, <strong class="text-info"><?php echo $v_nama_partner; ?></strong>.<br>
					Silakan isi <strong>SURAT PERMINTAAN PERTANGGUNGAN KENDARAAN BERMOTOR (SPPKB)</strong>
					di bawah ini untuk memproses pembelian Asuransi Kendaraan Anda.
				</p>
				<div class="row">
					<div class="col-sm-offset-2 col-md-offset-2 col-lg-offset-2 col-xs-12 col-sm-8 col-md-8 col-lg-8 closing-form">
						<div class="page-header">
							<h4 class="text-center">SURAT PERMINTAAN PERTANGGUNGAN KENDARAAN BERMOTOR (SPPKB)</h4>
						</div>
						<?php echo form_open_multipart('asuransi-kendaraan/isi-data/'.$v_id_partner.'', 'class="form-horizontal" role="form"'); ?>
							<div class="form-group">
								<label for="nomor_ktp" class="col-xs-4 col-sm-4 col-md-4 col-lg-4 control-label">Nomor KTP</label>
								<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
									<input type="text" name="nomor_ktp" id="nomor_ktp" value="<?php echo set_value('nomor_ktp'); ?>" class="form-control" placeholder="Nomor KTP" required="required"></input>
									<?php echo form_error('nomor_ktp', '<p class="text-danger">', '</p>'); ?>
								</div>
							</div>
							<div class="form-group">
								<label for="nama_tertanggung" class="col-xs-4 col-sm-4 col-md-4 col-lg-4 control-label">Nama Tertanggung</label>
								<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
									<input type="text" name="nama_tertanggung" id="nama_tertanggung" value="<?php echo set_value('nama_tertanggung', $v_nama_tertanggung); ?>" class="form-control" placeholder="Nama Anda" required="required"></input>
									<?php echo form_error('nama_tertanggung', '<p class="text-danger">', '</p>'); ?>
								</div>
							</div>
							<div class="form-group">
								<label for="jenis_kelamin" class="col-xs-4 col-sm-4 col-md-4 col-lg-4 control-label">Jenis Kelamin</label>
								<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
									<select name="jenis_kelamin" id="jenis_kelamin" class="form-control">
										<option value="1" <?php echo set_select('jenis_kelamin', '1'); ?> >Laki-Laki</option>
										<option value="2" <?php echo set_select('jenis_kelamin', '2'); ?> >Perempuan</option>
									</select>
								</div>
							</div>
							<div class="form-group">
								<label for="alamat_tertanggung" class="col-xs-4 col-sm-4 col-md-4 col-lg-4 control-label">Alamat Tertanggung</label>
								<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
									<textarea name="alamat_tertanggung" id="alamat_tertanggung" value="<?php echo set_value('alamat_tertanggung'); ?>" class="form-control" placeholder="Alamat Anda" required="required"></textarea>
									<?php echo form_error('alamat_tertanggung', '<p class="text-danger">', '</p>'); ?>
								</div>
							</div>
							<div class="form-group">
								<label for="rt" class="col-xs-4 col-sm-4 col-md-4 col-lg-4 control-label">RT</label>
								<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
									<input type="text" name="rt" id="rt" value="<?php echo set_value('rt'); ?>" class="form-control" placeholder="RT" required="required"></input>
									<?php echo form_error('rt', '<p class="text-danger">', '</p>'); ?>
								</div>
							</div>
							<div class="form-group">
								<label for="rw" class="col-xs-4 col-sm-4 col-md-4 col-lg-4 control-label">RW</label>
								<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
									<input type="text" name="rw" id="rw" value="<?php echo set_value('rw'); ?>" class="form-control" placeholder="RW" required="required"></input>
									<?php echo form_error('rw', '<p class="text-danger">', '</p>'); ?>
								</div>
							</div>
							<div class="form-group">
								<label for="desa_kelurahan" class="col-xs-4 col-sm-4 col-md-4 col-lg-4 control-label">Desa atau Kelurahan</label>
								<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
									<input type="text" name="desa_kelurahan" id="desa_kelurahan" value="<?php echo set_value('desa_kelurahan'); ?>" class="form-control" placeholder="Desa atau Kelurahan Anda" required="required"></input>
									<?php echo form_error('desa_kelurahan', '<p class="text-danger">', '</p>'); ?>
								</div>
							</div>
							<div class="form-group">
								<label for="kecamatan" class="col-xs-4 col-sm-4 col-md-4 col-lg-4 control-label">Kecamatan</label>
								<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
									<input type="text" name="kecamatan" id="kecamatan" value="<?php echo set_value('kecamatan'); ?>" class="form-control" placeholder="Kecamatan Anda" required="required"></input>
									<?php echo form_error('kecamatan', '<p class="text-danger">', '</p>'); ?>
								</div>
							</div>
							<div class="form-group">
								<label for="kab_kota" class="col-xs-4 col-sm-4 col-md-4 col-lg-4 control-label">Kabupaten/Kota</label>
								<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
									<input type="text" name="kab_kota" id="kab_kota" value="<?php echo set_value('kab_kota'); ?>" class="form-control" placeholder="Kabupaten/Kota Anda" required="required"></input>
									<?php echo form_error('kab_kota', '<p class="text-danger">', '</p>'); ?>
								</div>
							</div>
							<div class="form-group">
								<label for="email" class="col-xs-4 col-sm-4 col-md-4 col-lg-4 control-label">Email</label>
								<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
									<input type="email" name="email" id="email" value="<?php echo set_value('email', $v_email); ?>" class="form-control" placeholder="Email Anda" required="required"></input>
									<?php echo form_error('email', '<p class="text-danger">', '</p>'); ?>
								</div>
							</div>
							<div class="form-group">
								<label for="nomor_telepon" class="col-xs-4 col-sm-4 col-md-4 col-lg-4 control-label">Nomor Telepon</label>
								<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
									<input type="text" name="nomor_telepon" id="nomor_telepon" value="<?php echo set_value('nomor_telepon'); ?>" class="form-control" placeholder="Nomor Telepon Anda" required="required"></input>
									<span class="help-block">Contoh: 081234567890</span>
									<?php echo form_error('nomor_telepon', '<p class="text-danger">', '</p>'); ?>
								</div>
							</div>
							<div class="form-group">
								<label class="col-xs-4 col-sm-4 col-md-4 col-lg-4 control-label">Periode Asuransi</label>
								<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
									<label class="control-label">Mulai</label>
									<input type="text" name="periode_mulai" id="periode_mulai" class="form-control" required="required"></input><br>
									<?php echo form_error('periode_mulai', '<p class="text-danger">', '</p>'); ?>
									<label class="control-label">Sampai</label>
									<input type="text" name="periode_sampai" id="periode_sampai" class="form-control" required="required"></input><br>
									<?php echo form_error('periode_sampai', '<p class="text-danger">', '</p>'); ?>
								</div>
							</div>
							<div class="form-group">
								<label for="nama_stnk" class="col-xs-4 col-sm-4 col-md-4 col-lg-4 control-label">Nama Pemilik (Sesuai STNK)</label>
								<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
									<input type="text" name="nama_stnk" id="nama_stnk" value="<?php echo set_value('nama_stnk'); ?>" class="form-control" placeholder="Nama Pemilik (Sesuai STNK)" required="required"></input>
									<?php echo form_error('nama_stnk', '<p class="text-danger">', '</p>'); ?>
								</div>
							</div>
							<div class="form-group">
								<label for="alamat_stnk" class="col-xs-4 col-sm-4 col-md-4 col-lg-4 control-label">Alamat Pemilik (Sesuai STNK)</label>
								<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
									<textarea name="alamat_stnk" id="alamat_stnk" value="<?php echo set_value('alamat_stnk'); ?>" class="form-control" placeholder="Alamat Pemilik (Sesuai STNK)" required="required"></textarea>
									<?php echo form_error('alamat_stnk', '<p class="text-danger">', '</p>'); ?>
								</div>
							</div>
							<div class="form-group">
								<label for="merek_kendaraan" class="col-xs-4 col-sm-4 col-md-4 col-lg-4 control-label">Merek Kendaraan</label>
								<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
									<input type="text" name="merek_kendaraan" id="merek_kendaraan" value="<?php echo set_value('merek_kendaraan'); ?>" class="form-control" placeholder="Merek Kendaraan" required="required"></input>
									<?php echo form_error('merek_kendaraan', '<p class="text-danger">', '</p>'); ?>
								</div>
							</div>
							<div class="form-group">
								<label for="tipe_kendaraan" class="col-xs-4 col-sm-4 col-md-4 col-lg-4 control-label">Tipe Kendaraan</label>
								<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
									<input type="text" name="tipe_kendaraan" id="tipe_kendaraan" value="<?php echo set_value('tipe_kendaraan'); ?>" class="form-control" placeholder="Tipe Kendaraan" required="required"></input>
									<?php echo form_error('tipe_kendaraan', '<p class="text-danger">', '</p>'); ?>
								</div>
							</div>
							<div class="form-group">
								<label for="tahun_pembuatan" class="col-xs-4 col-sm-4 col-md-4 col-lg-4 control-label">Tahun Pembuatan</label>
								<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
									<input type="text" name="tahun_pembuatan" id="tahun_pembuatan" value="<?php echo set_value('tahun_pembuatan', $v_tahun_pembuatan); ?>" class="form-control" placeholder="Tahun Pembuatan" readonly="readonly"></input>
									<?php echo form_error('tahun_pembuatan', '<p class="text-danger">', '</p>'); ?>
								</div>
							</div>
							<div class="form-group">
								<label for="warna" class="col-xs-4 col-sm-4 col-md-4 col-lg-4 control-label">Warna</label>
								<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
									<input type="text" name="warna" id="warna" value="<?php echo set_value('warna'); ?>" class="form-control" placeholder="Warna Kendaraan" required="required"></input>
									<?php echo form_error('warna', '<p class="text-danger">', '</p>'); ?>
								</div>
							</div>
							<div class="form-group">
								<label for="transmisi" class="col-xs-4 col-sm-4 col-md-4 col-lg-4 control-label">Transmisi</label>
								<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
									<select name="transmisi" class="form-control">
										<option value="1" <?php echo set_select('transmisi', '1'); ?> >Manual</option>
										<option value="2" <?php echo set_select('transmisi', '2'); ?> >Otomatis</option>
									</select>
								</div>
							</div>
							<div class="form-group">
								<label for="nomor_polisi" class="col-xs-4 col-sm-4 col-md-4 col-lg-4 control-label">Nomor Polisi</label>
								<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
									<input type="text" name="nomor_polisi" id="nomor_polisi" value="<?php echo set_value('nomor_polisi'); ?>" class="form-control" placeholder="Nomor Polisi" required="required"></input>
									<?php echo form_error('nomor_polisi', '<p class="text-danger">', '</p>'); ?>
								</div>
							</div>
							<div class="form-group">
								<label for="nomor_rangka" class="col-xs-4 col-sm-4 col-md-4 col-lg-4 control-label">Nomor Rangka</label>
								<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
									<input type="text" name="nomor_rangka" id="nomor_rangka" value="<?php echo set_value('nomor_rangka'); ?>" class="form-control" placeholder="Nomor Rangka" required="required"></input>
									<?php echo form_error('nomor_rangka', '<p class="text-danger">', '</p>'); ?>
								</div>
							</div>
							<div class="form-group">
								<label for="nomor_mesin" class="col-xs-4 col-sm-4 col-md-4 col-lg-4 control-label">Nomor Mesin</label>
								<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
									<input type="text" name="nomor_mesin" id="nomor_mesin" value="<?php echo set_value('nomor_mesin'); ?>" class="form-control" placeholder="Nomor Mesin" required="required"></input>
									<?php echo form_error('nomor_mesin', '<p class="text-danger">', '</p>'); ?>
								</div>
							</div>
							<div class="form-group">
								<label for="jenis_kendaraan" class="col-xs-4 col-sm-4 col-md-4 col-lg-4 control-label">Jenis Kendaraan</label>
								<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
									<input type="text" name="jenis_kendaraan" id="jenis_kendaraan" value="<?php echo set_value('jenis_kendaraan', $v_jenis_kendaraan); ?>" class="form-control" placeholder="Jenis Kendaraan" readonly="readonly"></input>
									<?php echo form_error('jenis_kendaraan', '<p class="text-danger">', '</p>'); ?>
								</div>
							</div>
							<div class="form-group">
								<label for="jenis_pertanggungan" class="col-xs-4 col-sm-4 col-md-4 col-lg-4 control-label">Kondisi Pertanggungan Dasar</label>
								<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
									<input type="text" name="jenis_pertanggungan" id="jenis_pertanggungan" value="<?php echo set_value('jenis_pertanggungan', $v_jenis_pertanggungan); ?>" class="form-control" placeholder="Jenis Pertanggungan" readonly="readonly"></input>
									<?php echo form_error('jenis_pertanggungan', '<p class="text-danger">', '</p>'); ?>
								</div>
							</div>
							<div class="form-group">
								<label for="perluasan_jaminan" class="col-xs-4 col-sm-4 col-md-4 col-lg-4 control-label">Perluasan Jaminan</label>
								<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
									<input type="text" name="perluasan_jaminan" id="perluasan_jaminan" value="<?php echo set_value('perluasan_jaminan', $v_perluasan_jaminan); ?>" class="form-control" placeholder="Perluasan Jaminan" readonly="readonly"></input>
									<?php echo form_error('perluasan_jaminan', '<p class="text-danger">', '</p>'); ?>
								</div>
							</div>
							<div class="form-group">
								<label class="col-xs-4 col-sm-4 col-md-4 col-lg-4 control-label">Harga Pertanggungan</label>
								<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
									<label class="control-label">Kendaraan Bermotor</label>
									<div class="input-group">
										<span class="input-group-addon">Rp</span>
										<input type="text" name="harga_kendaraan" id="harga_kendaraan" value="<?php echo set_value('harga_kendaraan', $v_harga_kendaraan); ?>" class="form-control" placeholder="Harga Kendaraan" readonly="readonly"></input>
										<?php echo form_error('harga_kendaraan', '<p class="text-danger">', '</p>'); ?>
									</div>
									<label class="control-label">Perlengkapan Tambahan Nonstandar</label>
									<div class="input-group">
										<span class="input-group-addon">Rp</span>
										<input type="text" name="perlengkapan_nonstd" id="perlengkapan_nonstd" value="<?php echo set_value('perlengkapan_nonstd', $v_perlengkapan_nonstd); ?>" class="form-control" placeholder="Harga Perlengkapan Tambahan Nonstandar" readonly="readonly"></input>
										<?php echo form_error('perlengkapan_nonstd', '<p class="text-danger">', '</p>'); ?>
									</div>
								</div>
							</div>
							<div class="form-group">
								<label for="penggunaan_kendaraan" class="col-xs-4 col-sm-4 col-md-4 col-lg-4 control-label">Penggunaan Kendaraan</label>
								<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
									<select name="penggunaan_kendaraan" class="form-control">
										<option value="1" <?php echo set_select('penggunaan_kendaraan', '1'); ?> >Pribadi</option>
										<option value="2" <?php echo set_select('penggunaan_kendaraan', '2'); ?> >Komersial/Disewakan/Umum</option>
									</select>
								</div>
							</div>
							<div class="form-group">
								<label for="foto_ktp" class="col-xs-4 col-sm-4 col-md-4 col-lg-4 control-label">Foto KTP</label>
								<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
									<input type="file" name="foto_ktp" id="foto_ktp" accept="image/*;capture=camera" multiple></input>
									


									<span class="help-block">Foto KTP dengan format .jpg dan ukuran maksimal 500kb</span>
									<?php echo form_error('foto_ktp', '<p class="text-danger">', '</p>'); ?>
								</div>
							</div>
							<div class="form-group">
								<label for="foto_stnk" class="col-xs-4 col-sm-4 col-md-4 col-lg-4 control-label">Foto STNK</label>
								<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
									<input type="file" name="foto_stnk" id="foto_stnk" accept="image/*;capture=camera" multiple></input>
									<span class="help-block">Foto STNK dengan format .jpg dan ukuran maksimal 500kb</span>
									<?php echo form_error('foto_stnk', '<p class="text-danger">', '</p>'); ?>
								</div>
							</div>
							<div class="form-group">
								<label for="foto_kendaraan" class="col-xs-4 col-sm-4 col-md-4 col-lg-4 control-label">Foto Kendaraan</label>
								<?php echo form_error('foto_kendaraan', '<p class="text-danger">', '</p>'); ?>
								<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
									<label for="Sisi Depan" class="control-label">Sisi Depan</label>
									<div id="tombol-andro-depan"></div>
									<div id="tombol-modal-depan"></div>
									<label for="Sisi Kiri" class="control-label">Sisi Kiri</label>
									<div id="tombol-andro-kiri"></div>
									<div id="tombol-modal-kiri"></div>
									<label for="Sisi Belakang" class="control-label">Sisi Belakang</label>
									<div id="tombol-andro-belakang"></div>
									<div id="tombol-modal-belakang"></div>
									<label for="Sisi Kanan" class="control-label">Sisi Kanan</label>
									<div id="tombol-andro-kanan"></div>
									<div id="tombol-modal-kanan"></div>
								</div>
							</div>
							<div class="form-group">
								<div class="text-center">
									<input type="submit" value="Lanjut" class="btn btn-primary"></input>
									<a href="<?php echo site_url('asuransi-kendaraan'); ?>" class="btn btn-primary" role="button">Kembali ke Halaman Pencarian</a>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>


<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog modal-dialog-centered">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
		  <h4 class="modal-title">Modal Header</h4>
		  <button id="button">Get camera</button>
			<select id="select">
				<option></option>
			</select>
        </div>
        <div class="modal-body">
			<div class="modal-body" id="modal_body">
			
				<canvas height="300px" weight="auto"></canvas>
				<video  id="video" style="width: 70%; height: auto; margin:0 auto; frameborder:0;"autoplay playsinline></video>
			</div>
        </div>
        <div class="modal-footer">
		<button onclick="takeSnapshot()">Ambil Gambar</button>
        </div>
      </div>
      
    </div>
  </div>

<script>
const video = document.getElementById('video');
const button = document.getElementById('button');
const select = document.getElementById('select');
let currentStream;

function stopMediaTracks(stream) {
  stream.getTracks().forEach(track => {
    track.stop();
  });
}

function gotDevices(mediaDevices) {
  select.innerHTML = '';
  select.appendChild(document.createElement('option'));
  let count = 1;
  mediaDevices.forEach(mediaDevice => {
    if (mediaDevice.kind === 'videoinput') {
      const option = document.createElement('option');
      option.value = mediaDevice.deviceId;
      const label = mediaDevice.label || `Camera ${count++}`;
      const textNode = document.createTextNode(label);
      option.appendChild(textNode);
      select.appendChild(option);
    }
  });
}

button.addEventListener('click', event => {
  if (typeof currentStream !== 'undefined') {
    stopMediaTracks(currentStream);
  }
  const videoConstraints = {};
  if (select.value === '') {
    videoConstraints.facingMode = 'environment';
  } else {
    videoConstraints.deviceId = { exact: select.value };
  }
  const constraints = {
    video: videoConstraints,
    audio: false
  };
  navigator.mediaDevices
    .getUserMedia(constraints)
    .then(stream => {
      currentStream = stream;
      video.srcObject = stream;
      return navigator.mediaDevices.enumerateDevices();
    })
    .then(gotDevices)
    .catch(error => {
      console.error(error);
    });
});

navigator.mediaDevices.enumerateDevices().then(gotDevices);  

function takeSnapshot() {
    // buat elemen img
    var img = document.createElement('img');
    var context;
	var modal =document.getElementById('modal_body')

    // ambil ukuran video
    var width = video.offsetWidth
            , height = video.offsetHeight;

    // buat elemen canvas
    canvas = document.createElement('canvas');
    canvas.width = width;
    canvas.height = height;

    // ambil gambar dari video dan masukan 
    // ke dalam canvas
    context = canvas.getContext('2d');
    context.drawImage(video, 0, 0, width, height);

    // render hasil dari canvas ke elemen img
    img.src = canvas.toDataURL('image/png');
    modal.appendChild(img);
}


</script>