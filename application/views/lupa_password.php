<div class="container-fluid" style="background-color:#fff;">
    <div class="container margin_70">
        <div class="row">
            <div class="col-md-12 main_title textle">
                <h2 style="color:#575757;letter-spacing:1px;font-size: 23px;">Forgot Password</h2>
            </div>
            <div class="col-md-12" style="margin-top: -37px;"><hr></div>
        </div>
        <div class="row">
            <div class="col-md-3"></div>
            <div class="col-md-6">
                <div class="box_style_1 pad56">
				<p align="center">Silakan masukkan email login Anda yang terdaftar. Kami akan mengirimkan password baru ke email Anda.</p>
				<?php echo form_open($action, 'role="form"'); ?>
					<div class="form-group">
						<label for="email" class="control-label">Email</label>
						<div class="input-group">
							<span class="input-group-addon glyphicon glyphicon-envelope"></span>
							<input type="email" name="email" id="email" value="<?php echo set_value('email'); ?>" class="form-control" placeholder="Email" required="required"></input>
						</div>
						<?php echo form_error('email','<p class="text-danger">','</p>'); ?>
					</div>
					<div class="form-group text-center">
						<input type="submit" value="Reset Password" class="btn_2 btn agelogbt btn-block"></input>
					</div>
				</form>
				<div class="agentregfor">
					<span>New user?  <a href="../register/index.html"> Register Now</a></span>
				</div>
			</div>
		</div>
	</div>
</div>
</div>