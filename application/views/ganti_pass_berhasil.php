<div class="container-fluid" style="background-color:#fff;">
    <div class="container margin_70">
        <div class="row">
            <div class="col-md-3"></div>
            	<div class="col-md-6">
                	<div class="box_style_1 pad56">
						<h3 class="text-center text-success" style="margin-top: -30px">Ganti Password Berhasil</h3>
						<p align="center" class="text-center text-success">
							Proses mengganti password Anda telah berhasil. Kami telah mengirimkan password baru
							ke email Anda. Silakan cek email Anda tersebut untuk melihat password baru Anda.
						</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>