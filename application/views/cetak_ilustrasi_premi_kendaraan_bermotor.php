<?php
	class ilustrasi extends CI_Controller{
		private $CI;
		
		function __construct(){
			parent::__construct;
			$CI = &get_instance();
			$this->CI->load->library('fpdf');
		}
		
		function cetak(){
			//Lebar A4= 210mm
		//Margin default= 10mm tiap sisi
		//Bidang Horizontal yang dapat ditulisi= 210 - (10*2) = 190mm
		
		$this->CI->fpdf->AddPage();
		
		//SetFont ke Arial, B, 10pt
		$this->CI->fpdf->SetFont('Arial','B', 10);
		
		//Set Image(string file, x, y , lebar, tinggi, string type, mixed link)
		$this->CI->fpdf->Image(base_url().'assets/images/interface/logo-solusipremi-black.png', 10, 10, 40, 0, 'PNG');
		
		$this->CI->fpdf->SetY(20);
		
		//Cell(lebar, tinggi, teks, border, end line, [align])
		$this->CI->fpdf->Cell(0, 5, 'Ilustrasi Perhitungan Premi Asuransi Kendaraan Bermotor', 'TB', 1, 'C');
		
		//Hilangkan Bold
		$this->CI->fpdf->SetFont('');
		//SetFont sekarang adalah Arial 10pt
		
		//define standard font size
		$fontSize = 10;

		//define a temporary font size
		$tempFontSize = $fontSize;
		
		$this->CI->fpdf->Cell(95, 5, 'Harga Kendaraan', 0, 0, 'L');
		$this->CI->fpdf->Cell(3, 5, ':', 0, 0, 'C');
		$this->CI->fpdf->Cell(7, 5, 'Rp', 0, 0, 'C');
		$this->CI->fpdf->Cell(50, 5, $_SESSION['asuransi_kendaraan']['harga_kendaraan_str'], 0, 0, 'R');
		$this->CI->fpdf->Cell(35, 5, '', 0, 1, '');

		$this->CI->fpdf->Cell(95, 5, 'Perlengkapan Tambahan Nonstandar', 0, 0, 'L');
		$this->CI->fpdf->Cell(3, 5, ':', 0, 0, 'C');
		$this->CI->fpdf->Cell(7, 5, 'Rp', 0, 0, 'C');
		$this->CI->fpdf->Cell(50, 5, $_SESSION['asuransi_kendaraan']['perlengkapan_nonstd_str'], 0, 0, 'R');
		$this->CI->fpdf->Cell(35, 5, '', 0, 1, '');

		$this->CI->fpdf->Cell(95, 5, 'Total Harga Pertanggungan', 0, 0, 'L');
		$this->CI->fpdf->Cell(3, 5, ':', 0, 0, 'C');
		$this->CI->fpdf->Cell(7, 5, 'Rp', 0, 0, 'C');
		$this->CI->fpdf->Cell(50, 5, $_SESSION['asuransi_kendaraan']['total_harga_pertanggungan_str'], 0, 0, 'R');
		$this->CI->fpdf->Cell(35, 5, '', 0, 1, '');

		$this->CI->fpdf->Cell(95, 5, 'Tahun Kendaraan', 0, 0, 'L');
		$this->CI->fpdf->Cell(3, 5, ':', 0, 0, 'C');
		$this->CI->fpdf->Cell(92, 5, $_SESSION['asuransi_kendaraan']['tahun_kendaraan'], 0, 1, 'L');

		$this->CI->fpdf->Cell(95, 5, 'Jenis Kendaraan', 0, 0, 'L');
		$this->CI->fpdf->Cell(3, 5, ':', 0, 0, 'C');
		$this->CI->fpdf->Cell(92, 5, $_SESSION['asuransi_kendaraan']['jenis_kendaraan'], 0, 1, 'L');

		$this->CI->fpdf->Cell(95, 5, 'Wilayah', 0, 0, 'L');
		$this->CI->fpdf->Cell(3, 5, ':', 0, 0, 'C');
		$this->CI->fpdf->MultiCell(92, 5, $_SESSION['asuransi_kendaraan']['kode_wilayah'].' ('.$_SESSION['asuransi_kendaraan']['nama_kota'].')', 0, 'L');

		$this->CI->fpdf->Cell(95, 5, 'Basic Coverage', 0, 0, 'L');
		$this->CI->fpdf->Cell(3, 5, ':', 0, 0, 'C');
		$this->CI->fpdf->Cell(92, 5, $_SESSION['asuransi_kendaraan']['jenis_pertanggungan'], 0, 1, 'L');

		$this->CI->fpdf->Cell(95, 5, 'Premi Basic Coverage', 'T', 0, 'L');
		$this->CI->fpdf->Cell(3, 5, ':', 'T', 0, 'C');
		$this->CI->fpdf->Cell(7, 5, 'Rp', 'T', 0, 'C');
		$this->CI->fpdf->Cell(50, 5, $_SESSION['asuransi_kendaraan']['premi_dasar_str'], 'T', 0, 'R');
		$this->CI->fpdf->Cell(35, 5, $_SESSION['asuransi_kendaraan']['persen_premi_str'], 'T', 1, 'R');

		$this->CI->fpdf->Cell(95, 5, 'Loading Premi (Faktor Usia)', 0, 0, 'L');
		$this->CI->fpdf->Cell(3, 5, ':', 0, 0, 'C');
		$this->CI->fpdf->Cell(7, 5, 'Rp', 0, 0, 'C');
		$this->CI->fpdf->Cell(50, 5, $_SESSION['asuransi_kendaraan']['loading_premi_str'], 0, 0, 'R');
		$this->CI->fpdf->Cell(35, 5, $_SESSION['asuransi_kendaraan']['persen_usia_str'], 0, 1, 'R');

		$this->CI->fpdf->Cell(95, 5, 'Premi Banjir termasuk Angin Topan', 0, 0, 'L');
		$this->CI->fpdf->Cell(3, 5, ':', 0, 0, 'C');
		$this->CI->fpdf->Cell(7, 5, 'Rp', 0, 0, 'C');
		$this->CI->fpdf->Cell(50, 5, $_SESSION['asuransi_kendaraan']['premi_banjir_str'], 0, 0, 'R');
		$this->CI->fpdf->Cell(35, 5, $_SESSION['asuransi_kendaraan']['persen_banjir_str'], 0, 1, 'R');

		$this->CI->fpdf->Cell(95, 5, 'Premi Gempa Bumi, Tsunami', 0, 0, 'L');
		$this->CI->fpdf->Cell(3, 5, ':', 0, 0, 'C');
		$this->CI->fpdf->Cell(7, 5, 'Rp', 0, 0, 'C');
		$this->CI->fpdf->Cell(50, 5, $_SESSION['asuransi_kendaraan']['premi_gempa_str'], 0, 0, 'R');
		$this->CI->fpdf->Cell(35, 5, $_SESSION['asuransi_kendaraan']['persen_gempa_str'], 0, 1, 'R');

		$this->CI->fpdf->Cell(95, 5, 'Premi Huru-hara dan Kerusuhan (SRCC)', 0, 0, 'L');
		$this->CI->fpdf->Cell(3, 5, ':', 0, 0, 'C');
		$this->CI->fpdf->Cell(7, 5, 'Rp', 0, 0, 'C');
		$this->CI->fpdf->Cell(50, 5, $_SESSION['asuransi_kendaraan']['premi_huruhara_str'], 0, 0, 'R');
		$this->CI->fpdf->Cell(35, 5, $_SESSION['asuransi_kendaraan']['persen_huruhara_str'], 0, 1, 'R');

		$this->CI->fpdf->Cell(95, 5, 'Premi Terorisme dan Sabotase (TS)', 0, 0, 'L');
		$this->CI->fpdf->Cell(3, 5, ':', 0, 0, 'C');
		$this->CI->fpdf->Cell(7, 5, 'Rp', 0, 0, 'C');
		$this->CI->fpdf->Cell(50, 5, $_SESSION['asuransi_kendaraan']['premi_terorisme_str'], 0, 0, 'R');
		$this->CI->fpdf->Cell(35, 5, $_SESSION['asuransi_kendaraan']['persen_terorisme_str'], 0, 1, 'R');
		
		//multicell method
		$cellWidth = 45; //wrapped cell width
		$cellHeight = 5; //normal one-line cell height
		
		if($_SESSION['asuransi_kendaraan']['premi_pap'] !== 0)
		{
			$ket_pap = '; '.$_SESSION['asuransi_kendaraan']['jml_seat'].' orang';
		}
		else
		{
			$ket_pap = '';
		}
		
		$dataPerluasan = array(
								array('Premi Tanggung Jawab Hukum terhadap Pihak Ketiga (TJH)', $_SESSION['asuransi_kendaraan']['limit_tjh_str'], $_SESSION['asuransi_kendaraan']['premi_tjh_str'], 'Rate Progresif'),
								array('Premi Kecelakaan Diri untuk Pengemudi (PAD)', $_SESSION['asuransi_kendaraan']['limit_pad_str'], $_SESSION['asuransi_kendaraan']['premi_pad_str'], $_SESSION['asuransi_kendaraan']['persen_pad_str']),
								array('Premi Kecelakaan Diri untuk Penumpang (PAP)', $_SESSION['asuransi_kendaraan']['limit_pap_str'], $_SESSION['asuransi_kendaraan']['premi_pap_str'], $_SESSION['asuransi_kendaraan']['persen_pap_str'].$ket_pap)
						);
		
		foreach ($dataPerluasan as $itemPerluasan)
		{
			//check whether the text is overflowing
			if($this->CI->fpdf->GetStringWidth($itemPerluasan[0]) < $cellWidth)
			{
				//if not, then do nothing
				$line = 1;
			}
			else
			{
				//if it is, then calculate the height needed for wrapped cell
				//by splitting the text to fit the cell width
				//then count how many lines are needed for the text to fit the cell
				
				$textLength = strlen($itemPerluasan[0]); //total text length
				//$errMargin = 10; //cell width error margin, just in case
				$errMargin = 0;
				$startChar = 0; //character start position for each line
				$maxChar = 0; //maximum character in a line, to be incremented later
				$textArray = array(); //to hold the strings for each line
				$tmpString = ""; //to hold the string for a line (temporary)
				
				while ($startChar < $textLength)
				{
					//loop until end of text
					//loop until maximum character reached
					while($this->CI->fpdf->GetStringWidth($tmpString) < ($cellWidth - $errMargin) && ($startChar + $maxChar) < $textLength)
					{
						$maxChar++;
						$tmpString = substr($itemPerluasan[0], $startChar, $maxChar);
					}
					//move startChar to next line
					$startChar = $startChar + $maxChar;
					//then add it into the array so we know how many line are needed
					array_push($textArray, $tmpString);
					//reset maxChar and tmpString
					$maxChar = 0;
					$tmpString = '';
				}
				//get number of line
				$line = count($textArray);
			}
			
			//write the cells

			//use MultiCell instead of Cell
			//but first, because MultiCell is always treated as line ending, we need to
			//manually set the xy position for the next cell to be next to it
			//remember the x and y position before writing the multicell
			$xPos = $this->CI->fpdf->GetX();
			$yPos = $this->CI->fpdf->GetY();
			$this->CI->fpdf->MultiCell($cellWidth, $cellHeight, $itemPerluasan[0], 0, 'L');
			
			//return the position for next cell next to the multicell
			//and offset the x with multicell width
			$this->CI->fpdf->SetXY($xPos + $cellWidth, $yPos);

			$this->CI->fpdf->Cell(50, ($line * $cellHeight), $itemPerluasan[1], 0, 0, 'R');
			$this->CI->fpdf->Cell(3, ($line * $cellHeight), ':', 0, 0, 'C');
			$this->CI->fpdf->Cell(7, ($line * $cellHeight), 'Rp', 0, 0, 'C');
			$this->CI->fpdf->Cell(50, ($line * $cellHeight), $itemPerluasan[2], 0, 0, 'R');
			$this->CI->fpdf->Cell(35, ($line * $cellHeight), $itemPerluasan[3], 0, 1, 'R');
		}
		
		$this->CI->fpdf->SetFont('Arial', 'B', 10);

		$this->CI->fpdf->Cell(95, 5, 'Total Premi Setahun', 'T', 0, 'L');
		$this->CI->fpdf->Cell(3, 5, ':', 'T', 0, 'C');
		$this->CI->fpdf->Cell(7, 5, 'Rp', 'T', 0, 'C');
		$this->CI->fpdf->Cell(50, 5, $_SESSION['asuransi_kendaraan']['total_premi_str'], 'T', 0, 'R');
		$this->CI->fpdf->Cell(35, 5, '', 'T', 1, '');

		$this->CI->fpdf->Cell(95, 5, 'Biaya Polis dan Materai', 0, 0, 'L');
		$this->CI->fpdf->Cell(3, 5, ':', 0, 0, 'C');
		$this->CI->fpdf->Cell(7, 5, 'Rp', 0, 0, 'C');
		$this->CI->fpdf->Cell(50, 5, $_SESSION['asuransi_kendaraan']['biaya_polis_materai_str'], 0, 0, 'R');
		$this->CI->fpdf->Cell(35, 5, '', 0, 1, '');

		$this->CI->fpdf->Cell(95, 5, 'Total Bayar Setahun', 'TB', 0, 'L');
		$this->CI->fpdf->Cell(3, 5, ':', 'TB', 0, 'C');
		$this->CI->fpdf->Cell(7, 5, 'Rp', 'TB', 0, 'C');
		$this->CI->fpdf->Cell(50, 5, $_SESSION['asuransi_kendaraan']['total_bayar_setahun_str'], 'TB', 0, 'R');
		$this->CI->fpdf->Cell(35, 5, '', 'TB', 1, '');

		$this->CI->fpdf->Output('Ilustrasi Perhitungan Premi Asuransi Kendaraan Bermotor.pdf', 'I');
		}
	}
		
		
		