<div class="container-fluid" style="background-color:#fff;">
    <div class="container margin_70">
        <div class="row">
            <div class="col-md-3"></div>
            	<div class="col-md-6">
                	<div class="box_style_1 pad56">
						<h3 class="text-center text-success" style="margin-top: -30px">Proses Pendaftaran Berhasil</h3>
						<p class="text-center text-success">
							Proses pendaftaran akun Anda telah berhasil. Selamat! Anda kini telah menjadi member dari solusipremi.com.<br>
							Anda juga akan mendapatkan informasi terbaru seputar tips, diskon, atau promo dari SolusiPremi.com secara <strong><em>Gratis!</em></strong>
						</p>
						<p class="text-center">
							Silakan klik <a href="<?php echo site_url('auth'); ?>" class="btn btn-primary">link ini</a> untuk login ke akun Anda.
						</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>