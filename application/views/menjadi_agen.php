</header><section class="parallax-window" data-parallax="scroll" style="background-image: url('<?php echo base_url(); ?>uploads/gallery/home_bg_2.jpg'); background-repeat: no-repeat; background-size: 100%" data-natural-width="1400" data-natural-height="470">
    <div class="parallax-content-1">
        <<div class="animated fadeInDown">
            <h1>Become A Agency</h1>
            <p>Expand your policy business with Us</p>
    </div>
    </div>
</section>
<div class="clearfix"></div>
<div class="container margin_60 pdb20i">
    <div class="row">
        <div class="col-md-6 col-sm-6">
            <h3>How <span>Policy Plan</span> Works?</h3>
            <ul class="list_order">
                <li><span>1</span><b>Get verified customer requests</b>
                    <p>Get connected with customers looking for your service.</p>
                </li>
                <li><span>2</span><b>Pay to send quotes</b>
                    <p>Pay only to reply to customer leads that match your interests and qualifications.</p>
                </li>
                <li><span>3</span><b>Chat and get hired</b>
                    <p>Chat with the customers, finalise the quotes and get hired</p>
                </li>
            </ul>
        </div>

        <div class="col-sm-6">
            <div class="box_style_1 pad56">
                <div class="row">
					                    <form method="post">
                        <div class="form-group">
							<input type="text" class="form-control" name="name" style="width:455px" value="" placeholder="Ageny Name" data-validation="required" data-validation-error-msg-required="Enter agency name" onblur="return agenc_permalink(this.value);">
							<input type="hidden" name="permalink" id="permaval" value="">
						</div>
						<div class="form-group">
							<input type="email" class="form-control" name="email" value="" placeholder="Email Address" data-validation="required email" data-validation-error-msg-required="Enter your email address" data-validation-error-msg-email="Invalid email address">
						</div>
						<div class="form-group">
							<label class="info">Choose a category</label>
							<select class="form-control" name="category[]" multiple data-validation="required" data-validation-error-msg-required="Choose a category you deal with">
							<option value='1' >Health</option><option value='2' >Term Life</option><option value='3' >Pension</option><option value='4' >Car</option><option value='5' >Travel</option><option value='6' >Bike</option><option value='7' >Child</option><option value='8' >Tax Saving</option><option value='9' >Investment</option><option value='10' >Home</option><option value='11' >Pet</option><option value='12' >Flood</option><option value='13' >Agricultural</option><option value='14' >Shipping</option><option value='15' >Labor</option><option value='16' >Satelite</option><option value='17' >Group</option><option value='18' >Earthquake</option>							</select>
						</div>
						<div class="form-group">
							<input type="password" class="form-control" name="pass_confirmation" placeholder="Password" data-validation="required" data-validation-error-msg-required="Enter your password">
						</div>
						<div class="form-group">
							<input type="password" class="form-control" name="pass" placeholder="Confirm Password" data-validation="required confirmation" data-validation-error-msg-required="Reenter your password" data-validation-error-msg-confirmation="Passwords do not match">
						</div>
                        <div class="form-group">
							<button class="btn_2 btn agelogbt btn-block " type="submit" name="regSubmit">Register</button>
						</div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <hr>

    <div class="main_title pdt20i">
        <h2>Why become an <span> Partner </span> with us?</h2>
        <p>Join a community of 65,000+ Agency who have successfully grown their business with Us.</p>
    </div>

    <div class="row">
        <div class="col-md-4 wow zoomIn animated" data-wow-delay="0.2s" style="visibility: visible; animation-delay: 0.2s; animation-name: zoomIn;">
            <div class="feature_home">
                <i class="icon-chart-line"></i>
                <h3><span>+Grow</span> your business</h3>
                <p>
                    Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula.
                </p>

            </div>
        </div>
        <div class="col-md-4 wow zoomIn animated" data-wow-delay="0.4s" style="visibility: visible; animation-delay: 0.4s; animation-name: zoomIn;">
            <div class="feature_home">
                <i class="icon-user-7"></i>
                <h3><span>Work on</span> your own terms</h3>
                <p>
                    Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula.
                </p>

            </div>
        </div>
        <div class="col-md-4 wow zoomIn animated" data-wow-delay="0.6s" style="visibility: visible; animation-delay: 0.6s; animation-name: zoomIn;">
            <div class="feature_home">
                <i class=" icon-edit-3"></i>
                <h3><span>Business </span> Tools</h3>
                <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula.</p>
            </div>
        </div>
    </div>
</div>
<hr>

<div class="container margin_60 pdt20i">
    <div class="main_title">
        <h2>What our <span>Agencies </span>says</h2>
    </div>
    <div class="row">
		        <div class="col-md-6">
            <div class="review_strip">
                <img src="../uploads/testimonial/testim_5a93af57163d5.jpg" alt="Ben Hanna" class="img-circle" height="80" width="80">
                <h4>Ben Hanna</h4>
                <p>"Ancillae interpretaris ea has. Id amet impedit qui, eripuit mnesarchum percipitur in eam. Ne sit habeo inimicus, no his liber regione volutpat. Ex quot voluptaria usu, dolor vivendo docendi nec ea. Et atqui vocent integre vim, quod cibo recusabo ei usu, s"</p>
            </div>
        </div>
		        <div class="col-md-6">
            <div class="review_strip">
                <img src="../uploads/testimonial/testim_5a93b048e15f5.jpg" alt="Ben Hanna" class="img-circle" height="80" width="80">
                <h4>Ben Hanna</h4>
                <p>"Ancillae interpretaris ea has. Id amet impedit qui, eripuit mnesarchum percipitur in eam. Ne sit habeo inimicus, no his liber regione volutpat. Ex quot voluptaria usu, dolor vivendo docendi nec ea. Et atqui vocent integre vim, quod cibo recusabo ei usu, s"</p>
            </div>
        </div>
		    </div>
</div>