<div class="container message">
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<div class="message-box">
				<h3 class="text-center text-success">Pendaftaran <em>Newsletter</em> Berhasil</h3>
				<p class="text-center text-success">
					Pendaftaran <em>newsletter</em> berhasil dan Anda sudah resmi tergabung
					dalam <em>newsletter</em> SolusiPremi.com. Anda akan mendapatkan info terbaru seputar
					tips, diskon, atau promo dari SolusiPremi.com secara <strong><em>Gratis!</em></strong>
				</p>
			</div>
		</div>
	</div>
</div>