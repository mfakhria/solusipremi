<style>
.navbar-default{
 border: none;
 background-color: #1a243f;
}
.navbar-default .navbar-nav > li > a{
 color: white;
}
.navbar-default .navbar-nav > li > a:hover{
 color: white;
}
.navbar-default .navbar-nav > .open > a, .navbar-default .navbar-nav > .open > a:hover, .navbar-default .navbar-nav > .open > a:focus{
 color: white;
 background-color: #18bc9c;
}
.login{
 background-color: transparent;
 color: white;
}
.login:hover{
 color: white;
 background-color: #212437;
}
.navbar-default .navbar-toggle:hover, .navbar-default .navbar-toggle:focus{
 background-color: transparent;
} 

</style>
<!-- navbar -->
	<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
	<div class="container-fluid">
		<!-- Brand and toggle get grouped for better mobile display -->
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<table>
				<tr>
					<td>
						<img src="assets/img/logo.png" class="img-responsive" width="80" height="80">
					</td>
					<td>
						<a style="color:white;" class="navbar-brand" href="#">Ujian Online</a>
					</td>
				</tr>
			</table>
		</div>

		<!-- Collect the nav links, forms, and other content for toggling -->
		<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
			<ul class="nav navbar-nav">
				<li class=""><a href="#">Link</a></li>
				<li><a href="#">Link</a></li>
				<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">Dropdown <span class="caret"></span></a>
					<ul class="dropdown-menu" role="menu">
						<li><a href="#">Action</a></li>
						<li><a href="#">Another action</a></li>
						<li><a href="#">Something else here</a></li>
						<li class="divider"></li>
						<li><a href="#">Separated link</a></li>
						<li class="divider"></li>
						<li><a href="#">One more separated link</a></li>
					</ul>
				</li>
			</ul>
			<ul class="nav navbar-nav navbar-right">
				<li><a href="#"><button class="btn btn-default login">  <span class="glyphicon glyphicon-lock"></span> Login </button></a></li>
				<li><a href="#"><button class="btn btn-success">  <span class="glyphicon glyphicon-user"></span> Daftar </button></a></li>
			</ul>
		</div><!-- /.navbar-collapse -->
	</div><!-- /.container-fluid -->
</nav>