<script>
	$(function() {
		$("#periode_mulai, #periode_sampai").datepicker({
			dateFormat: "dd-mm-yy"
		});
	});
</script>
<div class="container-fluid">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<p align="center">
					Anda akan membeli Asuransi Properti dari partner kami, <strong class="text-info"><?php echo $v_nama_partner; ?></strong>.<br>
					Silakan isi <strong>SURAT PERMOHONAN ASURANSI KEBAKARAN (SPAK)</strong>
					di bawah ini untuk memproses pembelian Asuransi Properti Anda.
				</p>
				<div class="row">
					<div class="col-sm-offset-2 col-md-offset-2 col-lg-offset-2 col-xs-12 col-sm-8 col-md-8 col-lg-8">
						<h4 class="text-center">SURAT PERMOHONAN ASURANSI KEBAKARAN (SPAK)</h4>
						<hr>
						<?php echo form_open_multipart('asuransi-properti/isi-data/'.$v_id_partner.'', 'class="form-horizontal" role="form"'); ?>
							<div class="form-group">
								<label for="nomor_ktp" class="col-xs-4 col-sm-4 col-md-4 col-lg-4 control-label">Nomor KTP</label>
								<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
									<input type="text" name="nomor_ktp" id="nomor_ktp" value="<?php echo set_value('nomor_ktp'); ?>" class="form-control" placeholder="Nomor KTP" required="required"></input>
									<?php echo form_error('nomor_ktp','<p class="text-danger">','</p>'); ?>
								</div>
							</div>
							<div class="form-group">
								<label for="nama_pemohon" class="col-xs-4 col-sm-4 col-md-4 col-lg-4 control-label">Nama Pemohon</label>
								<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
									<input type="text" name="nama_pemohon" id="nama_pemohon" value="<?php echo set_value('nama_pemohon', $v_nama_pemohon); ?>" class="form-control" placeholder="Nama Anda" required="required"></input>
									<?php echo form_error('nama_pemohon','<p class="text-danger">','</p>'); ?>
								</div>
							</div>
							<div class="form-group">
								<label for="jenis_kelamin" class="col-xs-4 col-sm-4 col-md-4 col-lg-4 control-label">Jenis Kelamin</label>
								<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
									<select name="jenis_kelamin" id="jenis_kelamin" class="form-control">
										<option value="1" <?php echo set_select('jenis_kelamin', '1'); ?> >Laki-Laki</option>
										<option value="2" <?php echo set_select('jenis_kelamin', '2'); ?> >Perempuan</option>
									</select>
								</div>
							</div>
							<div class="form-group">
								<label for="alamat" class="col-xs-4 col-sm-4 col-md-4 col-lg-4 control-label">Alamat</label>
								<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
									<textarea name="alamat" id="alamat" value="<?php echo set_value('alamat'); ?>" class="form-control" placeholder="Alamat Anda" required="required"></textarea>
									<?php echo form_error('alamat','<p class="text-danger">','</p>'); ?>
								</div>
							</div>
							<div class="form-group">
								<label for="rt" class="col-xs-4 col-sm-4 col-md-4 col-lg-4 control-label">RT</label>
								<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
									<input type="text" name="rt" id="rt" value="<?php echo set_value('rt'); ?>" class="form-control" placeholder="RT" required="required"></input>
									<?php echo form_error('rt','<p class="text-danger">','</p>'); ?>
								</div>
							</div>
							<div class="form-group">
								<label for="rw" class="col-xs-4 col-sm-4 col-md-4 col-lg-4 control-label">RW</label>
								<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
									<input type="text" name="rw" id="rw" value="<?php echo set_value('rw'); ?>" class="form-control" placeholder="RW" required="required"></input>
									<?php echo form_error('rw','<p class="text-danger">','</p>'); ?>
								</div>
							</div>
							<div class="form-group">
								<label for="desa_kelurahan" class="col-xs-4 col-sm-4 col-md-4 col-lg-4 control-label">Desa atau Kelurahan</label>
								<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
									<input type="text" name="desa_kelurahan" id="desa_kelurahan" value="<?php echo set_value('desa_kelurahan'); ?>" class="form-control" placeholder="Desa atau Kelurahan Anda" required="required"></input>
									<?php echo form_error('desa_kelurahan','<p class="text-danger">','</p>'); ?>
								</div>
							</div>
							<div class="form-group">
								<label for="kecamatan" class="col-xs-4 col-sm-4 col-md-4 col-lg-4 control-label">Kecamatan</label>
								<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
									<input type="text" name="kecamatan" id="kecamatan" value="<?php echo set_value('kecamatan'); ?>" class="form-control" placeholder="Kecamatan Anda" required="required"></input>
									<?php echo form_error('kecamatan','<p class="text-danger">','</p>'); ?>
								</div>
							</div>
							<div class="form-group">
								<label for="email" class="col-xs-4 col-sm-4 col-md-4 col-lg-4 control-label">Email</label>
								<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
									<input type="email" name="email" id="email" value="<?php echo set_value('email', $v_email); ?>" class="form-control" placeholder="Email Anda" required="required"></input>
									<?php echo form_error('email','<p class="text-danger">','</p>'); ?>
								</div>
							</div>
							<div class="form-group">
								<label for="nomor_telepon" class="col-xs-4 col-sm-4 col-md-4 col-lg-4 control-label">Nomor Telepon</label>
								<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
									<input type="text" name="nomor_telepon" id="nomor_telepon" value="<?php echo set_value('nomor_telepon'); ?>" class="form-control" placeholder="Nomor Telepon Anda" required="required"></input>
									<span class="help-block">Contoh: 081234567890</span>
									<?php echo form_error('nomor_telepon','<p class="text-danger">','</p>'); ?>
								</div>
							</div>
							<div class="form-group">
								<label for="penggunaan_bangunan" class="col-xs-4 col-sm-4 col-md-4 col-lg-4 control-label">Bangunan dipergunakan sebagai</label>
								<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
									<input type="text" name="penggunaan_bangunan" id="penggunaan_bangunan" value="<?php echo set_value('penggunaan_bangunan'); ?>" class="form-control" placeholder="Penggunaan Bangunan" readonly="readonly"></input>
									<?php echo form_error('penggunaan_bangunan','<p class="text-danger">','</p>'); ?>
								</div>
							</div>
							<div class="form-group">
								<label for="jenis_penerangan" class="col-xs-4 col-sm-4 col-md-4 col-lg-4 control-label">Jenis penerangan yang dipergunakan</label>
								<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
									<select name="jenis_penerangan" class="form-control">
										<option value="1" <?php echo set_select('jenis_penerangan', '1'); ?> >Listrik</option>
										<option value="2" <?php echo set_select('jenis_penerangan', '2'); ?> >Menggunakan bensin atau minyak tanah atau solar</option>
									</select>
								</div>
							</div>
							<div class="form-group">
								<label class="col-xs-4 col-sm-4 col-md-4 col-lg-4 control-label">Jangka Waktu yang Diminta</label>
								<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
									<label class="control-label">Mulai</label><input type="text" name="periode_mulai" id="periode_mulai" class="form-control" required="required"></input><br>
									<?php echo form_error('periode_mulai','<p class="text-danger">','</p>'); ?>
									<label class="control-label">Sampai</label><input type="text" name="periode_sampai" id="periode_sampai" class="form-control" required="required"></input><br>
									<?php echo form_error('periode_sampai','<p class="text-danger">','</p>'); ?>
								</div>
							</div>
							<div class="form-group">
								<label for="perluasan_jaminan" class="col-xs-4 col-sm-4 col-md-4 col-lg-4 control-label">Perluasan Jaminan</label>
								<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
									<input type="text" name="perluasan_jaminan" id="perluasan_jaminan" value="<?php echo set_value('perluasan_jaminan', $v_perluasan_jaminan); ?>" class="form-control" placeholder="Perluasan Jaminan" readonly="readonly"></input>
									<?php echo form_error('perluasan_jaminan','<p class="text-danger">','</p>'); ?>
								</div>
							</div>
							<div class="form-group">
								<label for="harga_kendaraan" class="col-xs-4 col-sm-4 col-md-4 col-lg-4 control-label">Kendaraan Bermotor</label>
								<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8 input-group">
									<span class="input-group-addon">Rp</span>
									<input type="text" name="harga_kendaraan" id="harga_kendaraan" value="<?php echo set_value('harga_kendaraan', $v_harga_kendaraan); ?>" class="form-control" placeholder="Harga Kendaraan" readonly="readonly"></input>
									<?php echo form_error('harga_kendaraan','<p class="text-danger">','</p>'); ?>
								</div>
							</div>
							<div class="form-group">
								<label for="perlengkapan_nonstd" class="col-xs-4 col-sm-4 col-md-4 col-lg-4 control-label">Perlengkapan Tambahan Nonstandar</label>
								<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8 input-group">
									<span class="input-group-addon">Rp</span>
									<input type="text" name="perlengkapan_nonstd" id="perlengkapan_nonstd" value="<?php echo set_value('perlengkapan_nonstd', $v_perlengkapan_nonstd); ?>" class="form-control" placeholder="Harga Perlengkapan Tambahan Nonstandar" readonly="readonly"></input>
									<?php echo form_error('perlengkapan_nonstd','<p class="text-danger">','</p>'); ?>
								</div>
							</div>
							<div class="form-group">
								<label for="foto_ktp" class="col-xs-4 col-sm-4 col-md-4 col-lg-4 control-label">Foto KTP</label>
								<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
									<input type="file" name="foto_ktp" id="foto_ktp" accept="image/*;capture=camera" multiple="2"></input>
									<span class="help-block">Foto KTP dengan format .jpg dan ukuran maksimal 500kb</span>
									<?php echo form_error('foto_ktp','<p class="text-danger">','</p>'); ?>
								</div>
							</div>
							<div class="form-group">
								<label for="foto_properti" class="col-xs-4 col-sm-4 col-md-4 col-lg-4 control-label">Foto Properti</label>
								<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
									<input type="file" name="foto_properti" id="foto_properti" accept="image/*;capture=camera" multiple="20"></input>
									<span class="help-block">Foto Properti dengan format .jpg dan ukuran maksimal 500kb</span>
									<?php echo form_error('foto_properti','<p class="text-danger">','</p>'); ?>
								</div>
							</div>
							<div class="form-group">
								<div class="text-center">
									<input type="submit" value="Lanjut" class="btn btn-primary"></input>
									<a href="<?php echo site_url('asuransi-properti'); ?>" class="btn btn-primary" role="button">Kembali ke Halaman Pencarian</a>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script>

</script>