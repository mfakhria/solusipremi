<div class="container-fluid premium-illustration-content">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-offset-1 col-lg-offset-1 col-md-10 col-lg-10 premium-table-section">
				<div class="table-responsive">
					<table class="table table-condensed"><br><br><br>
						<caption class="text-center">Ilustrasi Perhitungan Premi Asuransi Properti</caption>
						<thead>
							<tr>
								<th colspan="5">Ilustrasi Perhitungan Premi Asuransi Properti</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td colspan="2">Penggunaan Bangunan</td>
								<td>:</td>
								<td colspan="2" align="left"><?php echo $v_penggunaan_bangunan.' (kode: '.$v_kode_okupasi.')'; ?></td>
							</tr>
							<tr>
								<td colspan="2">Kelas Konstruksi</td>
								<td>:</td>
								<td colspan="2" align="left"><?php echo $v_jenis_konstruksi; ?></td>
							</tr>
							<tr>
								<td colspan="2">Jumlah Lantai</td>
								<td>:</td>
								<td colspan="2" align="left"><?php echo $v_jumlah_lantai; ?></td>
							</tr>
							<tr>
								<td colspan="2">Provinsi</td>
								<td>:</td>
								<td colspan="2" align="left"><?php echo $v_provinsi; ?></td>
							</tr>
							<tr>
								<td colspan="2">Kota atau Kabupaten</td>
								<td>:</td>
								<td colspan="2" align="left"><?php echo $v_kab_kota; ?></td>
							</tr>
							<tr>
								<td colspan="2">Nilai Bangunan</td>
								<td>:</td>
								<td>Rp</td>
								<td align="right"><?php echo $v_nilai_bangunan; ?></td>
							</tr>
							<tr>
								<td colspan="2">Nilai Perabotan</td>
								<td>:</td>
								<td>Rp</td>
								<td align="right"><?php echo $v_nilai_perabotan; ?></td>
							</tr>
							<tr>
								<td colspan="2">Nilai Mesin</td>
								<td>:</td>
								<td>Rp</td>
								<td align="right"><?php echo $v_nilai_mesin; ?></td>
							</tr>
							<tr>
								<td colspan="2">Nilai Stok</td>
								<td>:</td>
								<td>Rp</td>
								<td align="right"><?php echo $v_nilai_stok; ?></td>
							</tr>
							<tr>
								<td colspan="2"><b>Total Nilai Pertanggungan</b></td>
								<td><b>:</b></td>
								<td><b>Rp</b></td>
								<td align="right"><b><?php echo $v_total_pertanggungan; ?></b></td>
							</tr>
							<tr>
								<td colspan="2"><b>Rate PAR</b></td>
								<td><b>:</b></td>
								<td colspan="2" align="right"><b><?php echo $v_rate_par; ?></b></td>
							</tr>
							<tr>
								<td></td>
								<td><b>FLEXAS</b></td>
								<td><b>:</b></td>
								<td colspan="2" align="right"><b><?php echo $v_permil_flexas; ?></b></td>
							</tr>
							<tr>
								<td></td>
								<td><b>TSFWD</b></td>
								<td><b>:</b></td>
								<td colspan="2" align="right"><b><?php echo $v_permil_banjir; ?></b></td>
							</tr>
							<tr>
								<td></td>
								<td><b>RSMD CC + OTHERS</b></td>
								<td><b>:</b></td>
								<td colspan="2" align="right"><b><?php echo $v_permil_rsmd_cc; ?></b></td>
							</tr>
							<tr>
								<td colspan="2"><b>Rate EARTHQUAKE (ZONA <?php echo $v_ket_zona_gempa; ?>)</b></td>
								<td><b>:</b></td>
								<td colspan="2" align="right"><b><?php echo $v_permil_gempa; ?> (jumlah lantai <?php echo $v_ket_jml_lantai; ?>)</b></td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="table-responsive">
					<table class="table table-condensed">
						<thead>
							<tr>
								<th colspan="4">PREMIUM CALCULATION FOR PAR</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>Rp<?php echo $v_total_pertanggungan;?> x <?php echo $v_rate_par; ?></td>
								<td>:</td>
								<td>Rp</td>
								<td align="right"><?php echo $v_premi_par; ?></td>
							</tr>
							<tr>
								<td>Administration & Policy Cost</td>
								<td>:</td>
								<td>Rp</td>
								<td align="right"><?php echo $v_biaya_polis_materai_par; ?></td>
							</tr>
							<tr>
								<td>TOTAL</td>
								<td>:</td>
								<td>Rp</td>
								<td align="right"><?php echo $v_total_premi_par; ?></td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="table-responsive">
					<table class="table table-condensed">
						<thead>
							<tr>
								<th colspan="4">PREMIUM CALCULATION FOR EQ</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>Rp<?php echo $v_total_pertanggungan;?> x <?php echo $v_permil_gempa; ?></td>
								<td>:</td>
								<td>Rp</td>
								<td align="right"><?php echo $v_premi_gempa; ?></td>
							</tr>
							<tr>
								<td>Administration & Policy Cost</td>
								<td>:</td>
								<td>Rp</td>
								<td align="right"><?php echo $v_biaya_polis_materai_gempa; ?></td>
							</tr>
							<tr>
								<td>TOTAL</td>
								<td>:</td>
								<td>Rp</td>
								<td align="right"><?php echo $v_total_premi_gempa ; ?></td>
							</tr>
						</tbody>
					</table>
				</div>
				<center>
					<a href="<?php echo site_url('asuransi-properti/cetak-ilustrasi'); ?>" class="btn btn-primary" role="button"><span class="glyphicon glyphicon-print"></span> Cetak Ilustrasi</a>
				</center>
			</div>
		</div>
	</div>
</div>
<div class="container-fluid partner-option-content">
	<div class="container">
		<?php $jumlah_partner = $list_partner; //$list_partner berasal dari data yang dilempar dari controller, yaitu $data['list_partner'], berisi result_array() ?>
		<?php if($jumlah_partner > 0) //Apabila data partner yang ada di dalam database lebih dari 0 maka baru ditampilkan ?>
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-offset-1 col-lg-offset-1 col-md-10 col-lg-10 partner-option-section">
				<div class="table-responsive">
					<table class="table table-bordered table-condensed">
						<caption class="text-center">Tabel Pilihan Asuransi</caption>
						<thead>
							<tr>
								<th><center>Asuransi</center></th>
								<th colspan="2"><center>Total Premi Setahun</center></th>
								<th colspan="2"><center>Biaya Tambahan</center></th>
								<th></th>
							</tr>
						</thead>
						<tbody>
							<?php
								foreach($list_partner as $row)
								{
							?>
							<tr>
								<td rowspan="2">
									<center>
									<img src="<?php echo base_url('assets/images/partner/'.$row['nama_gambar_kecil'].''); ?>" class="img-responsive" alt="<?php echo $row['nama_partner']; ?>">
									</center>
									</td>
								<td>Premi PAR</td>
								<td align="right">Rp<?php echo $v_premi_par; ?></td>
								<td>Biaya Polis dan Materai PAR</td>
								<td align="right">Rp<?php echo $v_biaya_polis_materai_par; ?></td>
								<td rowspan="2">
									<center>
									<!-- Button trigger modal -->
									<button class="view_data btn btn-info" id="<?php echo $row['id'];?>" data-toggle="modal"
										data-target="#myModal">Fitur
									</button>
									<a href="<?php echo site_url('asuransi-properti/isi-data/'.$row['id'].''); ?>" class="btn btn-primary" role="button">Beli</a>
									</center>
									</td>
							</tr>
							<tr>
								<td>Premi EQ</td>
								<td align="right">Rp<?php echo $v_premi_gempa; ?></td>
								<td>Biaya Polis dan Materai EQ</td>
								<td align="right">Rp<?php echo $v_biaya_polis_materai_gempa; ?></td>
							</tr>
							<?php
								}
							?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog"
	aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close"
					data-dismiss="modal" aria-hidden="true">
					&times;
				</button>
				<h4 class="modal-title" id="myModalLabel">
					<br>Fitur
				</h4>
			</div>
			<div class="modal-body" id="detail_produk">
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default"
					data-dismiss="modal">Close
				</button>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script>
	$(document).ready(function(){
		$('.view_data').click(function(){
			var id = $(this).attr("id");
			
			$.ajax({
				url: '<?php echo site_url(); ?>/asuransi-properti/view-detail',
				method: 'post',
				data: {id:id},
				success:function(data){
					$('#detail_produk').html(data);
					$('#myModal').modal("show");
				}
			});
		});
	});
</script>