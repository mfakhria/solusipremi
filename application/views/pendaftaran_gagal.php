<div class="container-fluid" style="background-color:#fff;">
    <div class="container margin_70">
        <div class="row">
            <div class="col-md-3"></div>
            	<div class="col-md-6">
                	<div class="box_style_1 pad56">
						<h3 class="text-center text-danger" style="margin-top: -30px">Proses Pendaftaran Gagal</h3>
						<p class="text-center text-danger">
							Mohon maaf, proses pendaftaran Anda gagal. Telah terjadi kesalahan dalam memasukkan data ke database kami.
						</p>
						<p class="text-center">
							Silakan klik <a href="<?php echo site_url('daftar'); ?>">link ini</a> untuk
							mengulangi kembali proses pendaftaran Anda.
						</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>