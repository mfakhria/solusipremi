<div class="container-fluid our-product">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<h3 class="text-center">Produk Kami</h3>
				<p align="justify" class="indent-paragraph">
					SolusiPremi.com memiliki bermacam-macam produk asuransi dari beberapa partner perusahaan asuransi kami.
					Produk asuransi yang kami miliki diantaranya adalah asuransi kendaraan, asuransi properti, asuransi pengiriman,
					asuransi penjaminan, asuransi perjalanan, asuransi kesehatan, dan asuransi jiwa. Dalam menganjurkan produk asuransi, 
					kami mengkaji seluruh informasi yang Anda berikan kepada kami untuk memberikan anjuran produk asuransi sesuai kebutuhan Anda.
				</p>
				<p align="justify" class="indent-paragraph">
					Informasi dan rekomendasi produk asuransi dari kami akan kami berikan secara objektif sesuai informasi dari Anda.
					Semua produk asuransi dari partner perusahaan asuransi kami selalu kami analisa untuk memberikan kemudahan dalam
					rekomendasi kepada Anda. Hal-hal yang perlu kami sampaikan kepada Anda mengenai produk kami yaitu:</p>
				<ul>
					<li>
						Anda dapat melakukan konsultasi produk asuransi kepada kami secara gratis, baik via <em>chat box</em> maupun email.
						Anda hanya perlu memasukkan data diri dan produk asuransi mana yang Anda butuhkan. Kemudian, kami akan memberikan
						uraian lengkap dari pilihan produk asuransi yang dapat Anda pilih sesuai kebutuhan Anda.
					</li>
					<li>
						Anda dapat membeli produk asuransi dari partner perusahaan asuransi kami yang memang dijual melalui kami, via SolusiPremi.com. 
					</li>
					<li>
						Kami akan selalu memperbarui informasi produk asuransi kami sesuai dengan informasi produk milik partner perusahaan asuransi kami.
						Selalu cek situs kami untuk memastikan Anda mendapatkan informasi produk asuransi terbaru.
					</li>
				</ul>
			</div>
		</div>
	</div>
</div>