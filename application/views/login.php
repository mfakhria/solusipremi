<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/id_ID/sdk.js#xfbml=1&version=v3.2&appId=949382838586160&autoLogAppEvents=1';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<div class="container-fluid" style="background-color:#fff;">
    <div class="container margin_70">
        <div class="row">
            <div class="col-md-12 main_title textle"><br>
                <h2 style="color:#575757;letter-spacing:1px;font-size: 23px;">Login</h2>
            </div>
            <div class="col-md-12" style="margin-top: -37px;">
                <hr>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3"></div>
            <div class="col-md-6">
                <div class="box_style_1 pad56">
                    <div class="row">
						  <?php echo form_open($action, 'role="form"'); ?>
							<div class="form-group">
								<label for="email" class="control-label">Email</label>
								<input type="email" name="email" id="email" value="<?php echo set_value('email'); ?>" class="form-control" placeholder="Email" required="required"></input>
							</div>
							<div class="form-group">
								<label for="password" class="control-label">Password</label>
								<input type="password" name="password" id="password" value="<?php echo set_value('password'); ?>" class="form-control" data-toggle="password" data-placement="before" placeholder="Password" required="required"></input>
							</div>
							<p class="text-danger"><?php echo $error; ?></p>
							<div class="form-group text-center">
								<input type="submit" value="Login" class="btn_2 btn agelogbt btn-block"></input>
							</div>
                            <div class="form-group">
                                <center><div class="fb-login-button" data-max-rows="1" data-size="large" data-button-type="continue_with" data-show-faces="false" data-auto-logout-link="false" data-use-continue-as="false"></div></center>
                                <a href="../gplogin/index.html" class="mt15i btn btn-block gplus-btn"><i class="fa fa-google-plus" aria-hidden="true"></i> Login With Google Plus</a>
                            </div>
                            <div class="agentregfor">
                                <span>New user?  <a href="<?php echo site_url('daftar'); ?>">Daftar</a></span>
                                <a href="<?php echo site_url('lupa-password'); ?>" class="pull-right">Forgot Password?</a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="<?php echo base_url('assets/bootstrap-show-password/bootstrap-show-password.js'); ?>"></script>
<script>
  window.fbAsyncInit = function() {
    FB.init({
      appId      : '949382838586160',
      cookie     : true,
      xfbml      : true,
      version    : 'v3.2'
    });


  FB.getLoginStatus(function(response) {
        if (response.status === 'connected') {
            //display user data
            getFbUserData();
        }
    });


  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "https://connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));

  function fbLogin() {
    FB.login(function (response) {
        if (response.authResponse) {
            // Get and display the user profile data
            getFbUserData();
        } else {
            document.getElementById('status').innerHTML = 'User cancelled login or did not fully authorize.';
        }
    }, {scope: 'email'});

}

function getFbUserData(){
    FB.api('/me', {locale: 'en_US', fields: 'id,first_name,last_name,email,link,gender,locale,picture'},
    function (response) {
        document.getElementById('fbLink').setAttribute("onclick","fbLogout()");
        document.getElementById('fbLink').innerHTML = 'Logout from Facebook';
        document.getElementById('status').innerHTML = 'Thanks for logging in, ' + response.first_name + '!';
        document.getElementById('userData').innerHTML = '<p><b>FB ID:</b> '+response.id+'</p><p><b>Name:</b> '+response.first_name+' '+response.last_name+'</p><p><b>Email:</b> '+response.email+'</p><p><b>Gender:</b> '+response.gender+'</p><p><b>Locale:</b> '+response.locale+'</p><p><b>Picture:</b> <img src="'+response.picture.data.url+'"/></p><p><b>FB Profile:</b> <a target="_blank" href="'+response.link+'">click to view profile</a></p>';
    });
}

// Logout from facebook
function fbLogout() {
    FB.logout(function() {
        document.getElementById('fbLink').setAttribute("onclick","fbLogin()");
        document.getElementById('fbLink').innerHTML = '<img src="fblogin.png"/>';
        document.getElementById('userData').innerHTML = '';
        document.getElementById('status').innerHTML = 'You have successfully logout from Facebook.';
    });
}

</script>
