<?php

class Fungsi{
	
	private $pass;
	
	//Fungsi untuk membuat password secara acak
	//Digunakan untuk mengirim password pada form lupa password
	function pass_acak()
	{
		$panjang = 8;
		$kar = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz";
		
		//Acak karakter
		srand((double)microtime() * 1000000);
	
		//Lakukan looping sebanyak $panjang(parameter)
		for($i=0; $i<$panjang; $i++) //Default diulang sebanyak 8x
		{
			$nom_acak = rand()%53; //Untuk mendapatkan satu nomor acak, pada substr()
			$this->pass.= substr($kar,$nom_acak,1); //Ambil satu karakter
		}
		return $this->pass; //Kembalikan hasil
	}
	
	//Fungsi untuk mengenkripsi string dengan metode md5 dan membalik urutannya
	function balik_md5($string)
	{
		//Untuk membalik urutan string digunakan fungsi strrev()
		$chiper_text = strrev(md5($string));
		return $chiper_text;
	}

	//Fungsi untuk mengkonversi hasil input nominal berupa string ke angka
	function convert_to_number($string_angka)
	{
		return intval(preg_replace('/[^0-9]/', '', $string_angka));
		/*
		$angka = str_replace('.', '', $string_angka); //Menghilangkan titik pada $string_angka
		$angka = intval($angka); //Untuk mengambil nilai integernya saja, kalau ada user melakukan input angka dengan 0 sebagai awalan
		return $angka;
		*/
	}
	
	//Fungsi untuk mengkonversi desimal dalam PHP ke string persen
	function konversi_str_persen($desimal)
	{
		$pembilang_persen = $desimal * 100; //Mengkalikan desimal dengan 100
		
		//Dalam perhitungan, tanda titik adalah separator untuk desimal
		//Cek apakah ada tanda titik setelah perkalian tadi
		if(strstr($pembilang_persen, '.'))
		{
			//Jika ada tanda titik, ganti tanda titik dengan tanda koma
			$pembilang_persen = str_replace('.', ',', $pembilang_persen);
		}
		
		$str_persen = $pembilang_persen.'%';
		return $str_persen;
	}
	
	//Fungsi untuk mengkonversi permil ke string permil
	function konversi_str_permil($permil)
	{
		$pembilang_permil = $permil;
		
		//Dalam perhitungan, tanda titik adalah separator untuk desimal
		//Cek apakah ada tanda titik
		if(strstr($pembilang_permil, '.'))
		{
			//Jika ada tanda titik, ganti tanda titik dengan tanda koma
			$pembilang_permil = str_replace('.', ',', $pembilang_permil);
		}
		
		$str_permil = $pembilang_permil.'%o';
		return $str_permil;
	}
	
	//Fungsi untuk memformat integer dalam PHP ke string untuk menghasilkan format mata uang
	function format_angka($angka)
	{
		$str_angka = number_format($angka, 2, ',' , '.');
		return $str_angka;
		
		//number_format(angka yang diubah, [banyak angka di belakang koma], [string pemisah angka di belakang koma], [string pemisah ribuan]);
		//Misalnya 1000000 menjadi 1.000.000,00
	}
	
	//Fungsi untuk menghitung biaya polis dan materai berdasarkan total premi setahun
	function hitung_biaya_polis($total_premi)
	{
		if($total_premi > 0 && $total_premi < 250000)
		{
			$biaya_polis_materai = 26000;
		}
		else if($total_premi >= 250000 && $total_premi < 1000000 )
		{
			$biaya_polis_materai = 29000;
		}
		else if($total_premi >= 1000000 )
		{
			$biaya_polis_materai = 32000;
		}
		else
		{
			$biaya_polis_materai = 0;
		}
		
		return $biaya_polis_materai;
	}
}