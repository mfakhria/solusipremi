<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cek_email_model extends CI_Model{
	
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	//Fungsi untuk ambil email dari tabel di database
	public function get_email($tabel, $email)
	{
		$query = $this->db->get_where($tabel, array('email' => $email));
		//Kembalikan jumlah baris yang didapat
		return $query->num_rows();
	}
}
/* End of file Cek_email_model.php
 Location: ./application/models/Cek_email_model.php */