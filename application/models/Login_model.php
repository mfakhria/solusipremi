<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login_model extends CI_Model{
	
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}
	
	//Fungsi untuk mengecek jumlah email dan password yang sesuai
	public function login($email, $password)
	{
		$query = $this->db->get_where('tabel_member', array('email' => $email, 'password' => $password));
		return $query->num_rows();
	}
	
	//Fungsi untuk mengambil data hasil login
	public function data_login($email, $password)
	{
		$query = $this->db->get_where('tabel_member', array('email' => $email, 'password' => $password));
		return $query->row();
	}
}
/* End of file Login_model.php
 Location: ./application/models/Login_model.php */