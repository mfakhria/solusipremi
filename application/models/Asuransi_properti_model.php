<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Asuransi_properti_model extends CI_Model{
	
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}
	
	//Fungsi untuk mengambil data detail produk asuransi properti
	public function get_detail($id)
	{
		$query = $this->db->get_where('detail_produk_asuransi_properti', array('id_partner' => $id));
		return $query->row_array();
	}
	
	public function get_partner($id_partner = FALSE)
	{
		//Jika id partner tidak ada
		if($id_partner === FALSE)
		{
			$query = $this->db->get('partner_kami');
			return $query->result_array();
		}
		
		//Jika id partner ada
		$query = $this->db->get_where('partner_kami', array('id' => $id_partner));
		return $query->row_array();
	}
	
	public function get_provinsi()
	{
		$this->db->order_by('nama_provinsi', 'asc');
		return $this->db->get('tabel_provinsi')->result();
	}
	
	public function get_kota()
	{
		//JOIN tabel 'tabel_kab_kota' dengan 'tabel_provinsi'
		$this->db->order_by('nama_kab_kota', 'asc');
		$this->db->join('tabel_provinsi', 'tabel_kab_kota.id_provinsi = tabel_provinsi.id_provinsi');
		return $this->db->get('tabel_kab_kota')->result();
	}
	
	//Fungsi untuk mengambil keterangan dari tabel 'tarif_premi_asuransi_harta_benda' untuk autocomplete
	public function get_row($keyword)
	{
		$this->db->like('keterangan', $keyword);
		return $this->db->get('tarif_premi_asuransi_harta_benda')->result_array();
	}
	
	//Fungsi untuk mengambil data dari tabel 'tarif_premi_asuransi_harta_benda'
	public function get_okupasi($penggunaan_bangunan)
	{//Lakukan query untuk mendapatkan sebaris data pada 'tarif_premi_asuransi_harta_benda'
		//berdasarkan jenis okupasi (penggunaan bangunan)
		
		$this->db->like('keterangan', $penggunaan_bangunan , 'both');
		//Menghasilkan: WHERE `keterangan` LIKE '%$penggunaan_bangunan%' ESCAPE '!'
		
		return $this->db->get('tarif_premi_asuransi_harta_benda')->result();
		//Menghasilkan: SELECT * FROM tarif_premi_asuransi_harta_benda
	}
	
	//Fungsi untuk mengambil data dari tabel 'jaminan_banjir_harta_benda' berdasarkan id provinsi
	public function get_jaminan_banjir($id_provinsi)
	{
		if($id_provinsi == 5 || $id_provinsi == 2 || $id_provinsi == 8) //Jika id provinsi adalah DKI JAKARTA (5), atau BANTEN (2), atau JAWA BARAT (8)
		{
			$id = 1; //JAKARTA, BANTEN, JABAR (data pada tabel dengan id = 1)
		}
		else
		{
			$id = 2; //LUAR JAKARTA, BANTEN, JABAR (data pada tabel dengan id = 2)
		}
		
		//Lakukan query untuk mendapatkan data pada 'jaminan_banjir_harta_benda'
		$query = $this->db->get_where('jaminan_banjir_harta_benda', array('id' => $id));
		return $query->row_array();
	}
	
	//Fungsi untuk mengambil data dari tabel 'tarif_premi_gempa_*' berdasarkan kode okupasi
	public function get_jaminan_gempa($kode_okupasi, $jml_lantai)
	{
		if ($kode_okupasi !== 2976) //Jika kode okupasi selain 2976, berarti objek pertanggungan adalah 'Commercial'
		{
			$tabel_gempa = 'tarif_premi_gempa_commercial_industrial';
			
			//Untuk objek pertanggungan 'Commercial', tentukan kelas konstruksi berdasarkan jumlah lantai
			if ($jml_lantai <= 9) //Jika jumlah lantai kurang dari sama dengan 9
			{
				$id = 1; //Ambil data pada tabel dengan id = 1
			}
			else //Jika jumlah lantai lebih dari 9
			{
				$id = 2; //Ambil data pada tabel dengan id = 2
			}
		}
		else //Selain itu, maka objek pertanggungan adalah 'Dwelling House' dengan kode okupasi 2976
		{
			$tabel_gempa = 'tarif_premi_gempa_dwelling_house';
			$id = 1; //Ambil data pada tabel dengan id = 1
		}
		
		$query = $this->db->get_where($tabel_gempa, array('id' => $id));
		return $query->row_array();
	}
	
	//Fungsi untuk mengambil data dari tabel 'zona_asuransi_gempa_bumi' berdasarkan id Kota atau Kabupaten
	public function get_zona($id_kab_kota)
	{
		$query = $this->db->get_where('zona_asuransi_gempa_bumi', array('no' => $id_kab_kota));
		return $query->row_array();
	}
}
/* End of file Asuransi_properti_model.php
 Location: ./application/models/Asuransi_properti_model.php */