<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Lupa_password_model extends CI_Model{
	
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}
	
	//Fungsi untuk mengganti password member
	public function ganti_password($email, $enkrip_pass)
	{	
		//Update password yang ada di tabel 'tabel_member' berdasarkan email milik user
		$data = array('password' => $enkrip_pass);

		$this->db->where('email', $email);
		$query = $this->db->update('tabel_member', $data);
		//Menghasilkan:
		//UPDATE tabel_member SET password = '{$enkrip_pass}' WHERE email = $email
		
		return $query;
	}		
	
	//Fungsi untuk mendapatkan data user untuk nantinya mengirim email ganti password
	public function get_userdata($email)
	{	
		$query = $this->db->get_where('tabel_member', array('email' => $email));
		return $query->row_array();
	}		
}
/* End of file Lupa_password_model.php
 Location: ./application/models/Lupa_password_model.php */