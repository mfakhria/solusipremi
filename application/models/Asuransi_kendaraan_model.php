<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Asuransi_kendaraan_model extends CI_Model{
	
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}
	
	//Fungsi untuk mengambil data detail produk asuransi kendaraan
	public function fitur($id)
	{
		$query = $this->db->get_where('detail_produk_asuransi_kendaraan', array('id_partner' => $id));
		return $query->row_array();
	}
	
	public function get_partner($id_partner = FALSE)
	{
		//Jika id partner tidak ada
		if($id_partner === FALSE)
		{
			$query = $this->db->get('partner_kami');
			return $query->result_array();
		}
		
		//Jika id partner ada
		$query = $this->db->get_where('partner_kami', array('id' => $id_partner));
		return $query->row_array();
	}
	
	//Fungsi untuk mengambil data dari tabel pertanggungan
	public function get_pertanggungan($jenis_pertanggungan, $jenis_kendaraan, $total_hrg_pertanggungan)
	{
		//Jenis Pertanggungan (Jika tidak dipilih, defaultnya '1' 'Comprehensive')
		//value="1" selected="selected" Comprehensive
		//value="2" Total Loss Only (TLO)
		
		if($jenis_pertanggungan === '1') //Jika $jenis_pertanggungan adalah '1' atau 'Comprehensive'
		{
			$tabel_pertanggungan = 'tarif_premi_kendaraan_comprehensive'; //Pilih tabel 'tarif_premi_kendaraan_comprehensive'
		}
		else //Jika $jenis_pertanggungan adalah '2' atau 'TLO'
		{
			$tabel_pertanggungan = 'tarif_premi_kendaraan_tlo'; //Pilih tabel 'tarif_premi_kendaraan_tlo'
		}
		
		//Jenis Kendaraan (Jika tidak dipilih, defaultnya '1' yaitu 'Penumpang (non Truk/Pick-Up/Bus)')
		//value="1" selected="selected" Penumpang (non Truk/Pick-Up/Bus)
		//value="2" Truk/Pick-Up
		//value="3" Bus
		//value="4" Roda Dua (Sepeda Motor)
		
		//Penentuan kategori untuk mobil penumpang berdasarkan Total Harga Pertanggungan
		if($jenis_kendaraan === '1') //Jika jenis kendaraan adalah 'Penumpang'
		{
			if($total_hrg_pertanggungan >= 0 && $total_hrg_pertanggungan <= 125000000)
			{
				$kategori = '1'; //Pertanggungan untuk kategori 1 (0 s.d. Rp125.000.000,00)
			}
			else if($total_hrg_pertanggungan > 125000000 && $total_hrg_pertanggungan <= 200000000)
			{
				$kategori = '2'; //Pertanggungan untuk kategori 2 (>Rp125.000.000,00 s.d. Rp200.000.000,00)
			}
			else if($total_hrg_pertanggungan > 200000000 && $total_hrg_pertanggungan <= 400000000)
			{
				$kategori = '3'; //Pertanggungan untuk kategori 3 (>Rp200.000.000,00 s.d. Rp400.000.000,00)
			}
			else if($total_hrg_pertanggungan > 400000000 && $total_hrg_pertanggungan <= 800000000)
			{
				$kategori = '4'; //Pertanggungan untuk kategori 4 (>Rp400.000.000,00 s.d. Rp800.000.000,00)
			}
			else
			{
				$kategori = '5'; //Pertanggungan untuk kategori 5 (>Rp800.000.000,00)
			}
		}
		else if($jenis_kendaraan === '2') //Jika jenis kendaraan adalah 'Truk/Pick-Up'
		{
			$kategori = '6'; //Pertanggungan untuk kategori 6 yaitu Truk/Pick-Up
		}
		else if($jenis_kendaraan === '3') //Jika jenis kendaraan adalah 'Bus'
		{
			$kategori = '7'; //Pertanggungan untuk kategori 7 yaitu Bus
		}
		else //Jika jenis kendaraan adalah 'Roda Dua (Sepeda Motor)'
		{
			$kategori = '8'; //Pertanggungan untuk kategori 8 yaitu Roda Dua (Sepeda Motor)
		}
		
		//Lakukan query untuk mendapatkan sebaris data pada tabel pertanggungan
		//berdasarkan kategori, baik Comprehensive maupun TLO
		$query = $this->db->get_where($tabel_pertanggungan, array('kategori' => $kategori));
		
		return $query->row_array();
	}
	
	public function get_wilayah($id_pelat)
	{
		//Pelat Nomor untuk menentukan wilayah (Jika tidak dipilih, defaultnya '12' atau pelat 'A')
		
		//Lakukan query untuk mendapatkan wilayah berdasarkan value hasil seleksi
		$query = $this->db->get_where('pelat_nomor', array('id' => $id_pelat));
		
		return $query->row_array();
	}
	
	public function get_jaminan_banjir($wil)
	{
		$query = $this->db->get_where('jaminan_banjir_kendaraan', array('wilayah' => $wil));
		return $query->row_array();
	}
	
	public function get_jaminan_gempa($wil)
	{
		$query = $this->db->get_where('jaminan_gempa_kendaraan', array('wilayah' => $wil));
		return $query->row_array();
	}
	
	public function get_jaminan_huruhara()
	{
		$query = $this->db->get_where('jaminan_huruhara_terorisme_kendaraan', array('id' => 3));
		return $query->row_array();
	}
	
	public function get_jaminan_terorisme()
	{
		$query = $this->db->get_where('jaminan_huruhara_terorisme_kendaraan', array('id' => 4));
		return $query->row_array();
	}
	
	public function get_jaminan_tjh($id_tjh)
	{
		$query = $this->db->get_where('jaminan_tpl_kendaraan', array('id' => $id_tjh));
		return $query->row_array();
	}
	
	public function get_jaminan_kecelakaan_diri()
	{
		$query = $this->db->get_where('jaminan_kecelakaan_diri', array('id' => 1));
		return $query->row_array();
	}
	
	public function simpan_data()
	{	
		/*
		$kd_jenis_kelamin = $this->input->post('jenis_kelamin');
		
		if($kd_jenis_kelamin == '1')
		{
			$jenis_kelamin = 'Laki-Laki';
		}
		else
		{
			$jenis_kelamin = 'Perempuan';
		}
		
		$kd_transmisi = $this->input->post('transmisi');
		
		if($kd_transmisi == '1')
		{
			$transmisi = 'Manual';
		}
		else
		{
			$transmisi = 'Otomatis';
		}
		
		$kd_penggunaan_kendaraan = $this->input->post('penggunaan_kendaraan');
		
		if($kd_penggunaan_kendaraan == '1')
		{
			$penggunaan_kendaraan = 'Pribadi';
		}
		else
		{
			$penggunaan_kendaraan = 'Komersial/Disewakan/Umum';
		}
		
		$data = array(
						'id_member' => $_SESSION['id_member'],
						'no_ktp' => $this->input->post('nomor_ktp'),
						'nama_tertanggung' => strtoupper($this->input->post('nama_tertanggung')),
						'jenis_kelamin' => strtoupper($jenis_kelamin),
						'alamat_tertanggung' => strtoupper($this->input->post('alamat_tertanggung')),
						'rt' => $this->input->post('rt'),
						'rw' => $this->input->post('rw'),
						'desa_kelurahan' => strtoupper($this->input->post('desa_kelurahan')),
						'kecamatan' => strtoupper($this->input->post('kecamatan')),
						'kab_kota' => strtoupper($this->input->post('kab_kota')),
						'email' => $this->input->post('email'),
						'nomor_telepon' => $this->input->post('nomor_telepon'),						
						'periode_mulai' => $this->input->post('periode_mulai'),
						'periode_sampai' => $this->input->post('periode_sampai'),
						'nama_stnk' => strtoupper($this->input->post('nama_stnk')),
						'alamat_stnk' => strtoupper($this->input->post('alamat_stnk')),
						'merek_kendaraan' => strtoupper($this->input->post('merek_kendaraan')),
						'tipe_kendaraan' => strtoupper($this->input->post('tipe_kendaraan')),
						'tahun_pembuatan' => $this->input->post('tahun_pembuatan'),
						'warna' => strtoupper($this->input->post('warna')),
						'transmisi' => $transmisi,
						'no_polisi' => strtoupper($this->input->post('nomor_polisi')),
						'no_rangka' => strtoupper($this->input->post('nomor_rangka')),
						'no_mesin' => strtoupper($this->input->post('nomor_mesin')),
						'jenis_kendaraan' => $this->input->post('jenis_kendaraan'),
						'wilayah' => , //dari session
						'jenis_pertanggungan' => , //dari session
						'perluasan_jaminan' => , //dari session
						'penggunaan_kendaraan' => $penggunaan_kendaraan,
						'harga_kendaraan' => ,
						'harga_perlengkapan_nonstd' => ,
						'total_harga_pertanggungan' => ,
						'premi_dasar' => ,
						'persen_premi' => ,
						'loading_premi' => ,
						'persen_usia' => ,
						'premi_banjir' => ,
						'persen_banjir' => ,
						'premi_gempa' => ,
						'persen_gempa' => ,
						'premi_huruhara' => ,
						'persen_huruhara' => ,
						'premi_terorisme' => ,
						'persen_terorisme' => ,
						'limit_tjh' => ,
						'premi_tjh' => ,
						'limit_pad' => ,
						'premi_pad' => ,
						'persen_pad' => ,
						'limit_pap' => ,
						'premi_pap' => ,
						'persen_pap' => ,
						'jml_seat' => ,
						'total_premi' => ,
						'biaya_polis_materai' => ,
						'total_bayar_setahun' => ,
						'prorata' => 
						);
						
		return $this->db->insert('sppkb', $data);*/
	}
}
/* End of file Asuransi_kendaraan_model.php
 Location: ./application/models/Asuransi_kendaraan_model.php */