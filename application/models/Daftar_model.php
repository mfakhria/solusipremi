<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Daftar_model extends CI_Model{
	
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}
	
	//Fungsi untuk melakukan proses menyimpan data pendaftaran ke database pada 'tabel_member'
	public function proses_daftar()
	{
		//Dapatkan data yang diinput oleh user dan simpan ke dalam bentuk array
		$data = array(
						'nama_depan' => $this->input->post('firstname'),
						'nama_belakang' => $this->input->post('lastname'),
						'email' => $this->input->post('email'),
						'telepon' => $this->input->post('handphone'),
						'password' => strrev(md5($this->input->post('password'))) //Enkripsi Password
					);

		//Simpan data pendaftaran berbentuk array tadi ke database pada 'tabel_member'
		$proses_insert = $this->db->insert('tabel_member', $data);
		//Menghasilkan: INSERT INTO tabel_member (id, nama_depan, nama_belakang, email, telepon, password) VALUES ('', 'namadepan', 'namabelakang', 'email', 'telepon', 'password')
		
		//Jika terjadi masalah saat menyimpan data ke tabel 'tabel_member'
		if(!$proses_insert)
		{
			return FALSE; //Kembalikan nilai FALSE
		}
		else
		{
			//Jika tidak ada masalah, simpan juga email pendaftar ke tabel 'newsletter'
			//Pertama, periksa tabel 'newsletter' apakah email pendaftar sudah ada
			$query = $this->db->get_where('newsletter', array('email' => $this->input->post('email')));
			
			//Jika email pendaftar tidak ada di tabel 'newsletter' (jumlah baris == 0)
			if($query->num_rows() == 0)
			{
				$tgl = date('d-m-Y, H:i');
				$data_newsletter = array(
								'email' => $this->input->post('email'),
								'tgl_registrasi' => $tgl
							);
				//Simpan email pendaftar ke tabel 'newsletter'
				$this->db->insert('newsletter', $data_newsletter);
			}
			return TRUE; //Kembalikan nilai TRUE
		}
	}
}
/* End of file Daftar_model.php
 Location: ./application/models/Daftar_model.php */