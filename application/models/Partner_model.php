<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Partner_model extends CI_Model{
	
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}
	
	public function get_partner($partner)
	{
		//Fungsi untuk mengambil data partner pada tabel 'partner_kami' di database berdasarkan slug partner
		$query = $this->db->get_where('partner_kami', array('slug' => $partner));
		return $query->row_array();
	}
}
/* End of file Partner_model.php
 Location: ./application/models/Partner_model.php */