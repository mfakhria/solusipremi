<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Newsletter_model extends CI_Model{
	
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}
	
	//Fungsi untuk menyimpan email anggota newsletter ke tabel 'newsletter' di database
	public function simpan_email()
	{
		$tgl = date('d-m-Y H:i:s');
		$data = array(
					'email' => $this->input->post('email'),
					'tgl_registrasi' => $tgl
					);
		$this->db->insert('newsletter', $data);
	}
}
/* End of file Newsletter_model.php
 Location: ./application/models/Newsletter_model.php */