<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kontak_model extends CI_Model{
	
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}
	
	public function simpan_kontak()
	{
		//Fungsi untuk menyimpan kontak ke tabel kontak_kami di database
		$tgl = date('d-m-Y H:i:s');
		
		$minat = implode(', ', $this->input->post('minat[]'));
		
		$data = array(
					'nama' => $this->input->post('nama', TRUE),
					'email' => $this->input->post('email', TRUE),
					'telepon' => $this->input->post('telepon'),
					'minat_asuransi' => $minat,
					'pesan' => nl2br($this->input->post('pesan')),
					'tgl_diterima' => $tgl
					);
		
		return $this->db->insert('kontak_kami', $data);
	}
}
/* End of file Kontak_model.php
 Location: ./application/models/Kontak_model.php */